<?php

class STT_PostAdmin extends STT_Controller
{
    private $actionAdd = 'add_';

    public function __construct()
    {
        parent::__construct();
        add_filter('excerpt_length', array($this, 'changeLengthExcerpt'));
        add_filter('excerpt_more', array($this, 'changeMoreExcerpt'));
        add_action($this->actionAdd . 'meta_boxes', array($this, 'postAddMetaBox'));
        add_action('wp_ajax_gallery_update', array($this, 'ajaxGalleryUpdate'));
        add_action('save_post', array($this, 'postInforSave'));
        add_action('manage_post_posts_columns', array($this, 'postSetColumns'));
        add_action('manage_post_posts_custom_column', array($this, 'postCustomColumns'), 10, 2);

    }

    public function ajaxGalleryUpdate()
    {
        check_ajax_referer('security', 'security');
        if (!empty($_POST['ids'])) {
            $return = '';
            $ids = implode(',', $_POST['ids']);
            foreach ($_POST['ids'] as $id) {
                $thumbnail = wp_get_attachment_image_src($id);
                $return .= '<div class="attachments-item"><span class="stt-attachments-delete">Close</span><img  src="' . $thumbnail[0] . '" data-id="' . $id . '"/></div>';
            }
            echo json_encode([
                'img' => $return,
                'ids' => $ids
            ]);
            die;
        }
    }

    public function postSetColumns($columns)
    {
        unset($columns['tags']);
        unset($columns['comments']);
        $newColumns = array(
            'image' => esc_html__('Image', 'layout-blog'),
            'title' => esc_html__('Post Name', 'layout-blog'),
            'description' => esc_html__('Description', 'layout-blog'),
        );
        $columns = array_merge($newColumns, $columns);
        return $columns;
    }

    public function postCustomColumns($column, $post_id)
    {
        switch ($column) {
            case 'description':
                echo get_the_excerpt();
                break;
            case 'image':
                $image = get_the_post_thumbnail($post_id, 'thumbnail');
                echo stt_esc_data($image);

                break;
            default:
                # code...
                break;
        }
    }

    public function postAddMetaBox()
    {
        stt_meta_box('post-infor', 'Post Information', [$this, 'postInforOutput'], 'post');
    }

    public function postInforOutput()
    {
        wp_nonce_field('_post_infor_save', '_post_add_metabox_nonce');
        $this->load->view('post_metabox', 'backend');
    }

    public function postInforSave($post_id)
    {
        if (!isset($_POST['_post_add_metabox_nonce'])) {
            return;
        }
        if (!wp_verify_nonce($_POST['_post_add_metabox_nonce'], '_post_infor_save')) {
            return;
        }
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
            return;
        }
        $image = sanitize_text_field($_POST['upload_value']);
        update_post_meta($post_id, 'stt_post_image', $image);
        $numberView = 0;
        update_post_meta($post_id, 'stt_number_view', $numberView);
    }

    public function changeMoreExcerpt($more)
    {
        return '...';
    }

    public function changeLengthExcerpt($length)
    {
        return 25;
    }

    public static function get_inst()
    {
        static $instance;
        if (is_null($instance)) {
            $instance = new self();
        }
        return $instance;
    }

}

STT_PostAdmin::get_inst();