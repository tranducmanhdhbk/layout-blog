<?php

class STT_ThemeOption extends STT_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('theme_option_model', $this);
        add_action('admin_menu', array($this, 'addMenuPage'));
    }

    public function sttListPage()
    {
        return $this->theme_option_model->sttListPage();
    }

    public function addMenuPage()
    {
        add_menu_page('Theme Options', 'Theme Options', 'manage_options', 'theme-option', [$this, 'menuPageOutput']);
    }

    function menuPageOutput()
    {
        if (!empty($_POST['save-theme-option'])) {
            $stFooterPageID = $_POST['stt_footer_page'];
            $stBlogPageID = $_POST['stt_blog_page'];
            $stInstagramUserName = $_POST['stt_insta_user_name'];
            $stNumberOfPhotos = $_POST['stt_number_of_photos'];
            $stInstagramLinh = $_POST['stt_instagram_link'];
            update_option('stt_footer_page', $stFooterPageID);
            update_option('stt_blog_page', $stBlogPageID);
            update_option('stt_insta_user_name', $stInstagramUserName);
            update_option('stt_number_of_photos', $stNumberOfPhotos);
            update_option('stt_instagram_link', $stInstagramLinh);
        }
        $this->load->view('theme_option', 'backend');
    }

    public static function get_inst()
    {
        static $instance;
        if (is_null($instance)) {
            $instance = new self();
        }
        return $instance;
    }
}

STT_ThemeOption::get_inst();