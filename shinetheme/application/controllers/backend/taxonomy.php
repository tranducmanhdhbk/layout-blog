<?php

Class STT_Taxonomy extends STT_Controller
{
    public function __construct()
    {
        parent::__construct();
        add_action('category_add_form_fields', array($this, 'cateCustomMetaField'));
        add_action('category_edit_form_fields', array($this, 'cateEditMetaField'));
        add_action('created_category', array($this, 'categoriesSave'), 10, 1);
        add_action('edited_category', array($this, 'categoriesSave'), 10, 1);
        add_filter('manage_edit-category_columns', array($this, 'cateSetColumn'));
        add_filter('manage_category_custom_column', array($this, 'cateCustomColumn'), 10, 3);
        add_action('st_destinations_add_form_fields', array($this, 'desCustomMetaField'));
        add_action('st_destinations_edit_form_fields', array($this, 'desEditMetaField'));
        add_action('created_st_destinations', array($this, 'destinationSave'), 10, 1);
        add_action('edited_st_destinations', array($this, 'destinationSave'), 10, 1);
        add_filter('manage_edit-st_destinations_columns', array($this, 'desSetColumn'));
        add_filter('manage_st_destinations_custom_column', array($this, 'desCustomColumn'), 10, 3);
        add_action('wp_ajax_st_dashboard_change_avatar', [$this, 'sttDashboardChangeAvatar']);

    }

    public function sttDashboardChangeAvatar()
    {
        check_ajax_referer('security', 'security');
        $stAttachment = $this->input->post('attachment_id');
        if (!empty($stAttachment)) {
            $url = '';
            $thumbnail = wp_get_attachment_image_url($stAttachment, array(80, 80));
            $url .= '<img src="' . esc_url($thumbnail) . '" data-id="' . esc_attr($stAttachment) . '" alt="'. stt_get_alt_image() .'" />';
            echo json_encode(
                [
                    'status' => 1,
                    'avatar' => $stAttachment,
                    'url' => $url
                ]
            );
        }

        die();

    }

    public function cateCustomMetaField()
    {

        $this->load->view('category_custom_field', 'backend/taxonomy');

    }

    public function cateEditMetaField()
    {

        $this->load->view('category_edit_field', 'backend/taxonomy');

    }

    public function categoriesSave($term_id)
    {
        $imageCate = sanitize_text_field($this->input->post('upload_value'));
        if($imageCate){

            update_term_meta($term_id, 'stt_cate_image', $imageCate);
        }
    }

    public function cateSetColumn($columns)
    {
        unset($columns['slug']);
        $newColumns = array(
            'image' => esc_html__('Image', 'layout-blog'),

        );
        $columns = array_merge($columns, $newColumns);
        return $columns;
    }

    public function cateCustomColumn($content, $columns, $term_id)
    {
        switch ($columns) {
            case'image':
                $imageID = get_term_meta($term_id, 'stt_cate_image', true);
                $imageUrl = wp_get_attachment_image_url($imageID, array(40, 40));
                $content = '<img src="' . esc_url($imageUrl) . '" alt="category image">';;
                break;

            default:
                # code...
                break;
        }
        return $content;
    }

    public function desCustomMetaField()
    {

        $this->load->view('destination_custom_field', 'backend/taxonomy');

    }

    public function desEditMetaField()
    {

        $this->load->view('destination_edit_field', 'backend/taxonomy');

    }

    public function destinationSave($term_id)
    {
        $imageCate = sanitize_text_field($_POST['upload_value']);
        if($imageCate){
            update_term_meta($term_id, 'stt_destination_image', $imageCate);
        }
        $numberView = 0;
        update_term_meta($term_id, 'stt_number_view', $numberView);
        $destinationPageID = sanitize_text_field($_POST['stt_destination_page']);
        update_term_meta($term_id, 'stt_destination_page', $destinationPageID);
        update_post_meta($destinationPageID, 'page_term_id', $term_id);
        wp_set_object_terms($destinationPageID, $term_id, 'st_destinations');
    }

    public function desSetColumn($columns)
    {
        unset($columns['slug']);
        $newColumns = array(
            'page' => esc_html__('Page', 'layout-blog'),
            'image' => esc_html__('Image', 'layout-blog'),
        );
        $columns = array_merge($columns, $newColumns);
        return $columns;
    }

    public function desCustomColumn($content, $columns, $term_id)
    {
        switch ($columns) {
            case'image':
                $imageID = get_term_meta($term_id, 'stt_destination_image', true);
                $imageUrl = wp_get_attachment_image_url($imageID, array(40, 40));
                $content = '<img src="' . esc_url($imageUrl) . '" alt="destination image">';
                break;
            case 'page';
                $pageID = get_term_meta($term_id, 'stt_destination_page', true);
                $content = get_the_title($pageID);
            default:
                # code...
                break;
        }
        return $content;
    }


    public static function get_inst()
    {
        static $instance;
        if (is_null($instance)) {
            $instance = new self();
        }
        return $instance;
    }
}

STT_Taxonomy::get_inst();