<?php
/**
 * Created by PhpStorm.
 * User: Jream
 * Date: 6/29/2019
 * Time: 3:58 PM
 */
if (!class_exists('STT_Elementor')) {
    class STT_Elementor
    {
        const MINIMUM_ELEMENTOR_VERSION = '2.0.0';
        const MINIMUM_PHP_VERSION = '5.6';
        private static $_instance = null;

        public function __construct()
        {
            // Check if Elementor installed and activated
            if (!did_action('elementor/loaded')) {
                add_action('admin_notices', [$this, 'adminNoticeMissingMainPlugin']);
                return;
            }

            // Check for required Elementor version
            if (!version_compare(ELEMENTOR_VERSION, self::MINIMUM_ELEMENTOR_VERSION, '>=')) {
                add_action('admin_notices', [$this, 'adminNoticeMinimumElementorVersion']);
                return;
            }

            // Check for required PHP version
            if (version_compare(PHP_VERSION, self::MINIMUM_PHP_VERSION, '<')) {
                add_action('admin_notices', [$this, 'adminNoticeMinimumPHPVersion']);
                return;
            }
            // Register widgets
            add_action('elementor/elements/categories_registered', [$this, 'addElementorWidgetCategories']);
            add_action('elementor/widgets/widgets_registered', [$this, 'registerElementorWidgets']);
        }

        public function adminNoticeMissingMainPlugin()
        {

            if (isset($_GET['activate'])) unset($_GET['activate']);

            $message = sprintf(
            /* translators: 1: Plugin name 2: Elementor */
                esc_html__('"%1$s" requires "%2$s" to be installed and activated.', 'layout-blog'),
                '<strong>' . esc_html__('Elementor Test Extension', 'layout-blog') . '</strong>',
                '<strong>' . esc_html__('Elementor', 'layout-blog') . '</strong>'
            );

            printf('<div class="notice notice-warning is-dismissible"><p>%1$s</p></div>', $message);

        }

        public function adminNoticeMinimumElementorVersion()
        {

            if (isset($_GET['activate'])) unset($_GET['activate']);

            $message = sprintf(
            /* translators: 1: Plugin name 2: Elementor 3: Required Elementor version */
                esc_html__('"%1$s" requires "%2$s" version %3$s or greater.', 'layout-blog'),
                '<strong>' . esc_html__('Elementor Test Extension', 'layout-blog') . '</strong>',
                '<strong>' . esc_html__('Elementor', 'layout-blog') . '</strong>',
                self::MINIMUM_ELEMENTOR_VERSION
            );

            printf('<div class="notice notice-warning is-dismissible"><p>%1$s</p></div>', $message);

        }

        public function adminNoticeMinimumPHPVersion()
        {

            if (isset($_GET['activate'])) unset($_GET['activate']);

            $message = sprintf(
            /* translators: 1: Plugin name 2: PHP 3: Required PHP version */
                esc_html__('"%1$s" requires "%2$s" version %3$s or greater.', 'layout-blog'),
                '<strong>' . esc_html__('Elementor Test Extension', 'layout-blog') . '</strong>',
                '<strong>' . esc_html__('PHP', 'layout-blog') . '</strong>',
                self::MINIMUM_PHP_VERSION
            );

            printf('<div class="notice notice-warning is-dismissible"><p>%1$s</p></div>', $message);

        }

        public function addElementorWidgetCategories($elements_manager)
        {
            $elements_manager->add_category(
                'stt_layout_blog',
                [
                    'title' => esc_html__('Layout Blog', 'layout-blog'),
                    'icon' => 'fa fa-plug',
                ]
            );
            return $elements_manager;
        }

        public function registerElementorWidgets()
        {
            $files = array_filter(glob(STT_APPLICATION_PATH . 'elements/elementor/widgets/*'), 'is_file');
            if (!empty($files)) {
                foreach ($files as $key => $value) {
                    require_once($value);
                }
            }

            \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new \Elementor_Banner_Widget());
            \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new \Elementor_List_Of_Blog_Widget());
            \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new \Elementor_List_Of_Destination_Widget());
            \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new \Elementor_List_Of_Category_Widget());
            \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new \Elementor_List_Image_Instagram_Widget());
            \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new \Elementor_List_Feature_Widget());
            \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new \Elementor_Map_Widget());
            \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new \Elementor_Gallery_Destination_Widget());
            \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new \Elementor_Tabs_Information_Destination_Widget());
            \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new \Elementor_Pages_Pagination_Widget());
            \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new \Elementor_Author_Information_Widget());
            \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new \Elementor_Form_Search_Widget());


        }

        public static function instance()
        {
            if (is_null(self::$_instance)) {
                self::$_instance = new self();
            }
            return self::$_instance;

        }

        public static function not()
        {
        }
    }

    STT_Elementor::instance();
}