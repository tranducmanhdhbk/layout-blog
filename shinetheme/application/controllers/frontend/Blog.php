<?php

class STT_Blog extends STT_Controller
{
    public function __construct()
    {
        parent::__construct();
        add_filter('excerpt_length', array($this, 'changeLengthExcerpt'));
        add_filter('excerpt_more', array($this, 'changeMoreExcerpt'));
        add_action('wp_ajax_st_filter_layout_blog', [$this, 'filterLayoutBlog']);
        add_action('wp_ajax_nopriv_st_filter_layout_blog', [$this, 'filterLayoutBlog']);
        add_action('wp_ajax_st_load_more', [$this, 'sstLoadMoreDestination']);
        add_action('wp_ajax_nopriv_st_load_more', [$this, 'sstLoadMoreDestination']);
        add_filter('comment_form_default_fields', array($this, 'removeCookiesCommentField'));
        add_filter('comment_form_defaults', array($this, 'addCommentTextareaPlaceholder'));
        add_filter('comment_form_fields', array($this, 'moveCommentFieldToBottom'));
    }

    public function removeCookiesCommentField($fields)
    {
        if (isset($fields['cookies'])) {
            unset($fields['cookies']);
        }
        foreach ($fields as &$field) {
            $field = str_replace('id="author"', 'id="author" placeholder="' . esc_html__(' Name ', 'layout-blog') . '"', $field);
            $field = str_replace('id="email"', 'id="email" placeholder="' . esc_html__(' Email ', 'layout-blog') . '"', $field);
            $field = str_replace('id="url"', 'id="url" placeholder="' . esc_html__(' Website ', 'layout-blog') . '"', $field);
        }
        return $fields;
    }

    public function addCommentTextareaPlaceholder($args)
    {
        $args['comment_field'] = str_replace('<textarea', '<textarea placeholder="'. esc_html__('Message', 'layout-blog') .'"', $args['comment_field']);
        return $args;
    }

    function moveCommentFieldToBottom($fields)
    {
        $comment_field = $fields['comment'];
        unset($fields['comment']);
        $fields['comment'] = $comment_field;
        return $fields;
    }

    public function sstLoadMoreDestination()
    {
        $this->checkSecurity();
        $offset = $this->input->post('offset');
        $total = $this->input->post('count');
        $paged = $this->input->post('paged');
        $args = array(
            'taxonomy' => 'st_destinations',
            'hide_empty' => false,
            'number' => $paged,
            'offset' => $offset,
        );
        $query = get_terms($args);
        if (empty($total)) {
            $count_tax = wp_count_terms('st_destinations', ['hide_empty' => false]);
            if (is_wp_error($count_tax)) {
                $total = 0;
            } elseif (is_array($count_tax)) {
                $total = count($count_tax);
            } else {
                $total = $count_tax;
            }

        }

        $html = '';
        foreach ($query as $k => $v) {
            $link = get_term_link($v->term_id, 'st_destinations');
            $html .= '<div class="col-md-3 col-6 destination-item">';
            $html .= '<div class="thumb">';
            $imageID = stt_get_term_meta($v->term_id, 'stt_destination_image');
            $imageUrl = wp_get_attachment_image_url($imageID, array(270,360));
            if (!empty($imageUrl)) {
                $html .= '<a href=" ' . esc_url($link) . ' "><img src=" ' . esc_url($imageUrl) . ' "></a>';
            }
            $html .= '</div>';
            $html .= '<div class="destination-name">';
            $html .= ' <a href="' . esc_url($link) . '"><h4>' . esc_html($v->name) . '</h4></a>';
            $html .= '</div>';
            $html .= '</div>';
        }
        $this->sendJson([
            'status' => 1,
            'html' => $html,
            'count' => $total
        ]);
    }

    public function filterLayoutBlog()
    {
        $this->checkSecurity();
        $orderBy = $this->input->post('orderby', 'lastest_post');
        $paged = $this->input->post('filter_paged', 1);
        $args = [
            'post_type' => 'post',
            'oderby' => $orderBy,
            'paged' => $paged,
        ];
        switch ($orderBy) {
            case 'lastest_post':
                $args['oderby'] = 'date';
                $args['order'] = 'DESC';
                break;
            case 'most_viewed_post';
                $args['meta_key'] = 'stt_number_view';
                $args['orderby'] = 'meta_value_num';
                $args['order'] = 'DESC';
                break;

        }
        $query = new WP_Query($args);
        set_query_var('paged', $paged);
        $pagination = $this->load->view('pagination', 'frontend/page', array('query' => $query), true);
        $html = '';
        if ($query->have_posts()) {
            $html .= '<div class="row">';
            while ($query->have_posts()): $query->the_post();
                $html .= '<div class="col-md-6">';
                $html .= $this->load->view('grid2', 'frontend/blog', false, true);
                $html .= '</div>';
            endwhile;
            $html .= '</div>';
        }
        wp_reset_postdata();

        $this->sendJson([
            'status' => 1,
            'pagination' => $pagination,
            'currentPage' => $paged,
            'html' => $html
        ]);

    }

    public function changeMoreExcerpt($more)
    {
        return '...';
    }

    public function changeLengthExcerpt($length)
    {
        return 25;
    }
}
