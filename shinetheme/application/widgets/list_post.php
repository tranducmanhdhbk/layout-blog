<?php
if (!class_exists('stt_list_post_widget')) {
    class stt_list_post_widget extends WP_Widget
    {
        public function __construct()
        {
            $widget_ops = array('classname' => 'stt_list_post_widget', 'description' => __("The list trending post for your site.", 'layout-blog'));
            parent::__construct('stt_list_post_widget', __("[Layout Blog] List post", 'layout-blog'), $widget_ops);
        }

        public function widget($args, $instance)
        {
            $title = apply_filters('widget_title', empty($instance['title']) ? '' : $instance['title'], $instance, $this->id_base);

            extract(wp_parse_args($instance, array(
                'title' => '',
                'number_post' => ''
            )));

            if (empty($instance['number_post']))
                $instance['number_post'] = '-1';
            echo stt_esc_data($args['before_widget']);
            if ($title) {
                echo stt_esc_data($args['before_title']) . esc_html($title) . stt_esc_data($args['after_title']);
            }
            $args = array(
                'post_type' => 'post',
                'posts_per_page' => $instance['number_post'],
                'post_status' => 'publish',
                'meta_key' => 'stt_number_view',
                'orderby' => 'meta_value_num',
                'order' => 'DESC',
            );
            $query = new WP_Query($args);

            if ($query->have_posts()) {
                $st = &stt_get_instance();
                while ($query->have_posts()) {
                    global $post;
                    $query->the_post();
                    ?>
                    <div class="blog-item">
                        <?php
                        if (has_post_thumbnail()) {
                            ?>
                            <div class="thumb">
                                <a href="<?php echo esc_url(get_the_permalink()); ?>">
                                    <?php the_post_thumbnail(array(370, 280)); ?>
                                </a>
                            </div>
                            <div class="content">
                                <span class="stt-time"><?php the_time('d F Y') ?></span>
                                <span class="stt-blog-item-title">
                                    <a href="<?php echo esc_url(get_permalink($post->ID)); ?>"><?php echo esc_html(get_the_title()); ?></a>
                                </span>
                            </div>
                        <?php }
                        ?>

                    </div>
                    <?php
                }

            }
            wp_reset_postdata();
            echo '</div>';

        }

        public function form($instance)
        {
            extract(wp_parse_args($instance, array(
                'title' => '',
                'number_post' => '',
            )));
            $title = isset($instance['title']) ? esc_attr($instance['title']) : '';
            $numberPost = isset($instance['number_post']) ? esc_attr($instance['number_post']) : '';
            ?>
            <p>
                <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php echo esc_html__('Title:', 'layout-blog'); ?>
                    <input class="widefat"
                           id="<?php echo esc_attr($this->get_field_id('title')); ?>"
                           name="<?php echo esc_attr($this->get_field_name('title')); ?>"
                           type="text"
                           value="<?php echo esc_attr($title); ?>"/></label>
            </p>

            <p>
                <label for="<?php echo esc_attr($this->get_field_id('number_post')); ?>"><?php echo esc_html__('Number of Post:', 'layout-blog'); ?>
                    <input
                            class="widefat" id="<?php echo esc_attr($this->get_field_id('number_post')); ?>"
                            name="<?php echo esc_attr($this->get_field_name('number_post')); ?>" type="text"
                            value="<?php echo esc_attr($numberPost); ?>"/></label></p>
            <?php

        }

        public function update($new_instance, $old_instance)
        {
            $instance = $old_instance;
            $new_instance = wp_parse_args((array)$new_instance, array(
                'title' => '',
                'number_post' => '',
            ));
            $instance['title'] = sanitize_text_field($new_instance['title']);
            $instance['number_post'] = sanitize_text_field($new_instance['number_post']);
            return $instance;
        }
    }

    function stt_list_post_widget_func()
    {
        if (function_exists('stt_registration_widget')) {
            stt_registration_widget('stt_list_post_widget');
        }
    }

    add_action('widgets_init', 'stt_list_post_widget_func');
}