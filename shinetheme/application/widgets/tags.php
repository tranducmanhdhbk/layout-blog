<?php
/**
 * Created by PhpStorm.
 * User: HanhDo
 * Date: 7/29/2019
 * Time: 10:26 AM
 */
if (!class_exists('stt_tags_widget')) {
    class stt_tags_widget extends WP_Widget
    {
        public function __construct()
        {
            $widget_ops = array('classname' => 'stt_tags_widget', 'description' => __("The list tags for your site.", 'layout-blog'));
            parent::__construct('stt_tags_widget', __("[Layout Blog] Tags", 'layout-blog'), $widget_ops);
        }

        public function widget($args, $instance)
        {
            $title = apply_filters('widget_title', empty($instance['title']) ? '' : $instance['title'], $instance, $this->id_base);

            extract(wp_parse_args($instance, array(
                'title' => '',
                'blog_url' => '',
                'show_count' => 'no',
                'hide_empty' => 'no'
            )));
            if (empty($instance['number_tags']))
                $instance['number_tags'] = 'all';

            if (empty($instance['show_count']))
                $instance['show_count'] = 'no';

            if (empty($instance['hide_empty']))
                $instance['hide_empty'] = 'no';


            if (empty($instance['blog_url']))
                $instance['blog_url'] = '#';

            echo stt_esc_data($args['before_widget']);
            if ($title) {
                echo stt_esc_data($args['before_title']) . esc_html($title) . stt_esc_data($args['after_title']);
            }
            ?>
            <?php
            $terms = get_terms(array(
                'taxonomy' => 'post_tag',
                'number' => $instance['number_tags'],
                'hide_empty' => $instance['hide_empty'] == 'yes' ? true : false,
            ));


            if (!is_wp_error($terms)) {
                if (!empty($terms)) {
                    echo ' <div class="list_tags">';
                    ?>
                    <?php
                    foreach ($terms as $k => $v) {
                        $link = get_term_link($v->term_id, 'post_tag');

                        ?>
                        <div class="  tag-item">
                            <div class="tag-name">
                                <a href="<?php echo esc_url($link) ?>"><span><?php echo esc_html($v->name) ?></span></a>
                            </div>
                        </div>
                        <?php
                    }
                    echo ' </div>';
                }
            }
            echo stt_esc_data($args['after_widget']);
        }

        public function form($instance)
        {
            $instance = wp_parse_args((array)$instance, array(
                    'title' => '',
                    'blog_url' => '',
                    'show_count' => 'no',
                    'hide_empty' => 'no',

                )
            );
            $title = $instance['title'];
            $blog_url = $instance['blog_url'];
            $show_count = $instance['show_count'];
            $number_tags = isset($instance['number_tags']) ? esc_attr($instance['number_tags']) : '';
            $hide_empty = $instance['hide_empty'];

            ?>
            <p>
                <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php echo esc_html__('Title:', 'layout-blog'); ?>
                    <input class="widefat"
                           id="<?php echo esc_attr($this->get_field_id('title')); ?>"
                           name="<?php echo esc_attr($this->get_field_name('title')); ?>"
                           type="text"
                           value="<?php echo esc_attr($title); ?>"/></label>
            </p>

            <p>
                <label for="<?php echo esc_attr($this->get_field_id('number_tags')); ?>"><?php echo esc_html__('Number Of Tags:', 'layout-blog'); ?>
                    <input class="widefat" id="<?php echo esc_attr($this->get_field_id('number_tags')); ?>"
                           name="<?php echo esc_attr($this->get_field_name('number_tags')); ?>" type="text"
                           value="<?php echo esc_attr($number_tags); ?>"/></label></p>

            <p><label for="<?php echo esc_attr($this->get_field_id('show_count')); ?>"><input class="widefat"
                                                                                              id="<?php echo esc_attr($this->get_field_id('show_count')); ?>"
                                                                                              name="<?php echo esc_attr($this->get_field_name('show_count')); ?>"
                                                                                              type="checkbox"
                                                                                              value="yes" <?php echo stt_esc_data($show_count) == 'yes' ? 'checked' : ''; ?> /><?php echo esc_html__('Show Post Count', 'layout-blog'); ?>
                </label></p>

            <p><label for="<?php echo esc_attr($this->get_field_id('hide_empty')); ?>"><input class="widefat"
                                                                                              id="<?php echo esc_attr($this->get_field_id('hide_empty')); ?>"
                                                                                              name="<?php echo esc_attr($this->get_field_name('hide_empty')); ?>"
                                                                                              type="checkbox"
                                                                                              value="yes" <?php echo stt_esc_data($hide_empty) == 'yes' ? 'checked' : ''; ?> /><?php echo esc_html__('Hide Empty', 'layout-blog'); ?>
                </label></p>


            <p>
                <label for="<?php echo esc_attr($this->get_field_id('blog_url')); ?>"><?php echo esc_html__('Blog URL:', 'layout-blog'); ?>
                    <input
                            class="widefat" id="<?php echo esc_attr($this->get_field_id('blog_url')); ?>"
                            name="<?php echo esc_attr($this->get_field_name('blog_url')); ?>" type="text"
                            value="<?php echo esc_attr($blog_url); ?>"/></label></p>
            <?php
        }

        public function update($new_instance, $old_instance)
        {
            $instance = $old_instance;
            $new_instance = wp_parse_args((array)$new_instance, array(
                'title' => '',
                'blog_url' => '',
                'show_count' => '',
                'hide_empty' => '',
                'number_tags' => ''
            ));
            $instance['title'] = sanitize_text_field($new_instance['title']);
            $instance['blog_url'] = sanitize_text_field($new_instance['blog_url']);
            $instance['show_count'] = sanitize_text_field($new_instance['show_count']);
            $instance['hide_empty'] = sanitize_text_field($new_instance['hide_empty']);
            $instance['number_tags'] = sanitize_text_field($new_instance['number_tags']);
            return $instance;
        }
    }

    function stt_tags_widget_func()
    {
        if (function_exists('stt_registration_widget')) {
            stt_registration_widget('stt_tags_widget');
        }
    }

    add_action('widgets_init', 'stt_tags_widget_func');
}