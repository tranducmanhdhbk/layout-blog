<?php
if (!class_exists('stt_video_popup_widget')) {
    class stt_video_popup_widget extends WP_Widget
    {
        public function __construct()
        {
            $widget_ops = array('classname' => 'stt_video_popup_widget', 'description' => __('Your site&#8217;s video popup.', 'layout-blog'));
            parent::__construct('stt_video_popup_widget', __('[Layout Blog] Video Popup', 'layout-blog'), $widget_ops);
        }

        public function widget($args, $instance)
        {
            $title = apply_filters('widget_title', empty($instance['title']) ? '' : $instance['title'], $instance, $this->id_base);
            extract(wp_parse_args($instance, array(
                'title' => '',
                'video_url' => '',


            )));
            if (empty($instance['video_url']))
                $instance['video_url'] = '#';

            $video_img = (!empty($instance['video_img'])) ? $instance['video_img'] : '';
            echo stt_esc_data($args['before_widget']);
            ?>
            <h4 class="stt-title"><?php echo esc_attr($instance['title']) ?></h4>
            <div class=" stt-video-popup">
                <?php
                $imageUrl = wp_get_attachment_image_url($video_img, 'full');

                ?>
                <a href="<?php echo esc_url($instance['video_url']) ?>" class="stt-video">

                    <img src="<?php echo esc_url($imageUrl) ?>" alt="Video background"
                         class="img-fluid background-image"/>
                    <div class="stt-gradient"></div>
                    <div class="play-icon">
                        <img src="<?php echo STT_ASSETS_URI ?>images/blog/ic-play.png" alt="<?php echo stt_get_alt_image() ?>" />
                    </div>
                </a>
            </div>
            <?php
            echo stt_esc_data($args['after_widget']);

        }

        public function update($new_instance, $old_instance)
        {
            $instance = $old_instance;
            $new_instance = wp_parse_args((array)$new_instance, array(
                'title' => '',
                'video_url' => '',
                'video_img' => ''
            ));
            $instance['title'] = strip_tags($new_instance['title']);
            $instance['video_url'] = sanitize_text_field($new_instance['video_url']);
            $instance['video_img'] = $new_instance['video_img'];

            return $instance;

        }

        public function form($instance)
        {
            $title = isset($instance['title']) ? esc_attr($instance['title']) : '';
            $video_url = isset($instance['video_url']) ? esc_attr($instance['video_url']) : '';
            $video_img = isset($instance['video_img']) ? $instance['video_img'] : '';
            $class_hide_remove = '';
            if (empty($video_img)) {
                $class_hide_remove = 'hide';
            }
            ?>
            <p>
                <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php echo esc_html__('Title:', 'layout-blog'); ?>
                    <input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>"
                           name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text"
                           value="<?php echo esc_attr($title); ?>"/></label></p>
            <p>
                <label for="<?php echo esc_attr($this->get_field_id('video_url')); ?>"><?php echo esc_html__('Video url:', 'layout-blog'); ?>
                    <input class="widefat" id="<?php echo esc_attr($this->get_field_id('video_url')); ?>"
                           name="<?php echo esc_attr($this->get_field_name('video_url')); ?>" type="text"
                           value="<?php echo esc_attr($video_url); ?>"/></label></p>
            <div class="video-img">
                <div class="upload-image">
                    <div class="thumb">
                        <?php
                        $img_src = wp_get_attachment_image_url($video_img, 'thumbnail');
                        if (!empty($img_src)) {
                            echo '<img src="' . esc_url($img_src) . '" alt="'. stt_get_alt_image() .'"/>';
                        }
                        ?>
                    </div>
                    <input type="hidden" value="<?php echo esc_attr($video_img) ?>" class="img_url"
                           name="<?php echo esc_attr($this->get_field_name('video_img')); ?>"/>
                    <a href="#" class="add-image"><?php echo esc_html__('Add image', 'layout-blog'); ?></a>
                    <a href="#"
                       class="remove-image right <?php echo esc_attr($class_hide_remove); ?>"><?php echo esc_html__('Remove image', 'layout-blog'); ?></a>
                </div>
            </div>

            <?php
        }
    }

    function stt_video_popup_widget_func()
    {
        if (function_exists('stt_registration_widget')) {
            stt_registration_widget('stt_video_popup_widget');
        }
    }

    add_action('widgets_init', 'stt_video_popup_widget_func');
}
