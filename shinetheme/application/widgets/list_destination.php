<?php

if (!class_exists('stt_list_destination_widget')) {
    class stt_list_destination_widget extends WP_Widget
    {

        public function __construct()
        {
            $widget_ops = array('classname' => 'stt_list_destination_widget', 'description' => __('Your site&#8217;s list destination.', 'layout-blog'));
            parent::__construct('stt_list_destination_widget', __('[Layout Blog] List Destination', 'layout-blog'), $widget_ops);
        }

        public function widget($args, $instance)
        {
            $title = apply_filters('widget_title', empty($instance['title']) ? '' : $instance['title'], $instance, $this->id_base);

            extract(wp_parse_args($instance, array(
                'title' => '',
                'blog_url' => '',
                'hide_empty' => 'no',

            )));

            if (empty($instance['number_des']))
                $instance['number_des'] = 'all';
            if (empty($instance['hide_empty']))
                $instance['hide_empty'] = 'no';

            if (empty($instance['widget_column']))
                $instance['widget_column'] = '2';

            if (empty($instance['blog_url']))
                $instance['blog_url'] = '#';

            echo stt_esc_data($args['before_widget']);
            if ($title) {
                echo stt_esc_data($args['before_title']) . esc_html($title) . stt_esc_data($args['after_title']);
            }
            ?>
            <?php

            $terms = get_terms(array(
                'taxonomy' => 'st_destinations',
                'number' => $instance['number_des'],
                'hide_empty' => $instance['hide_empty'] == 'yes' ? true : false,
                'meta_key' => 'stt_number_view',
                'orderby' => 'stt_number_view',
                'order' => 'DESC',
                'meta_type' => 'NUMERIC',

            ));

            $class_column = 'col-6';
            if ($instance['widget_column'] != '2') {
                $class_column = 'col-12';
            }

            if (!is_wp_error($terms)) {
                if (!empty($terms)) {
                    echo '<div class="row list_top_destinations">';
                    ?>
                    <?php
                    foreach ($terms as $k => $v) {
                        $link = get_term_link($v->term_id, 'st_destinations');
                        ?>
                        <div class=" <?php echo esc_attr($class_column) ?> destination-item">
                            <div class="thumb">
                                <?php
                                $imageID = stt_get_term_meta($v->term_id, 'stt_destination_image');
                                $imageUrl = wp_get_attachment_image_url($imageID, array(290,390));
                                ?>
                                <a href="<?php echo esc_url($link) ?>"><img src="<?php echo esc_url($imageUrl) ?>" alt="<?php echo stt_get_alt_image() ?>"></a>

                            </div>
                            <div class="description-name">
                                <a href="<?php echo esc_url($link) ?>"><p><?php echo esc_html($v->name) ?></p></a>
                            </div>
                        </div>
                        <?php
                    }
                    echo '</div>';
                }
            }
            echo stt_esc_data($args['after_widget']);
        }

        public function form($instance)
        {
            $instance = wp_parse_args((array)$instance, array(
                    'title' => '',
                    'blog_url' => '',
                    'hide_empty' => 'no',
                    'widget_column' => '2'

                )
            );
            $title = $instance['title'];
            $blog_url = $instance['blog_url'];
            $number_des = isset($instance['number_des']) ? esc_attr($instance['number_des']) : '';
            $hide_empty = $instance['hide_empty'];
            $widget_column = $instance['widget_column'];
            ?>
            <p>
                <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php echo esc_html__('Title:', 'layout-blog'); ?>
                    <input class="widefat"
                           id="<?php echo esc_attr($this->get_field_id('title')); ?>"
                           name="<?php echo esc_attr($this->get_field_name('title')); ?>"
                           type="text"
                           value="<?php echo esc_attr($title); ?>"/></label>
            </p>

            <p>
                <label for="<?php echo esc_attr($this->get_field_id('number_des')); ?>"><?php echo esc_html__('Number Of Destination:', 'layout-blog'); ?>
                    <input class="widefat" id="<?php echo esc_attr($this->get_field_id('number_des')); ?>"
                           name="<?php echo esc_attr($this->get_field_name('number_des')); ?>" type="text"
                           value="<?php echo esc_attr($number_des); ?>"/></label></p>

            <p><label for="<?php echo esc_attr($this->get_field_id('hide_empty')); ?>"><input class="widefat"
                                                                                              id="<?php echo esc_attr($this->get_field_id('hide_empty')); ?>"
                                                                                              name="<?php echo esc_attr($this->get_field_name('hide_empty')); ?>"
                                                                                              type="checkbox"
                                                                                              value="yes" <?php echo stt_esc_data($hide_empty) == 'yes' ? 'checked' : ''; ?> /><?php echo esc_html__('Hide Empty', 'layout-blog'); ?>
                </label></p>

            <p>
                <label for="<?php echo esc_attr($this->get_field_id('widget_column')); ?>"><?php echo esc_html__('Title:', 'layout-blog'); ?>
                    <select name="<?php echo esc_attr($this->get_field_name('widget_column')); ?>" class="widefat"
                            id="<?php echo esc_attr($this->get_field_id('widget_column')); ?>">
                        <option value="1" <?php echo selected(1, $widget_column); ?>><?php echo esc_html__('1 column', 'layout-blog'); ?></option>
                        <option value="2" <?php echo selected(2, $widget_column); ?>><?php echo esc_html__('2 columns', 'layout-blog'); ?></option>
                    </select>
                </label></p>

            <p>
                <label for="<?php echo esc_attr($this->get_field_id('blog_url')); ?>"><?php echo esc_html__('Blog URL:', 'layout-blog'); ?>
                    <input
                            class="widefat" id="<?php echo esc_attr($this->get_field_id('blog_url')); ?>"
                            name="<?php echo esc_attr($this->get_field_name('blog_url')); ?>" type="text"
                            value="<?php echo esc_attr($blog_url); ?>"/></label></p>
            <?php
        }

        public function update($new_instance, $old_instance)
        {
            $instance = $old_instance;
            $new_instance = wp_parse_args((array)$new_instance, array(
                'title' => '',
                'blog_url' => '',
                'hide_empty' => '',
                'widget_column' => '',
                'number_des' => ''
            ));
            $instance['title'] = sanitize_text_field($new_instance['title']);
            $instance['blog_url'] = sanitize_text_field($new_instance['blog_url']);
            $instance['hide_empty'] = sanitize_text_field($new_instance['hide_empty']);
            $instance['widget_column'] = sanitize_text_field($new_instance['widget_column']);
            $instance['number_des'] = sanitize_text_field($new_instance['number_des']);
            return $instance;
        }

    }

    function st_destination_widget_func()
    {
        if (function_exists('stt_registration_widget')) {
            stt_registration_widget('stt_list_destination_widget');
        }
    }

    add_action('widgets_init', 'st_destination_widget_func');
}