<?php
if (!class_exists('stt_instagram_widget')) {
    class stt_instagram_widget extends WP_Widget
    {
        public function __construct()
        {
            $widget_ops = array('classname' => 'stt_instagram_widget', 'description' => __("The list instagram for your site.", 'layout-blog'));
            parent::__construct('stt_instagram_widget', __("[Layout Blog] Instagram", 'layout-blog'), $widget_ops);
        }

        public function widget($args, $instance)
        {
            $title = apply_filters('widget_title', empty($instance['title']) ? '' : $instance['title'], $instance, $this->id_base);
            extract(wp_parse_args($instance, array(
                'title' => '',
            )));

            echo stt_esc_data($args['before_widget']);

            $listImageUrl = stt_get_instagram_images(get_option('stt_insta_user_name'), get_option('stt_number_of_photos'));
            ?>
            <div class="stt-instagram-content">
                <div class="stt-instagram-heading">
                    <h4><?php echo esc_attr($title) ?></h4>
                </div>
                <div class="stt-list-image">
                    <?php if (!empty($listImageUrl)) {
                        foreach ($listImageUrl as $ImageUrl) {
                            ?>
                            <div class="stt-image-item">
                                <a href="<?php echo esc_url($ImageUrl) ?>" data-toggle="stt-lightbox"
                                   data-gallery="stt-gallery">
                                    <div class="stt-ico-instagram">
                                        <img src="<?php echo STT_ASSETS_URI ?>images/image_instagram/ico_camera_insta.svg" alt="<?php echo stt_get_alt_image() ?>">
                                    </div>
                                    <div class="stt-instagram-gradient"></div>
                                    <img src="<?php echo esc_url($ImageUrl) ?>" class="img-fluid" alt="<?php echo stt_get_alt_image() ?>">
                                </a>

                            </div>

                        <?php }
                    } ?>
                </div>
                <div class="stt-instagram-follow">
                    <p><?php echo esc_html__('follow us ', 'layout-blog') ?><span class="st-instagram-text"><a
                                    href="<?php echo esc_url(get_option('stt_instagram_link')) ?>"
                                    target="_blank"><?php echo esc_html__('#GoAround', 'layout-blog') ?></a></span></p>
                </div>

            </div>
            <?php
            echo stt_esc_data($args['after_widget']);
        }

        public function form($instance)
        {
            $instance = wp_parse_args((array)$instance, array(
                    'title' => '',
                )
            );
            $title = $instance['title'];
            ?>
            <p>
                <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php echo esc_html__('Title:', 'layout-blog'); ?>
                    <input class="widefat"
                           id="<?php echo esc_attr($this->get_field_id('title')); ?>"
                           name="<?php echo esc_attr($this->get_field_name('title')); ?>"
                           type="text"
                           value="<?php echo esc_attr($title); ?>"/></label>
            </p>
            <?php
        }

        public function update($new_instance, $old_instance)
        {
            $instance = $old_instance;
            $new_instance = wp_parse_args((array)$new_instance, array(
                'title' => '',

            ));
            $instance['title'] = sanitize_text_field($new_instance['title']);
            return $instance;
        }
    }

    function stt_instagram_widget_func()
    {
        if (function_exists('stt_registration_widget')) {
            stt_registration_widget('stt_instagram_widget');
        }

    }

    add_action('widgets_init', 'stt_instagram_widget_func');
}
