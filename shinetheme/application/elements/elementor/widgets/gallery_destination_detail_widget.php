<?php

use Elementor\Widget_Base;

/**
 * Created by PhpStorm.
 * User: Jream
 * Date: 6/29/2019
 * Time: 9:54 PM
 */
class Elementor_Gallery_Destination_Widget extends Elementor\Widget_Base
{
    public function get_name()
    {
        // TODO: Implement get_name() method.
        return 'stt_gallery_destination';

    }

    public function get_title()
    {
        return __('Gallery Destination', 'layout-blog');
    }

    public function get_icon()
    {
        return 'fa fa-bars';
    }

    public function get_categories()
    {
        return ['stt_layout_blog'];
    }

    protected function _register_controls()
    {
        $this->start_controls_section(
            'content_section',
            [
                'label' => __('Settings', 'layout-blog'),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );
        $this->add_control(
            'title', [
                'label' => __('Title', 'layout-blog'),
                'type' => \Elementor\Controls_Manager::TEXT,
                'label_block' => true,
            ]
        );
        $this->end_controls_section();
    }

    protected function render()
    {
        $settings = $this->get_settings_for_display();
        $st = &stt_get_instance();
        $st->load->view('gallery', 'frontend/destination_detail', array('title' => $settings['title']));
    }
}