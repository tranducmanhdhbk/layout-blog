<?php
?>
<?php

use Elementor\Widget_Base;

class Elementor_Pages_Pagination_Widget extends Elementor\Widget_Base
{
    public function get_name()
    {
        // TODO: Implement get_name() method.
        return 'stt_page_pagination';

    }

    public function get_title()
    {
        return __('Pages Pagination', 'layout-blog');
    }

    public function get_icon()
    {
        return 'fas fa-magic';
    }

    public function get_categories()
    {
        return ['stt_layout_blog'];
    }

    protected function _register_controls()
    {
        $this->start_controls_section(
            'content_section',
            [
                'label' => esc_html__('Settings', 'layout-blog'),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );


        $this->end_controls_section();
    }

    protected function render()
    {
        $settings = $this->get_settings_for_display();
        $st = &stt_get_instance();
        $st->load->view('pages_pagination', 'frontend/destination_detail');
    }
}

