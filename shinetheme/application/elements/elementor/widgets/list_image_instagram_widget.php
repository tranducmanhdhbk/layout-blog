<?php

use Elementor\Widget_Base;

class Elementor_List_Image_Instagram_Widget extends Elementor\Widget_Base
{
    public function get_name()
    {
        // TODO: Implement get_name() method.
        return 'stt_list_image_instagram';

    }

    public function get_title()
    {
        return __('List Image Instagram', 'layout-blog');
    }

    public function get_icon()
    {
        return 'far fa-image';
    }

    public function get_categories()
    {
        return ['stt_layout_blog'];
    }

    protected function _register_controls()
    {
        $this->start_controls_section(
            'content_section',
            [
                'label' => esc_html__('Settings', 'layout-blog'),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );

        $this->add_control(
            'style', [
                'label' => esc_html__(' Choose Style', 'layout-blog'),
                'type' => \Elementor\Controls_Manager::SELECT,
                'options' => array(
                    'style1' => esc_html__('Style 1', 'layout-blog'),
                    'style2' => esc_html__('Style 2', 'layout-blog'),
                    'style3' => esc_html__('Style 3', 'layout-blog'),
                ),
                'default' => 'style1',
            ]
        );

        $this->add_control(
            'title', [
                'label' => __('Heading', 'layout-blog'),
                'type' => \Elementor\Controls_Manager::TEXT,

                'label_block' => true,
            ]
        );



        $this->end_controls_section();
    }

    protected function render()
    {
        $settings = $this->get_settings_for_display();
        $st = &stt_get_instance();
        $st->load->view('list_image_instagram', 'frontend/elements/elementor', array('style' => $settings['style'],
            'title' => $settings['title']));
    }
}
