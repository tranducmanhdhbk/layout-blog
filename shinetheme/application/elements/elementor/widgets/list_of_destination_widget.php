<?php

use Elementor\Widget_Base;

class Elementor_List_Of_Destination_Widget extends Elementor\Widget_Base
{
    public function get_name()
    {
        // TODO: Implement get_name() method.
        return 'stt_list_of_destination';

    }

    public function get_title()
    {
        return __('List Of Destination', 'layout-blog');
    }

    public function get_icon()
    {
        return 'fa fa-location-arrow';
    }

    public function get_categories()
    {
        return ['stt_layout_blog'];
    }

    public function get_script_depends()
    {
        return ['flickity'];
    }

    public function get_style_depends()
    {
        return ['flickity'];
    }

    protected function _register_controls()
    {
        $this->start_controls_section(
            'content_section',
            [
                'label' => esc_html__('Settings', 'layout-blog'),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );

        $this->add_control(
            'style', [
                'label' => esc_html__(' Choose Style', 'layout-blog'),
                'type' => \Elementor\Controls_Manager::SELECT,
                'options' => array(
                    'style1' => esc_html__('Style 1', 'layout-blog'),
                    'style2' => esc_html__('Style 2', 'layout-blog'),
                ),
                'default' => 'style1',
            ]
        );

        $this->add_control(
            'sub_title', [
                'label' => __('Sub Heading', 'layout-blog'),
                'type' => \Elementor\Controls_Manager::TEXT,
                'label_block' => true,
                'condition' => [
                    'style' => array('style1'),
                ],
            ]
        );

        $this->add_control(
            'title', [
                'label' => __('Heading', 'layout-blog'),
                'type' => \Elementor\Controls_Manager::TEXT,
                'condition' => [
                    'style' => array('style1'),
                ],
                'label_block' => true,
            ]
        );

        $this->add_control(
            'order_by', [
                'label' => esc_html__('Choose Order By','layout-blog'),
                'type' => \Elementor\Controls_Manager::SELECT,
                'options' => array(
                    'id' => esc_html__('ID', 'layout-blog'),
                    'name' => esc_html__('Name', 'layout-blog'),
                ),
                'condition' => [
                    'style' => array('style1'),
                ],
                'default' => 'id',
            ]
        );

        $this->add_control(
            'order', [
                'label' => esc_html__('Choose Order','layout-blog'),
                'type' => \Elementor\Controls_Manager::SELECT,
                'options' => array(
                    'asc' => esc_html__('ASC', 'layout-blog'),
                    'desc' => esc_html__('DESC', 'layout-blog'),
                ),
                'condition' => [
                    'style' => array('style1'),
                ],
                'default' => 'asc',
            ]
        );

        $this->add_control(
            'number', [
                'label' => esc_html__('Number of items', 'layout-blog'),
                'type' => \Elementor\Controls_Manager::NUMBER,
                'min' => 1,
                'step' => 1,
                'default' => 4,

            ]
        );

        $this->add_control(
            'des_ids', [
                'label' => __('IDs', 'layout-blog'),
                'type' => \Elementor\Controls_Manager::TEXT,
                'description' => esc_html__('Separated by commas', 'layout-blog'),
                'label_block' => true,
                'condition' => [
                    'style' => array('style1'),
                ],
            ]
        );
        $this->end_controls_section();
    }

    protected function render()
    {
        $settings = $this->get_settings_for_display();
        $st = &stt_get_instance();
        $st->load->view('list_of_destination', 'frontend/elements/elementor', array('style' => $settings['style'], 'order_by' => $settings['order_by'], 'order' => $settings['order'], 'number' => $settings['number'],
            'des_ids' => $settings['des_ids'], 'title' => $settings['title'], 'sub_title' => $settings['sub_title']));
    }
}
