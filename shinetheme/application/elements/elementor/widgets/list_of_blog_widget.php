<?php

class Elementor_List_Of_Blog_Widget extends Elementor\Widget_Base
{
    public function get_name()
    {
        // TODO: Implement get_name() method.
        return 'stt_list_of_blog';

    }

    public function get_title()
    {
        return __('List Of Blog', 'layout-blog');
    }

    public function get_icon()
    {
        return 'fa fa-rss';
    }

    public function get_categories()
    {
        return ['stt_layout_blog'];
    }

    public function get_script_depends()
    {
        return ['flickity'];
    }

    public function get_style_depends()
    {
        return ['flickity'];
    }

    protected function _register_controls()
    {
        $this->start_controls_section(
            'content_section',
            [
                'label' => esc_html__('Settings', 'layout-blog'),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );

        $this->add_control(
            'style', [
                'label' => esc_html__(' Choose Style', 'layout-blog'),
                'type' => \Elementor\Controls_Manager::SELECT,
                'options' => array(
                    'style1' => esc_html__('Style 1', 'layout-blog'),
                    'style2' => esc_html__('Style 2', 'layout-blog'),
                    'style3' => esc_html__('Style 3', 'layout-blog'),
                    'style4' => esc_html__('Style 4', 'layout-blog'),
                    'style5' => esc_html__('Style 5', 'layout-blog'),
                ),
                'default' => 'style1',
            ]
        );

        $this->add_control(
            'sub_title', [
                'label' => __('Sub Heading', 'layout-blog'),
                'type' => \Elementor\Controls_Manager::TEXT,
                'condition' => [
                    'style' => array('style2'),
                ],
                'label_block' => true,
            ]
        );

        $this->add_control(
            'title', [
                'label' => __('Heading', 'layout-blog'),
                'type' => \Elementor\Controls_Manager::TEXT,
                'condition' => [
                    'style' => array('style5','style2' ,'style3'),
                ],
                'label_block' => true,
            ]
        );

        $this->add_control(
            'order_by', [
                'label' => esc_html__('Choose Order By','layout-blog'),
                'type' => \Elementor\Controls_Manager::SELECT,
                'options' => array(
                    'id' => esc_html__('ID', 'layout-blog'),
                    'name' => esc_html__('Name', 'layout-blog'),
                    'random' => esc_html__('Ran Dom', 'layout-blog'),
                ),
                'condition' => [
                    'style' => array('style1', 'style2'),
                ],
                'default' => 'id',
            ]
        );

        $this->add_control(
            'order', [
                'label' => esc_html__('Choose Order','layout-blog'),
                'type' => \Elementor\Controls_Manager::SELECT,
                'options' => array(
                    'asc' => esc_html__('ASC', 'layout-blog'),
                    'desc' => esc_html__('DESC', 'layout-blog'),

                ),
                'condition' => [
                    'style' => array('style1', 'style2'),
                ],
                'default' => 'asc',
            ]
        );

        $this->add_control(
            'number', [
                'label' => esc_html__('Number of items', 'layout-blog'),
                'type' => \Elementor\Controls_Manager::NUMBER,
                'min' => 1,
                'step' => 1,
                'default' => 4,
                'condition' => [
                    'style' => array('style1', 'style2', 'style3'),
                ],
            ]
        );

        $this->add_control(
            'post_ids', [
                'label' => __('IDs', 'layout-blog'),
                'type' => \Elementor\Controls_Manager::TEXT,
                'description' => esc_html__('Separated by commas', 'layout-blog'),
                'condition' => [
                    'style' => array('style1', 'style2'),
                ],
                'label_block' => true,
            ]
        );
        $this->end_controls_section();
    }

    protected function render()
    {
        $settings = $this->get_settings_for_display();
        $st = &stt_get_instance();
        $st->load->view('list_of_blog', 'frontend/elements/elementor', array('style' => $settings['style'], 'order_by' => $settings['order_by'], 'order' => $settings['order'], 'number' => $settings['number'],
            'post_ids' => $settings['post_ids'], 'title' => $settings['title'], 'sub_title' => $settings['sub_title']));
    }
}
