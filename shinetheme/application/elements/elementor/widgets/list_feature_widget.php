<?php

/**
 * Created by PhpStorm.
 * User: Jream
 * Date: 6/29/2019
 * Time: 9:54 PM
 */
class Elementor_List_Feature_Widget extends Elementor\Widget_Base
{
    public function get_name()
    {
        // TODO: Implement get_name() method.
        return 'stt_list_feature';

    }

    public function get_title()
    {
        return __('List Of Feature', 'layout-blog');
    }

    public function get_icon()
    {
        return 'fas fa-feather-alt';
    }

    public function get_categories()
    {
        return ['stt_layout_blog'];
    }


    protected function _register_controls()
    {
        $this->start_controls_section(
            'content_section',
            [
                'label' => __('Settings', 'layout-blog'),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );
        $this->add_control(
            'style', [
                'label' => esc_html__(' Choose Style', 'layout-blog'),
                'type' => \Elementor\Controls_Manager::SELECT,
                'options' => array(
                    'style1' => esc_html__('Style 1', 'layout-blog'),
                    'style2' => esc_html__('Style 2', 'layout-blog'),
                ),
                'default' => 'style1',
            ]
        );

        $repeater = new \Elementor\Repeater();

        $repeater->add_control(
            'list_title', [
                'label' => __('Name of Feature', 'layout-blog'),
                'type' => \Elementor\Controls_Manager::TEXT,
                'label_block' => true,
            ]
        );

        $repeater->add_control(
            'list_image', [
                'label' => __('Thumbnail', 'layout-blog'),
                'type' => \Elementor\Controls_Manager::MEDIA,
                'default' => [
                    'url' => \Elementor\Utils::get_placeholder_image_src(),
                ],
            ]
        );

        $repeater->add_control(
            'list_description', [
                'label' => __('Description', 'layout-blog'),
                'type' => \Elementor\Controls_Manager::TEXT,
                'label_block' => true,
            ]
        );

        $this->add_control(
            'list_feature',
            [
                'label' => __('List of Feature', 'layout-blog'),
                'type' => \Elementor\Controls_Manager::REPEATER,
                'fields' => $repeater->get_controls(),
                'title_field' => '{{{ list_title }}}',
            ]
        );


        $this->end_controls_section();
    }

    protected function render()
    {
        $settings = $this->get_settings_for_display();
        $st = &stt_get_instance();
        $st->load->view('list_feature', 'frontend/elements/elementor', array('style' => $settings['style'], 'settings' => $settings['list_feature']));

    }
}