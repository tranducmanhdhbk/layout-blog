<?php

use Elementor\Widget_Base;

class Elementor_Author_Information_Widget extends Elementor\Widget_Base
{
    public function get_name()
    {
        // TODO: Implement get_name() method.
        return 'stt_auhtor_infor';

    }

    public function get_title()
    {
        return __('Author Information', 'layout-blog');
    }

    public function get_icon()
    {
        return 'far fa-user';
    }

    public function get_categories()
    {
        return ['stt_layout_blog'];
    }

    protected function _register_controls()
    {
        $this->start_controls_section(
            'content_section',
            [
                'label' => esc_html__('Settings', 'layout-blog'),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );

        $this->add_control(
            'style', [
                'label' => esc_html__(' Choose Style', 'layout-blog'),
                'type' => \Elementor\Controls_Manager::SELECT,
                'options' => array(
                    'style1' => esc_html__('Style 1', 'layout-blog'),
                    'style2' => esc_html__('Style 2', 'layout-blog'),
                ),
                'default' => 'style1',
            ]
        );
        $this->add_control(
            'author_logo', [
                'label' => __('Author Logo', 'layout-blog'),
                'type' => \Elementor\Controls_Manager::MEDIA,
                'default' => [
                    'url' => \Elementor\Utils::get_placeholder_image_src(),
                ],
            ]
        );
        $this->add_control(
            'logo', [
                'label' => __(' Logo', 'layout-blog'),
                'type' => \Elementor\Controls_Manager::MEDIA,
                'condition' => [
                    'style' => array('style1'),
                ],
                'default' => [
                    'url' => \Elementor\Utils::get_placeholder_image_src(),
                ],
            ]
        );
        $this->add_control(
            'description', [
                'label' => __('Description', 'layout-blog'),
                'type' => \Elementor\Controls_Manager::TEXT,
                'label_block' => true,
            ]
        );
        $this->add_control(
            'facebook', [
                'label' => __('Facebook', 'layout-blog'),
                'type' => \Elementor\Controls_Manager::TEXT,
                'label_block' => true,
            ]
        );
        $this->add_control(
            'instagram', [
                'label' => __('Instagram', 'layout-blog'),
                'type' => \Elementor\Controls_Manager::TEXT,
                'label_block' => true,
            ]
        );
        $this->add_control(
            'twitter', [
                'label' => __('Twitter', 'layout-blog'),
                'type' => \Elementor\Controls_Manager::TEXT,
                'label_block' => true,
            ]
        );


        $this->end_controls_section();
    }

    protected function render()
    {
        $settings = $this->get_settings_for_display();
        $st = &stt_get_instance();
        $st->load->view('author_information', 'frontend/elements/elementor', array('style' => $settings['style'], 'description' => $settings['description'], 'author_logo' => $settings['author_logo'], 'logo' => $settings['logo'],
            'facebook' => $settings['facebook'],'instagram' => $settings['instagram'],'twitter' => $settings['twitter']));
    }
}
