<?php

/**
 * Created by PhpStorm.
 * User: Jream
 * Date: 6/29/2019
 * Time: 9:54 PM
 */
class Elementor_Map_Widget extends Elementor\Widget_Base
{
    public function get_name()
    {
        // TODO: Implement get_name() method.
        return 'stt_map';

    }

    public function get_title()
    {
        return __('Map', 'layout-blog');
    }

    public function get_icon()
    {
        return 'fa fa-map-o';
    }

    public function get_categories()
    {
        return ['stt_layout_blog'];
    }

    protected function _register_controls()
    {
        $this->start_controls_section(
            'content_section',
            [
                'label' => __('Settings', 'layout-blog'),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );


        $this->add_control(
            'map_icon',
            [
                'label' => __('Icon Marker', 'layout-blog'),
                'type' => \Elementor\Controls_Manager::MEDIA,
            ]
        );

        $this->add_control(
            'map_lat',
            [
                'label' => __('Map Lat', 'layout-blog'),
                'type' => \Elementor\Controls_Manager::TEXT,
                'input_type' => 'text',

            ]
        );

        $this->add_control(
            'map_lng',
            [
                'label' => __('Map Lng', 'layout-blog'),
                'type' => \Elementor\Controls_Manager::TEXT,
                'input_type' => 'text',
            ]
        );

        $this->add_control(
            'map_zoom',
            [
                'label' => __('Map Zoom', 'layout-blog'),
                'type' => \Elementor\Controls_Manager::NUMBER,
                'input_type' => 'text',
                'default' => 13
            ]
        );

        $this->add_control(
            'map_control',
            [
                'label' => __('Show Controls', 'layout-blog'),
                'type' => \Elementor\Controls_Manager::SWITCHER,
                'default' => ''
            ]
        );

        $this->add_control(
            'map_label',
            [
                'label' => __('Show Label', 'layout-blog'),
                'type' => \Elementor\Controls_Manager::SWITCHER,
                'default' => '',
                'condition' => [
                    'map_style' => array('roadmap', 'satellite', 'hybrid', 'terrain'),

                ],
            ]
        );

        $this->add_control(
            'map_height',
            [
                'label' => __('Map Height', 'layout-blog'),
                'type' => \Elementor\Controls_Manager::NUMBER,
                'default' => '500'
            ]
        );

        $this->add_control(
            'map_style',
            [
                'label' => __('Map Type', 'layout-blog'),
                'type' => \Elementor\Controls_Manager::SELECT,
                'options' => array(
                    'roadmap' => esc_html__('Roadmap', 'layout-blog'),
                    'satellite' => esc_html__('Satellite', 'layout-blog'),
                    'hybrid' => esc_html__('Hybrid', 'layout-blog'),
                    'terrain' => esc_html__('Terrain', 'layout-blog'),
                    'custom_style' => esc_html__('Custom style', 'layout-blog'),
                ),
                'default' => 'roadmap',

            ]
        );

        $this->add_control(
            'map_custom',
            [
                'label' => __('Map Custom Style', 'layout-blog'),
                'type' => \Elementor\Controls_Manager::TEXTAREA,
                'condition' => [
                    'map_style' => 'custom_style',
                ],
                'rows' => '20',
                'default' => '[
    {
        "featureType": "all",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "weight": "2.00"
            }
        ]
    },
    {
        "featureType": "all",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#9c9c9c"
            }
        ]
    },
    {
        "featureType": "all",
        "elementType": "labels.text",
        "stylers": [
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "administrative.land_parcel",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#372305"
            },
            {
                "saturation": "-25"
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "all",
        "stylers": [
            {
                "color": "#f2f2f2"
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#ffffff"
            }
        ]
    },
    {
        "featureType": "landscape.man_made",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#ffffff"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "all",
        "stylers": [
            {
                "saturation": -100
            },
            {
                "lightness": 45
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#eeeeee"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#7b7b7b"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "color": "#ffffff"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "all",
        "stylers": [
            {
                "color": "#46bcec"
            },
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#c8d7d4"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#070707"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "color": "#ffffff"
            }
        ]
    }
]'
            ]
        );

        $this->end_controls_section();
    }

    protected function render()
    {
        $settings = $this->get_settings_for_display();

        $st = &stt_get_instance();

        $settings_arr = array(
            'map_icon' => $settings['map_icon'],
            'map_lat' => $settings['map_lat'],
            'map_lng' => $settings['map_lng'],
            'map_zoom' => $settings['map_zoom'],
            'map_control' => $settings['map_control'],
            'map_label' => $settings['map_label'],
            'map_style' => $settings['map_style'],
            'map_custom' => $settings['map_custom'],
            'map_height' => $settings['map_height'],

        );

        $st->load->view('map', 'frontend/elements/elementor', $settings_arr);
    }
}