<?php

use Elementor\Widget_Base;

/**
 * Created by PhpStorm.
 * User: Jream
 * Date: 6/29/2019
 * Time: 9:54 PM
 */
class Elementor_Tabs_Information_Destination_Widget extends Elementor\Widget_Base
{
    public function get_name()
    {
        // TODO: Implement get_name() method.
        return 'stt_tabs_infor_destination';

    }

    public function get_title()
    {
        return __('Tab information of destination ', 'layout-blog');
    }

    public function get_icon()
    {
        return 'fas fa-tasks';
    }

    public function get_categories()
    {
        return ['stt_layout_blog'];
    }


    protected function _register_controls()
    {
        $this->start_controls_section(
            'content_section',
            [
                'label' => __('Settings', 'layout-blog'),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );


        $repeater = new \Elementor\Repeater();

        $repeater->add_control(
            'list_title', [
                'label' => __('Name of Item', 'layout-blog'),
                'type' => \Elementor\Controls_Manager::TEXT,
                'label_block' => true,
            ]
        );

        $repeater->add_control(
            'link', [
                'label' => __('Link', 'layout-blog'),
                'type' => \Elementor\Controls_Manager::TEXT,
                'label_block' => true,
            ]
        );

        $this->add_control(
            'tabs',
            [
                'label' => __('List of Items', 'layout-blog'),
                'type' => \Elementor\Controls_Manager::REPEATER,
                'fields' => $repeater->get_controls(),
                'title_field' => '{{{ list_title }}}',
            ]
        );


        $this->end_controls_section();
    }

    protected function render()
    {
        $settings = $this->get_settings_for_display();
        $st = &stt_get_instance();
        $st->load->view('tabs_infor_destination', 'frontend/destination_detail', array('settings' => $settings['tabs']));

    }
}