<?php

/**
 * Created by PhpStorm.
 * User: Jream
 * Date: 6/29/2019
 * Time: 9:54 PM
 */
class Elementor_Banner_Widget extends Elementor\Widget_Base
{
    public function get_name()
    {
        // TODO: Implement get_name() method.
        return 'stt_banner';

    }

    public function get_title()
    {
        return __('Banner', 'layout-blog');
    }

    public function get_icon()
    {
        return 'fa fa-leaf';
    }

    public function get_categories()
    {
        return ['stt_layout_blog'];
    }

    protected function _register_controls()
    {
        $this->start_controls_section(
            'content_section',
            [
                'label' => __('Settings', 'layout-blog'),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );

        $this->add_control(
            'banner_bg',
            [
                'label' => __('Banner background', 'layout-blog'),
                'type' => \Elementor\Controls_Manager::MEDIA,
            ]
        );

        $this->end_controls_section();
    }

    protected function render()
    {
        $settings = $this->get_settings_for_display();

        $st = &stt_get_instance();


        $st->load->view('banner', 'frontend/elements/elementor', array('banner_bg' => $settings['banner_bg']));
    }
}