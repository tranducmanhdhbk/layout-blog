<?php
/**
 * @package WordPress
 * @subpackage Traveler
 * @since 1.0
 *
 * Initialize the custom Meta Boxes.
 *
 * Created by ShineTheme
 *
 */
$custom_metabox[] = array(
    'id' => 'demo_meta_box',
    'title' => __(' Meta Box', 'layout-blog'),
    'pages' => array('page'),
    'context' => 'normal',
    'priority' => 'high',
    'fields' => array(
        array(
            'label' => __('Information', 'layout-blog'),
            'id' => 'infomation_tab',
            'type' => 'tab'
        ),

        array(
            'label' => __('Gallery', 'layout-blog'),
            'id' => 'stt_gallery',
            'type' => 'gallery',
            'desc' => __('This is a gallery option type', 'layout-blog'),
        )
    )
);

