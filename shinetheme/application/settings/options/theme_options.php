<?php
/**
 * @package WordPress
 * @subpackage Traveler
 * @since 1.0
 *
 * Custom option theme option
 *
 * Created by ShineTheme
 *
 */
$custom_settings = array(

    'sections' => array(
        array(
            'id' => 'option_general',
            'title' =>esc_html__('General Options', 'layout-blog')
        ),
        array(
            'id' => 'option_style',
            'title' =>esc_html__('Styling Options', 'layout-blog')
        ),
    ),
    'settings' => array(
        /*---- .START GENERAL OPTIONS ----*/
        array(
            'id' => 'general_tab',
            'label' =>esc_html__('General Options', 'layout-blog'),
            'type' => 'tab',
            'section' => 'option_general',
        ),
        array(
            'id' => 'enable_user_online_noti',
            'label' =>esc_html__('User notification info', 'layout-blog'),
            'desc' =>esc_html__('Enable/disable online notification of user', 'layout-blog'),
            'type' => 'on-off',
            'section' => 'option_general',
            'std' => 'on'
        ),
        array(
            'id' => 'enable_last_booking_noti',
            'label' =>esc_html__('Last booking notification', 'layout-blog'),
            'desc' =>esc_html__('Enable/disable notification of last booking', 'layout-blog'),
            'type' => 'on-off',
            'section' => 'option_general',
            'std' => 'on'
        ),
        array(
            'id' => 'enable_user_nav',
            'label' =>esc_html__('User navigator', 'layout-blog'),
            'desc' =>esc_html__('Enable/disable user dashboard menu', 'layout-blog'),
            'type' => 'on-off',
            'section' => 'option_general',
            'std' => 'on'
        ),
        array(
            'id' => 'noti_position',
            'label' =>esc_html__('Notification position', 'layout-blog'),
            'desc' =>esc_html__('The position to appear notices', 'layout-blog'),
            'type' => 'select',
            'section' => 'option_general',
            'std' => 'topRight',
            'choices' => array(
                array(
                    'label' =>esc_html__('Top Right', 'layout-blog'),
                    'value' => 'topRight'
                ),
                array(
                    'label' =>esc_html__('Top Left', 'layout-blog'),
                    'value' => 'topLeft'
                ),
                array(
                    'label' =>esc_html__('Bottom Right', 'layout-blog'),
                    'value' => 'bottomRight'
                ),
                array(
                    'label' =>esc_html__('Bottom Left', 'layout-blog'),
                    'value' => 'bottomLeft'
                )
            ),
        ),
        array(
            'id' => 'admin_menu_normal_user',
            'label' =>esc_html__('Normal user adminbar', 'layout-blog'),
            'desc' =>esc_html__('Show/hide adminbar for user', 'layout-blog'),
            'type' => 'on-off',
            'section' => 'option_general',
            'std' => 'off'
        ),
        array(
            'id' => 'once_notification_per_each_session',
            'label' =>esc_html__('Only show notification for per session', 'layout-blog'),
            'desc' =>esc_html__('Only show the unique notification for each user\'s session', 'layout-blog'),
            'type' => 'on-off',
            'section' => 'option_general',
            'std' => 'off'
        ),
        array(
            'id' => 'st_weather_temp_unit',
            'label' =>esc_html__('Weather unit', 'layout-blog'),
            'desc' =>esc_html__('The unit of weather- you can use Fahrenheit or Celsius or Kelvin', 'layout-blog'),
            'type' => 'select',
            'section' => 'option_general',
            'std' => 'c',
            'choices' => array(
                array(
                    'label' =>esc_html__('Fahrenheit (f)', 'layout-blog'),
                    'value' => 'f'
                ),
                array(
                    'label' =>esc_html__('Celsius (c)', 'layout-blog'),
                    'value' => 'c'
                ),
                array(
                    'label' =>esc_html__('Kelvin (k)', 'layout-blog'),
                    'value' => 'k'
                ),
            ),
        ),
        array(
            'id' => 'search_enable_preload',
            'label' =>esc_html__('Preload option', 'layout-blog'),
            'desc' =>esc_html__('Enable Preload when loading site', 'layout-blog'),
            'type' => 'on-off',
            'section' => 'option_general',
            'std' => 'on'
        ),
        array(
            'id' => 'search_preload_image',
            'label' =>esc_html__('Preload image', 'layout-blog'),
            'desc' =>esc_html__('This is the background for preload', 'layout-blog'),
            'type' => 'upload',
            'section' => 'option_general',
            'condition' => 'search_enable_preload:is(on)'
        ),
        array(
            'id' => 'search_preload_icon_default',
            'label' =>esc_html__('Customize preloader icon', 'layout-blog'),
            'desc' =>esc_html__('Using custom preload icon', 'layout-blog'),
            'type' => 'on-off',
            'section' => 'option_general',
            'condition' => 'search_enable_preload:is(on)',
            'std' => 'off'
        ),
        array(
            'id' => 'search_preload_icon_custom',
            'label' =>esc_html__('Upload custom preload image', 'layout-blog'),
            'desc' =>esc_html__('This is the image for preload', 'layout-blog'),
            'type' => 'upload',
            'section' => 'option_general',
            'operator' => 'and',
            'condition' => 'search_preload_icon_default:is(on),search_enable_preload:is(on)'
        ),

        array(
            'id' =>'header_id',
            'label' =>esc_html__('Header','layout-blog'),
            'type' =>'tab',
            'section' => 'option_general',
        ),
        array(
            'id' => 'header',
            'label' =>esc_html__('Choose Header','layout-blog'),
            'type' => 'radio_image',
            'choices' => [
                [
                    'value' => 'style-1',
                    'label' =>esc_html__('Style 1', 'layout-blog'),
                    'src' => STT_ASSETS_URI. 'images/header_style/header_style1.png'
                ],
            ],
            'section' => 'option_general',
        ),
        array(
            'id' => 'logo_tab',
            'label' =>esc_html__('Logo', 'layout-blog'),
            'type' => 'tab',
            'section' => 'option_general',


        ),
        array(
            'id' => 'logo',
            'label' =>esc_html__('Logo options', 'layout-blog'),
            'desc' =>esc_html__('To change logo', 'layout-blog'),
            'type' => 'upload',
            'section' => 'option_general',
        ),

        array(
            'id' => '404_tab',
            'label' =>esc_html__('404 Options', 'layout-blog'),
            'type' => 'tab',
            'section' => 'option_general',
        ),
        array(
            'id' => '404_bg',
            'label' =>esc_html__('Background for 404 page', 'layout-blog'),
            'desc' =>esc_html__('To change background for 404 error page', 'layout-blog'),
            'type' => 'upload',
            'section' => 'option_general',
        ),
        array(
            'id' => '404_text',
            'label' =>esc_html__('Text of 404 page', 'layout-blog'),
            'desc' =>esc_html__('To change text for 404 page', 'layout-blog'),
            'type' => 'textarea',
            'rows' => '3',
            'section' => 'option_general',
        ),
       /*---- .END GENERAL OPTIONS ----*/
        array(
            'id' => 'advance_tab',
            'label' =>esc_html__('Advance Options', 'layout-blog'),
            'type' => 'tab',
            'section' => 'option_general',
        ),
        array(
            'id' => 'google_map_key',
            'label' =>esc_html__('Enter Your Google Key', 'layout-blog'),
            'desc' =>'',
            'std' => '',
            'type' => 'text',
            'section' => 'option_general',
            'class' => '',
        ),

        /*---- .START STYLE OPTIONS ----*/
        array(
            'id' => 'general_style_tab',
            'label' =>esc_html__('General', 'layout-blog'),
            'type' => 'tab',
            'section' => 'option_style',
        ),
        array(
            'id' => 'right_to_left',
            'label' =>esc_html__('Right to left mode', 'layout-blog'),
            'desc' =>esc_html__('Enable "Right to let" displaying mode for content', 'layout-blog'),
            'type' => 'on-off',
            'section' => 'option_style',
            'output' => '',
            'std' => 'off'
        ),
        array(
            'id' => 'style_layout',
            'label' =>esc_html__('Layout', 'layout-blog'),
            'desc' =>esc_html__('You can choose wide layout or boxed layout', 'layout-blog'),
            'type' => 'select',
            'section' => 'option_style',
            'choices' => array(
                array(
                    'value' => 'wide',
                    'label' =>esc_html__('Wide', 'layout-blog')
                ),
                array(
                    'value' => 'boxed',
                    'label' =>esc_html__('Boxed', 'layout-blog')
                )

            )
        ),
        array(
            'id' => 'typography',
            'label' =>esc_html__('Typography, Google Fonts', 'layout-blog'),
            'desc' =>esc_html__('To change the display of text', 'layout-blog'),
            'type' => 'typography',
            'section' => 'option_style',
            'output' => 'body'
        ),
        array(
            'id' => 'google_fonts',
            'label' =>esc_html__('Google Fonts', 'layout-blog'),
            'type' => 'google-fonts',
            'section' => 'option_style',
        ),
        array(
            'id' => 'star_color',
            'label' =>esc_html__('Star color', 'layout-blog'),
            'desc' =>esc_html__('To change the color of star hotel', 'layout-blog'),
            'type' => 'colorpicker',
            'section' => 'option_style',
        ),
        array(
            'id' => 'body_background',
            'label' =>esc_html__('Body Background', 'layout-blog'),
            'desc' =>esc_html__('To change the color, background image of body', 'layout-blog'),
            'type' => 'background',
            'section' => 'option_style',
            'output' => 'body',
            'std' => array(
                'background-color' => "",
                'background-image' => "",
            )
        ),
        array(
            'id' => 'main_wrap_background',
            'label' =>esc_html__('Wrap background', 'layout-blog'),
            'desc' =>esc_html__('To change background color, bachground image of box surrounding the content', 'layout-blog'),
            'type' => 'background',
            'section' => 'option_style',
            'output' => '.global-wrap',
            'std' => array(
                'background-color' => "",
                'background-image' => "",
            )
        ),
        array(
            'id' => 'style_default_scheme',
            'label' =>esc_html__('Default Color Scheme', 'layout-blog'),
            'desc' =>esc_html__('Select  available color scheme to display', 'layout-blog'),
            'type' => 'select',
            'section' => 'option_style',
            'output' => '',
            'std' => '',
            'choices' => array(
                array('label' => '-- Please Select ---', 'value' => ''),
                array('label' => 'Bright Turquoise', 'value' => '#0EBCF2'),
                array('label' => 'Turkish Rose', 'value' => '#B66672'),
                array('label' => 'Salem', 'value' => '#12A641'),
                array('label' => 'Hippie Blue', 'value' => '#4F96B6'),
                array('label' => 'Mandy', 'value' => '#E45E66'),
                array('label' => 'Green Smoke', 'value' => '#96AA66'),
                array('label' => 'Horizon', 'value' => '#5B84AA'),
                array('label' => 'Cerise', 'value' => '#CA2AC6'),
                array('label' => 'Brick red', 'value' => '#cf315a'),
                array('label' => 'De-York', 'value' => '#74C683'),
                array('label' => 'Shamrock', 'value' => '#30BBB1'),
                array('label' => 'Studio', 'value' => '#7646B8'),
                array('label' => 'Leather', 'value' => '#966650'),
                array('label' => 'Denim', 'value' => '#1A5AE4'),
                array('label' => 'Scarlet', 'value' => '#FF1D13'),
            )
        ),
        array(
            'id' => 'main_color',
            'label' =>esc_html__('Main Color', 'layout-blog'),
            'desc' =>esc_html__('To change the main color for web', 'layout-blog'),
            'type' => 'colorpicker',
            'section' => 'option_style',
            'std' => '#ed8323',

        ),
    )
);