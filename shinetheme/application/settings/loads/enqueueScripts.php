<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 3/22/2019
 * Time: 3:26 PM
 */
if (!class_exists('ST_EnqueueScripts')) {
    class STT_EnqueueScripts extends STT_Controller
    {
        private $googleKey = '';

        public function __construct()
        {
            parent::__construct();
            add_action('admin_enqueue_scripts', [$this, 'enqueueDAdminScripts'], 1);
            add_action('wp_enqueue_scripts', [$this, 'registerFrontendScripts']);
            add_filter('ajax_query_attachments_args', [$this, 'filterMedia']);
            add_filter('upload_mimes', [$this, 'my_myme_types'], 10, 1);
            add_action('wp_enqueue_scripts', [$this, 'customCss'], 9);
        }

        public function registerFrontendScripts()
        {
            $this->googleKey = stt_get_option('google_map_key');
            wp_enqueue_script('google-map', 'https://maps.googleapis.com/maps/api/js?key=' . esc_attr($this->googleKey) . '&libraries=places', [], null, true);
            wp_enqueue_script('bootstrap', STT_ASSETS_URI . 'dashboard/js/bootstrap.bundle.min.js', ['jquery'], null, true);
            wp_enqueue_script('font-awesome', STT_ASSETS_URI . 'js/font-awesome-5.10.1/all.min.js', ['jquery'], null, true);
            wp_enqueue_script('owlcarousel', STT_ASSETS_URI . 'js/owlcarousel/owl.carousel.min.js', ['jquery'], null, true);
            wp_enqueue_script('flickity', STT_ASSETS_URI . 'js/flickity.pkgd.min.js', ['jquery'], null, true);
            wp_enqueue_script('magnific-popup', STT_ASSETS_URI . 'js/magnific-popup/jquery.magnific-popup.min.js', ['jquery'], null, true);
            wp_enqueue_script('moment-daterangepicker', STT_ASSETS_URI . 'dashboard/js/moment.min.js', ['jquery'], null, true);
            wp_enqueue_script('daterangepicker', STT_ASSETS_URI . 'dashboard/js/daterangepicker/daterangepicker.js', ['jquery'], null, true);
            wp_localize_script('jquery', 'st_params', [
                'ajaxurl' => admin_url('admin-ajax.php'),
                'security' => wp_create_nonce('security')
            ]);
            wp_enqueue_script('frontend', STT_ASSETS_URI . 'js/custom.js', ['jquery'], null, true);
            //css plugin
            wp_enqueue_style('bootstrap', STT_ASSETS_URI . 'dashboard/css/bootstrap.min.css');
            wp_enqueue_style('owlcarousel', STT_ASSETS_URI . 'js/owlcarousel/assets/owl.carousel.min.css');
            wp_enqueue_style('flickity', STT_ASSETS_URI . 'css/flickity/flickity.css');
            wp_enqueue_style('magnific-popup', STT_ASSETS_URI . 'js/magnific-popup/magnific-popup.css');
            wp_enqueue_style('font-awesome', STT_ASSETS_URI . 'css/font-awesome-5.10.1/css/font-awesome.min.css');
            wp_enqueue_style('daterangepicker', STT_ASSETS_URI . 'dashboard/js/daterangepicker/daterangepicker.css');
            // css custom
            wp_enqueue_style('frontend', STT_ASSETS_URI . 'css/main.css');
            wp_enqueue_style('layout-blog-style', get_stylesheet_uri());

            if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
                wp_enqueue_script( 'comment-reply' );
            }

        }

        public function enqueueDAdminScripts()
        {
            wp_enqueue_script('admin', STT_ASSETS_URI . 'backend/js/main.js');
            wp_enqueue_style('admin', STT_ASSETS_URI . 'backend/css/main.css');
            wp_localize_script('jquery', 'st_params', [
                'ajaxurl' => admin_url('admin-ajax.php'),
                'security' => wp_create_nonce('security'),
                'isValidated' => new stdClass(), // used for bootstrap validation js,
                'layout_blog_css' => $this->load->customView('main.color', 'configs', ['color' => '__maincolor__'], true),
            ]);

        }
        public function filterMedia($query)
        {
            if (is_admin() && !current_user_can('manage_options'))
                $query['author'] = get_current_user_id();

            return $query;
        }

        public function customCss()
        {
            $contents = '';

            $css_php = $this->load->customView('main.color', 'configs', null, true);

            echo '<style id="blog_custom_css_php" type="text/css">' . stt_esc_data($css_php) . '</style>';

            $css = get_option('custom_css', '');
            $contents .= $css;

            $contents = apply_filters('stt_custom_css_content', $contents);

            echo '<style type="text/css">' . stt_esc_data($contents) . '</style>';
        }

        public function my_myme_types($mime_types)
        {
            $mime_types['svg'] = 'image/svg+xml'; //Adding svg extension
            return $mime_types;
        }

        public static function get_inst()
        {
            static $instance;
            if (is_null($instance)) {
                $instance = new self();
            }

            return $instance;
        }
    }

    STT_EnqueueScripts::get_inst();
}
