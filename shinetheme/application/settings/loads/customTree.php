<?php
/**
 * @package WordPress
 * @subpackage Layout Blog
 * @since 1.0
 *
 * Class STOptiontree
 *
 * Created by ShineTheme
 *
 */

if (!class_exists('STOptiontree')) {
    class STOptiontree
    {
        public $theme;

        function __construct()
        {

            add_action('after_setup_theme', array($this, 'custom_theme_options'));
            if (!class_exists('OT_Loader')) return;


            add_action('admin_init', array($this, 'custom_meta_boxes'));

            $this->theme = wp_get_theme();

            add_filter('ot_header_version_text', array($this, 'opt_header_version_text'));
            add_filter('ot_theme_options_menu_slug', array($this, 'change_menu_slug'));
            add_filter('ot_theme_options_icon_url', array($this, 'change_icon_url'));
            add_filter('ot_theme_options_parent_slug', array($this, 'change_parent_slug'));
            add_filter('ot_theme_options_menu_title', array($this, 'change_menu_title'));
            add_filter('ot_theme_options_position', array($this, 'change_position'));
            add_filter('ot_header_logo_link', array($this, 'change_header_logo_link'));

            add_filter('ot_show_pages', '__return_false');

        }

        function change_header_logo_link()
        {
            return "<a ><img alt='" . stt_get_alt_image() . "' src='" . get_template_directory_uri() . "/assets/backend/images/Logo_Shinetheme.png'></a>";
        }

        function change_position()
        {
            if (class_exists('Envato_WP_Toolkit')) {
                return 59;
            }
            return 58;
        }

        function change_menu_title()
        {
            return apply_filters('change_menu_settings_title', __(' Blog Settings', 'layout-blog'));
        }

        function change_parent_slug()
        {
            return '';
        }

        function change_icon_url()
        {
            return "dashicons-st-traveler";
        }

        function change_menu_slug($slug)
        {
            return apply_filters('change_menu_settings_slug', 'st_traveler_option');
        }

        function opt_header_version_text($title)
        {
            $title = esc_html($this->theme->display('Name'));
            $title .= ' - ' . sprintf(__('Version %s', 'layout-blog'), $this->theme->display('Version'));

            return $title;
        }


        function custom_meta_boxes()
        {
            /**
             * Get a copy of the saved settings array.
             */
            $custom_metabox = array();

            include_once STT_APPLICATION_PATH . 'settings/options/metabox.php';

            /**
             * Register our meta boxes using the
             * ot_register_meta_box() function.
             */
            if (function_exists('ot_register_meta_box')) {
                if (!empty($custom_metabox)) {
                    foreach ($custom_metabox as $value) {
                        ot_register_meta_box($value);
                    }
                }
            }

        }

        function custom_theme_options()
        {
            if(function_exists('ot_settings_id')){
                $custom_settings = array();
                include_once STT_APPLICATION_PATH . 'settings/options/theme_options.php';
                update_option(ot_settings_id(), $custom_settings);
            }
        }

        static function getOption($key = '', $default = '')
        {
            $options = get_option(ot_options_id(), []);
            return (isset($options[$key]) && !empty($options[$key])) ? maybe_unserialize($options[$key]) : $default;
        }

    }

    new STOptiontree();
}
