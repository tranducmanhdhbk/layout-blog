<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 15/12/2016
 * Time: 2:51 PM
 * Since 1.0
 * Updated 1.0
 */
if (!class_exists('STT_ThemeSettings')) {
    class STT_ThemeSettings extends STT_Controller
    {
        /**
         * @var array
         */
        private $load_files = [];
        /**
         * @var null|string
         */
        private $path = null;

        /**
         * STT_ThemeSettings constructor.
         */
        public function __construct()
        {
            parent::__construct();
            $this->path = ST_APPLICATION . 'settings';

            $this->load_files = [
                'enqueueScripts',
                'customTree',
            ];
            $this->load();
            $this->loadVC();
            $this->loadWidgets();
        }

        /**
         * @since   1.0.0
         * @updated 1.0.0
         */
        public function load()
        {
            if (!empty($this->load_files)) {
                foreach ($this->load_files as $name) {
                    $file = locate_template($this->path . '/' . 'loads' . '/' . $name . '.php');
                    if (is_file($file)) {
                        require_once($file);
                    }
                }
            }
        }

        public function loadWidgets()
        {
            $files = array_filter( glob( STT_APPLICATION_PATH . 'widgets/*' ), 'is_file' );
            if(!empty($files)){
                foreach ( $files as $key => $value ) {
                    require_once($value);
                }
            }
        }

	    public function loadElementor()
	    {
		    $files = array_filter( glob( STT_APPLICATION_PATH . 'elements/elementor/*' ), 'is_file' );
		    if(!empty($files)){
			    foreach ( $files as $key => $value ) {
				    require_once($value);
			    }
		    }
	    }

        public function loadVC()
        {
            if (function_exists('vc_map')) {
                $path = ST_APPLICATION . 'elements/vc';
                $file_vc_map = locate_template($path . '/vc_map.php');
                if (is_file($file_vc_map)) {
                    require_once($file_vc_map);
                }

                $file_vc_shortcode = locate_template($path . '/shortcodes.php');
                if (is_file($file_vc_shortcode)) {
                    require_once($file_vc_shortcode);
                }
            }
        }

        public static function get_inst()
        {
            static $instance;
            if (is_null($instance)) {
                $instance = new self();
            }

            return $instance;
        }
    }

    STT_ThemeSettings::get_inst();
}
