<?php
if($number <0){
    $number = -1;
}

if($order_by == 'id'){
    $orderBy = 'ID';
}else if( $order_by == 'name'){
    $orderBy = 'name';
}

if($order == 'asc'){
    $stOrder = 'ASC';
}else{
    $stOrder = 'DESC';
}

$args = array(
    'taxonomy' => 'category',
    'hide_empty' => false,
    'orderby' => $orderBy,
    'order' => $stOrder,
    'number' => $number,
);

$query = get_terms($args);
?>
<?php stt_breadcrumbs(true);  ?>
<div class=" stt-list-categorries">

   <?php foreach ($query as $value){
       $link = get_term_link($value->term_id, 'category');
       ?>
       <div class=" category-item">
               <div class="thumb">
                   <?php $imageID = stt_get_term_meta($value->term_id,'stt_cate_image');
                   $imageUrl = wp_get_attachment_image_url($imageID,array(100,100));
                   if(!empty($imageUrl)){ ?>
                       <a href="<?php echo esc_url($link) ?>"><img src="<?php echo esc_url($imageUrl) ?>" alt="<?php echo stt_get_alt_image() ?>"></a>
                   <?php }?>
               </div>
               <div class="category-name"><a href="<?php echo esc_url($link) ?>"><span><?php echo esc_html($value->name) ?></span></a></div>
       </div>
    <?php } ?>
</div>
