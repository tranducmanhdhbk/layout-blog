<?php
global $post;

?>
<div class="blog-item">
    <?php
    if (has_post_thumbnail()) {
        ?>
        <div class="thumb">
            <a href="<?php echo esc_url(get_the_permalink()); ?>">
                <?php the_post_thumbnail(array(740, 555)); ?>
            </a>
        </div>

    <?php }
    ?>
    <div class="content">
        <div class="stt-taxonomy-blog clearfix">
            <div class="stt-category">
                <?php
                $stCategory = get_the_terms(get_the_ID(), 'category');

                shuffle($stCategory);
                if (!empty($stCategory)) {
                    $c = $stCategory[0];
                    $link = get_category_link($c->term_id);
                }
                ?>
                <a href="<?php echo esc_url($link) ?>"><?php echo esc_html($c->name) ?></a>
            </div>
            <div class="stt-destination">
                <?php
                $stDestination = get_the_terms(get_the_ID(), 'st_destinations');
                if (!empty($stDestination)) {
                    $d = $stDestination[0];
                    $link = get_term_link($d->term_id, 'st_destinations');
                    echo '<a href="' . esc_url($link) . '">' . esc_html($d->name) . '</a>';
                }
                ?>
            </div>
        </div>
        <h4><a href="<?php echo esc_url(get_permalink($post->ID)); ?>"><?php echo esc_html(get_the_title()); ?></a>
        </h4>
        <p class="text-muted">
            <?php echo esc_html__('By ', 'layout-blog'); ?> <span
                    class="stt-author"><?php echo esc_html(stt_username($post->post_author)) ?></span>
            <span><?php the_time(get_option('date_format')) ?></span>
        </p>
        <p class="stt-blog-description"><?php echo esc_html(get_the_excerpt()); ?></p>
    </div>
</div>