<?php
?>
<div class="blog-item">
    <?php
    if(has_post_thumbnail()){
        ?>
        <div class="thumb">
            <a href="<?php echo esc_url(get_the_permalink()); ?>">
                <?php the_post_thumbnail(array(759,506)); ?>
            </a>
            <div class="content">
                <ul>
                    <?php
                    $stDestination = get_the_terms(get_the_ID(),'st_destinations');

                    if(!empty($stDestination) && !is_wp_error($stDestination) ){
                       if(  count($stDestination)>1){
                           $v = esc_html__('many destinations','layout-blog');
                       }else{
                           $v = $stDestination[0]->name;
                       }
                    }
                    ?>
                    <li class="date"><?php the_time('d F Y')?></li>
                    <li class="destination"><a href="#"><?php echo esc_html($v) ?></a></li>
                </ul>
                <h3><a href="<?php echo esc_url(get_the_permalink()); ?>"><?php echo esc_html(get_the_title()); ?></a></h3>
            </div>
            <div class="stt-gradient"></div>
        </div>
   <?php }
    ?>

</div>