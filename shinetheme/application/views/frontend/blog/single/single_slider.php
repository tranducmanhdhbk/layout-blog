<?php
global $post;

$postImageID = stt_get_meta($post->ID, 'stt_post_image');
if (!empty($postImageID)) {
    $listImageID = explode(',', $postImageID);
}


?>
<div class="blog-slider">
    <div class="owl-carousel">
        <?php
        if (!empty($listImageID)) {
            foreach ($listImageID as $k => $v) {
                $imageUrl = wp_get_attachment_image_url($v, [770, 580]);
                echo '<img class="img-responsive" src="' . esc_url($imageUrl) . '" alt="' . stt_get_alt_image() . '">';
            }
        } ?>
    </div>
    <?php
    if (!empty($listImageID)) {
        ?>
        <button class="Blog-Next" type="button"></button>
    <?php } ?>
</div>
<div class="stt-taxonomy-blog clearfix">
    <div class="stt-category">
        <?php
        $stCategory = get_the_terms($post->ID, 'category');
        if (!empty($stCategory)) {
            $c = $stCategory[0];
            $link = get_term_link($c->term_id, 'category');
            ?>
            <a href="<?php echo esc_url($link) ?>"><?php echo esc_html($c->name) ?></a>
            <?php
        }
        ?>

    </div>
    <div class="stt-destination">
        <?php
        $stDestination = get_the_terms($post->ID, 'st_destinations');
        if (!empty($stDestination)) {
            $d = $stDestination[0];
            $link = get_term_link($d->term_id, 'st_destinations');
            ?>
            <a href="<?php echo esc_url($link) ?>"><?php echo esc_html($d->name) ?></a>
            <?php
        }
        ?>

    </div>
</div>
