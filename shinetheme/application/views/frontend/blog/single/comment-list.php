<?php
/**
 * Created by PhpStorm.
 * User: HanhDo
 * Date: 7/29/2019
 * Time: 4:31 PM
 */
$GLOBALS[ 'comment' ]  = $comment;
$comment_id            = $comment->comment_ID;
$user_id               = $comment->user_id;
$args[ 'avatar_size' ] = 60;
if ( 'pingback' == $comment->comment_type || 'trackback' == $comment->comment_type ) : ?>
    <li id="comment-<?php comment_ID(); ?>" <?php comment_class(); ?>>
    <div class="comment-body">
        <?php comment_author_link(); ?><?php edit_comment_link( esc_html__('Edit', 'layout-blog'), '<span class="edit-link"><i class="fa fa-pencil-square-o"></i>', '</span>' ); ?>
    </div>
<?php else :
    $comment_class = '';
    empty( $args[ 'has_children' ] ) ? '' : $comment_class .= 'parent';
    ?>
<li id="comment-<?php comment_ID(); ?>" <?php comment_class( $comment_class ); ?>>
    <div id="div-comment-<?php comment_ID(); ?>" class="article comment  clearfix" >
        <div class="comment-item-head">
            <div class="media">
                <div class="media-left rounded-circle">
                    <?php echo stt_get_avatar( $user_id, $args[ 'avatar_size' ] ); ?>
                </div>
                <div class="media-body">
                    <div class="media-user">
                        <?php
                        if(empty($user_id)){
                            ?>
                            <p class="media-heading"><?php echo esc_html($comment->comment_author); ?></p>
                            <?php
                        }else{
                            ?>
                            <p class="media-heading"><?php echo esc_html(stt_username( $user_id )); ?></p>
                            <?php
                        }
                        ?>
                        <div class="date"><?php echo esc_html(get_comment_date( 'j F Y', $comment_id )) ?></div>
                    </div>
                    <div class="comment-item-body">
                        <?php if ( '0' == $comment->comment_approved ) : ?>
                            <p class="comment-awaiting-moderation"><?php echo esc_html__( 'Your comment is waiting for moderation.', 'layout-blog' ) ?></p>
                        <?php endif; ?>
                        <div class="comment-content">
                            <?php comment_text(); ?>
                        </div>
                        <span class="comment-reply">
                    <?php comment_reply_link( array_merge( $args, [ 'add_below' => 'div-comment', 'depth' => $depth, 'max_depth' => $args[ 'max_depth' ] ] ) ); ?>
            </span>
                    </div>
                </div>
            </div>
        </div>

    </div>
<?php
endif;