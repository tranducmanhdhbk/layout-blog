<?php
global $post;
?>
<div class="blog-title">
    <h1><?php echo esc_html(get_the_title()) ?>
        <?php
        if (is_sticky(get_the_ID())) {
            ?>
            <span class="post-featured"><?php echo esc_html__('FEATURED', 'layout-blog'); ?></span>
            <?php
        }
        ?>
    </h1>
    <div class="text-muted">
       <span class="stt-author"><?php echo esc_html__('By ', 'layout-blog');
           echo esc_html(stt_username($post->post_author)) ?></span>
        <span><?php the_time(get_option('date_format')) ?></span>
    </div>
</div>
