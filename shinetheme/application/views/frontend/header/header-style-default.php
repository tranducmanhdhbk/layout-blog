<?php

$st= &stt_get_instance();
?>
<div class="container-fluid">
    <div class="row  stt-header-swapper">
        <div class="stt-header-content d-flex justify-content-between <?php if(is_front_page()){ ?> border-0 <?php } ?> ">
            <div  class="toggle-menu">
                <img src="<?php echo STT_ASSETS_URI ?>images/ico_menu-options.svg" alt="<?php echo stt_get_alt_image() ?>">
            </div>
            <div class="stt-header-left">
                <div class="stt-logo">
                        <h1><a class="no-logo " href="<?php echo esc_url(home_url('/')) ?>"><?php echo esc_html(get_bloginfo('name')); ?></a></h1>
                </div>
                <div class="stt-header-menu">
                    <div class="back-menu"><img src="<?php echo STT_ASSETS_URI ?>images/ico_close.svg" alt="<?php echo stt_get_alt_image() ?>" ></div>
                    <?php
                    if(has_nav_menu('primary-menu')) {
                        wp_nav_menu([
                            'theme_location' => 'primary-menu',
                            "container" => "",

                        ]);
                    }
                    ?>
                </div>
            </div>

        </div>

    </div>
</div>

