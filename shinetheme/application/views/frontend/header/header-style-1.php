<?php
$headerLogo = stt_get_option('logo');
$st= &stt_get_instance();
?>
<div class="container">
    <div class="row  stt-header-swapper">
        <div class="stt-header-content d-flex justify-content-between <?php if(is_front_page()){ ?> border-0 <?php } ?> ">
            <div  class="toggle-menu">
                <img src="<?php echo STT_ASSETS_URI ?>images/ico_menu-options.svg" alt="<?php echo stt_get_alt_image() ?>">
            </div>
            <div class="stt-header-left">
                <div class="stt-logo">
                    <?php if ($headerLogo) { ?>
                        <a href="<?php echo esc_url(home_url('/')) ?>"><img src="<?php echo esc_url($headerLogo) ?>" alt="<?php echo stt_get_alt_image() ?>"></a>
                    <?php } else { ?>
                        <h1><a class="no-logo " href="<?php echo esc_url(home_url('/')) ?>"><?php echo esc_html(get_bloginfo('name')); ?></a></h1>
                    <?php } ?>
                </div>
                <div class="stt-header-menu">
                    <div class="back-menu"><img src="<?php echo STT_ASSETS_URI ?>images/ico_close.svg" alt="<?php echo stt_get_alt_image() ?>" ></div>
                    <?php
                    if(has_nav_menu('primary-menu')) {
                        wp_nav_menu([
                            'theme_location' => 'primary-menu',
                            "container" => "",

                        ]);
                    }
                    ?>
                </div>
            </div>
            <div class="stt-header-right">
                <ul>
                    <li class="ico-item ico-search ">
                        <?php get_search_form(); ?>
                    </li>
                    <li class="ico-item ico-cart"><img src="<?php echo STT_ASSETS_URI?>images/ic_cart.svg" alt="<?php echo stt_get_alt_image() ?>">
                        <span class="stt-number-cart">(0)</span>
                    </li>
                    <li class="ico-facebook"><i class="fab fa-facebook-f"></i></li>
                    <li class="ico-instagram"><i class="fab fa-instagram"></i></li>
                    <li class="ico-twitter"><i class="fab fa-twitter"></i></li>
                </ul>
            </div>
        </div>

    </div>
</div>

