<?php
/**
 * Created by PhpStorm.
 * User: HanhDo
 * Date: 7/9/2019
 * Time: 8:27 AM
 */
?>
<div class="stt-pagination">

    <?php
    if (isset($query)) {
        stt_pagination($query);
    } else {
        stt_pagination();
    }
    ?>
</div>
