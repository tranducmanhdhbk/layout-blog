<?php
/**
 * Created by PhpStorm.
 * User: HanhDo
 * Date: 6/28/2019
 * Time: 1:39 PM
 */
$st = stt_get_instance();
$orderby = $st->input->get('orderBy', 'lastest_post');
if(!in_array($orderby, array('lastest_post', 'most_viewed_post'))){
    $orderby = 'lastest_post';
}
?>
<div class="sort-by">
    <div class="dropdown">
        <input type="hidden" name="sort_by" value="Lastest_post" />
        <div  id="sortDropdownButton" data-toggle="dropdown" aria-haspopup="true"  role="none">
            <span class="value"><span class="value-change">
                    <?php
                    switch ($orderby){
                        case 'lastest_post':
                            echo esc_html__('Lastest post', 'layout-blog');
                            break;
                        case 'most_viewed_post':
                            echo esc_html__('Most viewed ', 'layout-blog');
                            break;
                    }
                    ?>
                </span> <img src="<?php echo STT_ASSETS_URI ?>images/blog/ico-sort.svg" alt="<?php echo stt_get_alt_image() ?>"> </span>
        </div>
        <div class="dropdown-menu sort-dropdown" aria-labelledby="sortDropdownButton">
            <ul>
                <li data-value="lastest_post" class="<?php echo esc_attr($orderby) == 'lastest_post' ? 'active' : ''; ?>"><?php echo esc_html__('Lastest post', 'layout-blog'); ?></li>
                <li data-value="most_viewed_post" class="<?php echo esc_attr($orderby) == 'most_viewed_post' ? 'active' : ''; ?>"><?php echo esc_html__('Most viewed ', 'layout-blog'); ?></li>
            </ul>
        </div>
    </div>
</div>

