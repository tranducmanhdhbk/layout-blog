<?php
$st = &stt_get_instance();
$termID = get_queried_object()->term_id;
if(empty($termID)){
    $pageID = get_the_ID();
    $termID = get_post_meta($pageID,'page_term_id'); ;
}else{
    $pageID = stt_get_term_meta($termID,'stt_destination_page');
}


$args = array(
    'post_type' => 'post',
    'orderby' => 'date',
    'order' => 'DESC',
    'post_status' => 'publish',
    'posts_per_page' =>'4',
    'tax_query' => array(
            array(
                'taxonomy' => 'st_destinations',
                'field' => 'term_id',
                'terms' => $termID,
            ),

        ),



);
$query = new WP_Query($args);


if ($query->have_posts()) {
    $st = &stt_get_instance();
    ?>

    <div class=" row stt-list-blog">
        <div class=" col-md-12 blog-heading">
            <p><?php echo esc_html($title) ?></p>
        </div>
    <?php
    while ($query->have_posts()) {
        $query->the_post();
        if(has_post_thumbnail()){
        ?>
            <div class="col-md-3">
                <div class="stt-blog-item">
                    <div class="thumb">
                        <a href="<?php echo esc_url(get_the_permalink()); ?>">
                            <?php the_post_thumbnail(array(540, 540)); ?>
                        </a>
                    </div>
                    <div class="blog-title">
                        <a href="<?php echo esc_url(get_the_permalink()); ?>"><?php echo esc_html(get_the_title()); ?></a>
                    </div>
                </div>
            </div>
        <?php

    } }
    echo '</div>';
}

?>

