<?php

if ($number < 0) {
    $number = -1;
}
if ($order_by == 'id') {
    $orderBy = 'ID';
} else if ($order_by == 'name') {
    $orderBy = 'name';
} else if ($order_by == 'random') {
    $orderBy = 'rand';
}
if ($order == 'asc') {
    $stOrder = 'ASC';
} else {
    $stOrder = 'DESC';
}
$args = array(
    'post_type' => 'post',
    'orderby' => $orderBy,
    'order' => $stOrder,
);

if (!empty($post_ids)) {
    $args['post__in'] = explode(',', $post_ids);
} else {
    $args['posts_per_page'] = $number;
}
$query = new WP_Query($args);

if ($query->have_posts()) {
    $st = &stt_get_instance();
    ?>
    <div class="carousel stt-carousel"
         data-flickity='{ "wrapAround": true}'>
    <?php
    while ($query->have_posts()) {
        $query->the_post();

        $st->load->view('grid1', 'frontend/blog');
    }
    echo '</div>';
}


