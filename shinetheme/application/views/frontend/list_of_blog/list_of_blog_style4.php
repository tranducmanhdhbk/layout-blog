<?php
$st = &stt_get_instance();

?>
<?php $st->load->view('loader', 'frontend/page') ?>
<div class=" stt-toolbar d-flex justify-content-between">
    <?php stt_breadcrumbs(true); ?>
    <div class="text-right">
        <?php $st->load->view('sort', 'frontend/page/toolbar') ?>
    </div>
</div>
<div class="stt-list-blog"></div>
<div class="blog-pagination"></div>

