<?php

if ($number < 0) {
    $number = -1;
}


$args = array(
    'post_type' => 'post',
    'meta_key'   => 'stt_number_view',
    'orderby'    => 'meta_value_num',
    'order' => 'DESC',
);

if (!empty($post_ids)) {
    $args['post__in'] = explode(',', $post_ids);
} else {
    $args['posts_per_page'] = $number;
}
$query = new WP_Query($args);

if ($query->have_posts()) {
    $st = &stt_get_instance();
    ?>
    <div class="stt-list-blog">
    <div class="  stt-blog-heading ">
            <h4><?php echo esc_html($title)?></h4>
    </div>
    <?php
    while ($query->have_posts()){
        $query->the_post();
        ?>
        <div class="blog-item">
    <?php
    if(has_post_thumbnail()){
        ?>
        <div class="thumb">
            <a href="<?php echo esc_url(get_the_permalink()); ?>">
                <?php the_post_thumbnail(array(240,180)); ?>
            </a>
        </div>
        <div class="content">
                <span class="stt-time"><?php the_time('d F Y')?></span>
            <span class="stt-blog-item-title"><a href="<?php echo esc_url(get_the_permalink()); ?>"><?php echo esc_html(get_the_title()); ?></a></span>
        </div>
    <?php }
    ?>
</div>
        <?php
    }
    echo '</div>';
}