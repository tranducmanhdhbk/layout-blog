<?php
if ($number < 0) {
    $number = -1;
}
if ($order_by == 'id') {
    $orderBy = 'ID';
} else if ($order_by == 'name') {
    $orderBy = 'name';
} else if ($order_by == 'random') {
    $orderBy = 'rand';
}
if ($order == 'asc') {
    $stOrder = 'ASC';
} else {
    $stOrder = 'DESC';
}

$args = array(
    'post_type' => 'post',
    'orderby' => $orderBy,
    'order' => $stOrder,
);

if (!empty($post_ids)) {
    $args['post__in'] = explode(',', $post_ids);
} else {
    $args['posts_per_page'] = $number;
}

$query = new WP_Query($args);

if ($query->have_posts()) {
    $st = &stt_get_instance();
    ?>

    <div class=" row stt-list-blog">
    <div class=" col-md-12 stt-blog-heading ">
        <div class="stt-blog-sub-heading"><?php echo esc_html($sub_title) ?></div>
        <div class="blog-heading">
            <h1><?php echo esc_html($title) ?></h1>
        </div>
    </div>
    <?php
    while ($query->have_posts()) {
        $query->the_post();
        echo '<div class="col-md-6">';
        $st->load->view('grid2', 'frontend/blog');
        echo '</div>';
    }
    echo '</div>';
    ?>
    <div class=" col-lg-12 stt-view-all">
        <a href="<?php echo esc_attr(get_permalink(get_option('stt_blog_page'))) ?>"><span
                    class="stt-view-text"><?php echo esc_html__('View all', 'layout-blog') ?></span></a>
    </div>
    <?php
}