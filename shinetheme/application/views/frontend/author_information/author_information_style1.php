<?php

?>
<div class="author-info">
    <div class="author-avatar">
        <img src="<?php echo esc_url($author_logo['url']) ?>" alt="author-avatar" >
    </div>
    <div class="logo">
        <img src="<?php echo esc_url($logo['url']) ?>" alt="logo" >
    </div>
    <div class="author-content">
        <div class="stt-author-text">
            <span><?php echo  esc_html($description) ?></span>
        </div>
    </div>
</div>
