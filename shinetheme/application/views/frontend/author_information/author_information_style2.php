<?php
global $post;
?>
<div class="author-info">
    <div class="author-avatar">
        <img src="<?php echo esc_url($author_logo['url']) ?>" alt="author-avatar" >
    </div>
    <div class="author-content">
        <div class="stt-author-infor">
            <p class="author-name"><?php echo esc_html__('By ', 'layout-blog') ?>
                <span><?php echo esc_html(stt_username($post->post_author)) ?> </span></p>
            <div class="contact">
                <a class="facebook ico-item" href="<?php echo esc_url($facebook) ?>" target="_blank"><i class="fab fa-facebook-f"></i></a>
                <a class="instagram ico-item" href="<?php echo esc_url($instagram) ?>" target="_blank" ><i class="fab fa-instagram"></i></a>
                <a class="twitter ico-item" href="<?php echo esc_url($twitter) ?>" target="_blank"><i class="fab fa-twitter"></i></a>
            </div>
        </div>
        <div class="stt-author-text">
            <span><?php echo  esc_html($description) ?></span>
        </div>
    </div>
</div>
