<?php

if ($number < 0) {
    $number = 'all';
}

if ($order_by == 'id') {
    $orderBy = 'ID';
} else if ($order_by == 'name') {
    $orderBy = 'name';
}

if ($order == 'asc') {
    $stOrder = 'ASC';
} else {
    $stOrder = 'DESC';
}

$args = array(
    'taxonomy' => 'st_destinations',
    'hide_empty' => false,
    'orderby' => $orderBy,
    'order' => $stOrder,


);
if (!empty($des_ids)) {
    $args['include'] = explode(",", $des_ids);
} else {
    $args['number'] = $number;
}

$query = get_terms($args);
?>
<div class="stt-destination-title">
    <div class="stt-sub-title">
        <?php
        if (!empty($sub_title)) { ?>
            <p><?php echo esc_html($sub_title) ?></p>
        <?php } ?>
    </div>
    <div class="stt-title">
        <?php
        if (!empty($title)) { ?>
            <h1><?php echo esc_html($title) ?></h1>
        <?php } ?>

    </div>
</div>
<div class="stt-list-destination">
    <div class="owl-carousel ">
        <?php foreach ($query as $value) {
            $link = get_term_link($value->term_id, 'st_destinations');
            ?>
            <div class="destination-item">
                <div class="thumb">
                    <?php $imageID = stt_get_term_meta($value->term_id, 'stt_destination_image');
                    $imageUrl = wp_get_attachment_image_url($imageID, array(560,745));
                    if (!empty($imageUrl)) { ?>
                        <a href="<?php echo esc_url($link) ?>"><img src="<?php echo esc_url($imageUrl) ?> " alt="<?php echo stt_get_alt_image() ?>"></a>
                    <?php } ?>
                </div>
                <div class="destination-name"><a href="<?php echo esc_url($link) ?>">
                        <p><?php echo esc_html($value->name) ?></p></a></div>
            </div>
        <?php } ?>
    </div>
    <button class="Destination-Next stt-next" type="button"></button>
</div>


