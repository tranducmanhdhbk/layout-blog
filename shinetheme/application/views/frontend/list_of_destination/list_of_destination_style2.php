<?php

if ($number < 0) {
    $number = 'all';
}

$args = array(
    'taxonomy' => 'st_destinations',
    'hide_empty' => false,
    'number' => $number,
    'offset' => '0',

);
$query = get_terms($args);
$st = &stt_get_instance();
?>
<?php $st->load->view('loader', 'frontend/page') ?>
<?php stt_breadcrumbs(true);  ?>
<div class="stt-list-destination">
    <div class="row">
        <?php foreach ($query as $value) {
            $link = get_term_link($value->term_id, 'st_destinations');
            ?>
        <div class="col-md-3 col-6 destination-item">
            <div class="thumb">
                <?php $imageID = stt_get_term_meta($value->term_id, 'stt_destination_image');
                $imageUrl = wp_get_attachment_image_url($imageID, array(540,720));
                if (!empty($imageUrl)) { ?>
                    <a href="<?php echo esc_url($link) ?>"><img src="<?php echo esc_url($imageUrl) ?>" alt="<?php echo stt_get_alt_image() ?>"></a>
                <?php } ?>
            </div>
            <div class="destination-name">
                <a href="<?php echo esc_url($link) ?>">
                    <h4><?php echo esc_html($value->name) ?></h4>
                </a>
            </div>
        </div>
        <?php } ?>
    </div>
    <input type="hidden" name="stt-number-des" value="<?php echo esc_attr($number) ?>">
</div>
<div class="des-load-more">
    <button class="stt-load-more" data-offset="<?php echo esc_attr($number) ?>"><?php echo esc_html__('Load more','layout-blog') ?></button>
</div>

