<?php
$st = &stt_get_instance();
$class_item = 'col-md-6';
$active_sidebar = is_active_sidebar('normal-sidebar');
$class_left = 'col-md-8';
if (!$active_sidebar) {
    $class_left = 'col-md-12';
    $class_item = 'col-md-4';
}
$search_term = $st->input->request('s');
$paged = (get_query_var('paged')) ? get_query_var('paged') :1;
$args = array(
    'post_type'   => 'post',
    's'           => $search_term,
    'post_status' => 'publish',
    'paged'       => $paged
);
$query = new WP_Query($args);

?>
<div class="stt-search-blog">
    <div class="container">
        <div class="row">
            <div class="<?php echo esc_attr($class_left) ?> col-xs-12">
                <div class="list-search-blog">
                <?php  stt_breadcrumbs(); ?>
                <?php if($query->have_posts()){
                    ?>
                    <div class="row list-search-blog-content">
                       <?php while ($query->have_posts()){
                           $query->the_post();
                           ?>
                           <div class="<?php echo esc_attr($class_item) ?>">
                               <?php  $st->load->view('grid2', 'frontend/blog'); ?>
                           </div>
                        <?php }?>
                    </div>
                    <?php }else{ ?>
                    <div class="alert alert-warning">
                        <?php echo esc_html__('No blogs found','layout-blog') ?>
                    </div>
                    <?php }?>
                    <div class="stt-pagination">
                        <?php  stt_pagination($query); ?>
                    </div>
                </div>
            </div>
            <?php if($active_sidebar) { ?>
            <div class="col-md-4 col-xs-12">
                <aside class='sidebar-right'>
                    <?php dynamic_sidebar('normal-sidebar'); ?>
                </aside>
            </div>
            <?php } ?>
        </div>
    </div>
</div>
<?php

