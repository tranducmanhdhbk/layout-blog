<?php
$st = &stt_get_instance();
?>
<div class="stt-author-information <?php echo esc_attr($style) ?> ">
    <?php
    if($style == 'style1'){
        $st->load->view('author_information_style1','frontend/author_information',array('style' => $style, 'description' => $description,'author_logo' => $author_logo,'logo' => $logo));
    }
    if($style == 'style2'){
        $st->load->view('author_information_style2','frontend/author_information',array('style' => $style, 'description' => $description,'author_logo' => $author_logo,'facebook' => $facebook,
            'instagram' => $instagram,'twitter' => $twitter));
    }
    ?>
</div>
