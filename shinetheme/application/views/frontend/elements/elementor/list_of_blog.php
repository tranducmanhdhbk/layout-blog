<?php
$st = &stt_get_instance();
wp_enqueue_script('st-flickity-js');
wp_enqueue_style('st-flickity-css');
?>
<div class="stt-list-of-blog <?php echo esc_attr($style) ?>">

    <?php
    if($style == 'style1'){
        $st->load->view('list_of_blog_style1','frontend/list_of_blog',array('style' => $style,'order_by' => $order_by,'order' => $order,'number' => $number,'post_ids' => $post_ids));
    }
    if($style == 'style2'){
        $st->load->view('list_of_blog_style2','frontend/list_of_blog',array('style' => $style,'order_by' => $order_by,'order' => $order,'number' => $number,'post_ids' => $post_ids,
            'title' => $title,'sub_title' => $sub_title));
    }
    if($style == 'style3'){
        $st->load->view('list_of_blog_style3','frontend/list_of_blog',array('style' => $style,'order_by' => $order_by,'order' => $order,'number' => $number,'post_ids' => $post_ids,
            'title' => $title));
    }
    if($style == 'style4'){
        $st->load->view('list_of_blog_style4','frontend/list_of_blog',array('style' => $style));
    }
    if($style == 'style5'){
        $st->load->view('list_of_blog_style5','frontend/list_of_blog',array('style' => $style, 'title' => $title));
    }
    ?>
</div>

