<?php
$st = &stt_get_instance();
?>
<div class="stt-list-of-destination clearfix <?php echo esc_attr($style) ?> ">
    <?php
    if($style == 'style1'){
        $st->load->view('list_of_destination_style1','frontend/list_of_destination',array('style' => $style,'order_by' => $order_by,'order' => $order,'number' => $number,'des_ids' => $des_ids,
            'title' => $title,'sub_title' =>$sub_title));
    }
    if($style == 'style2'){
        $st->load->view('list_of_destination_style2','frontend/list_of_destination',array('style' => $style,'order_by' => $order_by,'order' => $order,'number' => $number));
    }
    ?>
</div>
