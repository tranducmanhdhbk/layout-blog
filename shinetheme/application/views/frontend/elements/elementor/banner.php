<?php

$st = &stt_get_instance();
$banner_class = '';

?>

<?php stt_breadcrumbs(true);  ?>
<div class="stt-banner  <?php echo esc_attr($banner_class); ?> " style="background: transparent url('<?php echo esc_url($banner_bg['url']); ?>') center center no-repeat">

    <div class="stt-title">
        <h1><?php echo esc_html(get_the_title()) ?></h1>
    </div>
</div>