<?php
$st = &stt_get_instance();
?>
<div class="stt-list-of-feature <?php echo esc_attr($style) ?> ">
    <?php
    if($style == 'style1'){
        $st->load->view('list_feature_style1','frontend/list_feature',array('style' => $style,'settings' => $settings));
    }
    elseif ($style == 'style2'){
        $st->load->view('list_feature_style2','frontend/list_feature',array('style' => $style,'settings' => $settings));
    }
    ?>
</div>

