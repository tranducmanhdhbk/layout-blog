<?php
if(!empty($map_icon)){
    $map_icon = $map_icon['url'];
}
if(empty($map_control)){
    $map_control = 'false';
}else{
    $map_control = 'true';
}

if(empty($map_label)){
    $map_label = 'false';
}else{
    $map_label = 'true';
}
if(empty($map_height)){
    $map_height = '500';
}
$map_height_css=  STT_Assets::get_inst()->build_css('height: ' . esc_attr($map_height).  'px !important');
?>
<?php stt_breadcrumbs(true);  ?>
<div class="stt-map-element">
    <div class="google-map <?php echo esc_attr($map_height_css); ?>"
         style="height: <?php echo esc_attr($map_height) . 'px !important'; ?>"
         data-lat="<?php echo esc_attr(trim($map_lat)); ?>"
         data-lng="<?php echo esc_attr(trim($map_lng)); ?>"
         data-icon="<?php echo esc_url($map_icon); ?>"
         data-zoom="<?php echo esc_attr($map_zoom); ?>"
         data-disablecontrol="true"
         data-showcustomcontrol="<?php echo esc_attr($map_control); ?>"
         data-showlabel="<?php echo esc_attr($map_label); ?>"
         data-style="<?php echo esc_attr($map_style); ?>"
         data-customstyle='<?php echo stt_esc_data($map_custom); ?>'></div>
</div>
