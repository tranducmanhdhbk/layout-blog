<?php
$st = &stt_get_instance();
?>
<div class="stt-list-of-category  <?php echo esc_attr($style) ?> ">
    <?php
    if($style == 'style1'){
        $st->load->view('list_of_category_style1','frontend/list_of_category',array('style' => $style,'order_by' => $order_by,'order' => $order,'number' => $number,'des_ids' => $des_ids,
            'title' => $title));
    }
    if($style == 'style2'){
        $st->load->view('list_of_category_style2','frontend/list_of_category',array('style' => $style,'order_by' => $order_by,'order' => $order,'number' => $number,'des_ids' => $des_ids,
            'title' => $title));
    }
    ?>
</div>
