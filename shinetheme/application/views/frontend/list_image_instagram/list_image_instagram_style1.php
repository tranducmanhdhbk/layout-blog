<?php
$listImageUrl = stt_get_instagram_images(get_option('stt_insta_user_name'),get_option('stt_number_of_photos'));

?>

<div class="stt-instagram-content">
    <div class="stt-instagram-heading">
        <h4><?php echo esc_html($title) ?></h4>
    </div>
    <div class="stt-list-image">
        <?php if(!empty($listImageUrl)){
            foreach($listImageUrl as $ImageUrl){
         ?>
        <div class="stt-image-item" >
            <a href="<?php echo esc_url($ImageUrl) ?>" data-toggle="stt-lightbox" data-gallery="stt-gallery" >
                <div class="stt-ico-instagram">
                    <img src="<?php echo STT_ASSETS_URI ?>images/image_instagram/ico_camera_insta.svg" alt="<?php echo stt_get_alt_image() ?>" >
                </div>
                <div class="stt-instagram-gradient"></div>
                <img src="<?php echo esc_url($ImageUrl) ?>" class="img-fluid" alt="<?php echo stt_get_alt_image() ?>">
            </a>

        </div>

        <?php } } ?>
    </div>
    <div class="stt-instagram-follow">
    <p><?php echo esc_html__('follow us ','layout-blog')?><span class="st-instagram-text"><a href="<?php echo esc_url(get_option('stt_instagram_link')) ?>" target="_blank"><?php echo esc_html__('#GoAround','layout-blog')?></a></span></p>
    </div>

</div>


