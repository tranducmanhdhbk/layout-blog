<?php
?>
<div class="stt-search-form">
   <div class="container">
       <div class="row">
           <div class=" stt-title col-md-12">
               <img src="<?php echo STT_ASSETS_URI ?>images/ic-search.svg" alt="<?php echo stt_get_alt_image() ?>">
               <span><?php echo esc_html($title) ?></span>
           </div>
           <form class="stt-form col-md-12">
               <div class="form">
                   <div class="row">
                       <div class="col-md-4">
                           <div class="form-group form-destination-search">
                               <label><?php echo esc_html__('Destiations','layout-blog')?></label>
                               <input class="form-control location-input" type="text">
                           </div>
                       </div>
                       <?php
                       $start = date('d/m/Y');
                       $end = date('d/m/Y', strtotime(' + 1 days'));
                       $date = date('d/m/Y') . date('d/m/Y', strtotime(' + 1 days')) ;
                       if (isset($_GET['start']) && isset($_GET['end']) && isset($_GET['date'])) {
                           if (!empty($_GET['start'])) {
                               $start =  $_GET['start'];
                           }
                           if (!empty($_GET['end'])) {
                               $end = $_GET['end'];
                           }
                           if (!empty($_GET['date'])) {
                               $date = $_GET['date'];
                           }
                       }
                       ?>
                       <div class="col-md-4">
                           <div class="form-group form-date-search">
                               <label><?php echo esc_html__('Checkin - Check out','layout-blog')?></label>
                               <div class="render">
                                   <div id="reportrange">
                                       <?php echo esc_html__('dd/mm/yy - dd/mm/yy','layout-blog')?>
                                   </div>
                                   <div class="ico-calendar">
                                       <img src="<?php echo STT_ASSETS_URI ?>images/destinations/ico-calendar.svg" alt="<?php echo stt_get_alt_image() ?>">
                                   </div>
                                   <input type="hidden" name="start" id="start"  value="<?php echo esc_attr($start); ?>">
                                   <input type="hidden" name="end" id="end" value="<?php echo esc_attr($end); ?>">
                                   <input type="text" name="date" id="date" value="<?php echo esc_attr($start) . ' - ' . esc_attr($end); ?>">
                               </div>
                           </div>
                       </div>
                       <div class="col-md-4">
                           <div class="form-group form-guest-search">
                               <label><?php echo esc_html__('Guest','layout-blog')?></label>
                               <div class="render">
                                   <div class="dropdown" data-toggle="dropdown" id="dropdown-1" role="none">
                                       <span class="adult" data-text="Adult" data-text-multi="Adults">1 Adult</span>/<span class="children" data-text="Child" data-text-multi="Children">0 Child</span>
                                   </div>
                                   <ul class="dropdown-menu" aria-labelledby="dropdown-1" >
                                       <li class="item">
                                           <label>Adults</label>
                                           <div class="stt-number" data-min="1" data-max="20">
                                               <span class="minus control"><img src="<?php echo STT_ASSETS_URI ?>images/destinations/icon-minus.svg" alt="<?php echo stt_get_alt_image() ?>"></span>
                                               <span class="value">1</span>
                                               <span class="add control"><img src="<?php echo STT_ASSETS_URI ?>images/destinations/icon-plus.svg" alt="<?php echo stt_get_alt_image() ?>"></span>
                                               <input type="hidden" name="number_adult" value="1" data-text="adult" data-text-multi="adults">
                                           </div>
                                       </li>
                                       <li class="item">
                                           <label>Children</label>
                                           <div class="stt-number" data-min="0" data-max="20">
                                               <span class="minus control"><img src="<?php echo STT_ASSETS_URI ?>images/destinations/icon-minus.svg" alt="<?php echo stt_get_alt_image() ?>"></span>
                                               <span class="value">0</span>
                                               <span class="add control"><img src="<?php echo STT_ASSETS_URI ?>images/destinations/icon-plus.svg" alt="<?php echo stt_get_alt_image() ?>"></span>
                                               <input type="hidden" name="number_child" value="0" data-text="adult" data-text-multi="adults">
                                           </div>
                                       </li>
                                   </ul>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
               <div class="stt-form-button">
                   <button class="btn btn-default btn-search" type="submit">Search</button>
               </div>
           </form>
       </div>
   </div>
</div>
