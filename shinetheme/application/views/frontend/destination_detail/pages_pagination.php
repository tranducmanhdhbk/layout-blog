<?php
wp_reset_postdata();
$termID = get_queried_object()->term_id;
if(empty($termID)){
    $pageID = get_the_ID();
    $termID = get_post_meta($pageID,'page_term_id');
}else{
    $pageID = stt_get_term_meta($termID,'stt_destination_page');

}
?>
<div class="pages-pagination clearfix">
    <?php
    echo get_tax_navigation( 'st_destinations',$pageID );
    ?>
</div>
