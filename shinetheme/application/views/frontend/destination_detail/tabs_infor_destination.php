<?php

?>

<div class="row justify-content-center stt-tabs-infor-des">
    <div class="stt-title">
        <?php
        foreach ($settings as $key => $value) {
            ?>
            <div class="stt-title-item ">
                <a href="#<?php echo esc_html($value['link']) ?>"><?php echo esc_html($value['list_title']); ?></a>
            </div>
        <?php } ?>
    </div>
</div>

