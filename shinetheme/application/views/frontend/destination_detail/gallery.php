<?php
$st = &stt_get_instance();
$termID = get_queried_object()->term_id;
if(empty($termID)){
    $pageID = get_the_ID();
    $termID = get_post_meta($pageID,'page_term_id'); ;
}else{
    $pageID = stt_get_term_meta($termID,'stt_destination_page');
}
$galleryImage = explode(',',stt_get_meta($pageID,'stt_gallery') );
?>
<div class="stt-gallery-des">
    <?php stt_breadcrumbs(true) ?>
    <div class="stt-gallery">
        <div class="owl-carousel" >
            <?php foreach ($galleryImage as $value){ ?>
                <div class="thumb">
                    <?php
                    $imageUrl = wp_get_attachment_image_url($value,array(740,555));
                    if (!empty($imageUrl)) { ?>
                       <img src="<?php echo esc_url($imageUrl) ?>" alt="<?php echo stt_get_alt_image() ?>">
                    <?php } ?>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<div class="stt-des-infor">
    <div class="stt-des-title">
        <h1><?php echo esc_html($title) ?></h1>
    </div>
    <div class="share-column">
        <?php stt_blog_sharet(); ?>
    </div>
    <div class="stt-des-description">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class=" stt-description">
                        <?php echo  term_description($termID[0]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>



