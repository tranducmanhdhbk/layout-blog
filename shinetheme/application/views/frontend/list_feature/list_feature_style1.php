<?php

?>

<div class="row justify-content-center stt-list-feature">
        <?php
        foreach ($settings as $key => $v){ ?>
            <div class="col-lg-5 feature-item" >
                <div class="stt-feature-image">
                    <img src="<?php echo esc_url($v['list_image']['url'])?>" class="img-fluid" alt="<?php echo stt_get_alt_image() ?>">
                </div>
                <div class="stt-feature-content">
                    <h4 class="stt-feature-title"><?php echo esc_html($v['list_title']) ?></h4>
                    <span class="stt-feature-text"><?php echo esc_html($v['list_description']) ?></span>
                </div>
            </div>

       <?php } ?>
</div>
