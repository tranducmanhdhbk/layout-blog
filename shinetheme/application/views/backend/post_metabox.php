<?php
$postID = get_the_ID();
$imagePost = stt_get_meta($postID,'stt_post_image');
?>
    <label class="stt-submit-text" > <?php echo  esc_html__( 'Attachment:','layout-blog') ?></label>
    <div class="stt-upload-wrapper">
        <input type="hidden" class="upload_value" name="upload_value" value="<?php echo esc_attr($imagePost) ?>">
        <div class="attachments">
        <?php


      if(!empty($imagePost)){
          $imagePost = explode(",",$imagePost);
      foreach($imagePost as $key => $values ){
          $thumbnail = wp_get_attachment_image_url($values,[100,100]);
        ?>
          <div class="attachments-item"><span class="stt-attachments-delete">Close</span><img  src="<?php echo esc_url($thumbnail) ?>" data-id="<?php echo esc_attr($values) ?>"/></div>
        <?php } } ?>
        </div>
        <input type="button" class="add-attachments button button-default"  value="upload images">
    </div>


