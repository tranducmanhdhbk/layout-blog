<?php
wp_enqueue_media();
$st = &stt_get_instance();
$termID = $st->input->get('tag_ID');
$imageID = get_term_meta($termID,'stt_destination_image',true);
$listPage = STT_ThemeOption::get_inst()->sttListPage();
?>
<tr class="form-field term-group-wrap">
    <th scope="row">
        <label> <?php echo  esc_html__( 'Select Page','layout-blog') ?></label>
    </th>
    <td>
        <select  name="stt_destination_page" >
            <?php
            $stDestinationPageID = get_term_meta($termID,'stt_destination_page');
            foreach ($listPage as $key =>$value){
                ?>
                <option value="<?php echo esc_attr($value->ID)?>" <?php  selected($value->ID,$stDestinationPageID[0])?> ><?php echo esc_html($value->post_title)?>  </option>
            <?php } ?>
        </select>
    </td>
</tr>
<tr class="form-field term-group-wrap">
    <th scope="row">
        <label class="stt-submit-text" > <?php echo  esc_html__( 'Attachment','layout-blog') ?></label>
    </th>
    <td>
        <input type="hidden" name="upload_value" value="<?php echo esc_attr($imageID) ?>">
        <div class="stt-edit-avatar">
            <?php
            $imageURL = wp_get_attachment_image_url($imageID,array(80,80));
            if(!empty($imageURL)){ ?>
                <img src="<?php echo esc_url($imageURL); ?>" alt="destination image">
            <?php }?>
        </div>
        <div class="stt-upload-avatar">
            <input type="button" class="button button-defautl stt-update-avatar" value="upload image">

        </div>
    </td>
</tr>

