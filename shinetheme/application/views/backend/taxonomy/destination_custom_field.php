<?php
wp_enqueue_media();
$listPage = STT_ThemeOption::get_inst()->sttListPage();
?>
<div class="form-field">
    <label> <?php echo  esc_html__( 'Select Page','layout-blog') ?></label>
    <select  name="stt_destination_page" >
        <?php foreach ($listPage as $key =>$value){
            $stDestinationPageID = get_option('stt_destination_page');
            ?>
            <option value="<?php echo esc_attr($value->ID)?>" <?php selected($value->ID,$stDestinationPageID)?>><?php echo esc_html($value->post_title)?>  </option>
        <?php } ?>
    </select>
</div>
<div class="form-field">
    <label class="stt-submit-text" > <?php echo  esc_html__( 'Attachment','layout-blog') ?></label>
    <div class="stt-edit-avatar">
    </div>
    <input type="hidden" name="upload_value" value="">
    <div class="stt-upload-avatar">
        <input type="button" class="button button-defautl stt-update-avatar" value="upload image">
    </div>
</div>


