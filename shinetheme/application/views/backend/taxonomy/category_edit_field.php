<?php
wp_enqueue_media();
$st = &stt_get_instance();
$termID = $st->input->get('tag_ID');
$imageID = get_term_meta($termID,'stt_cate_image',true);
?>
<tr class="form-field term-group-wrap">
    <th scope="row">
        <label class="stt-submit-text" > <?php echo  esc_html__('Attachment','layout-blog') ?></label>
    </th>
    <td>
        <input type="hidden" name="upload_value" value="<?php echo esc_attr($imageID) ?>">
        <div class="stt-edit-avatar">
        <?php
             $imageURL = wp_get_attachment_image_url($imageID,array(80,80));
             if(!empty($imageURL)){ ?>
            <img src="<?php echo esc_url($imageURL); ?>" alt="category image">
            <?php }?>
        </div>
        <div class="stt-upload-avatar">
            <input type="button" class="button button-defautl stt-update-avatar" value="upload image">

        </div>
    </td>
</tr>
