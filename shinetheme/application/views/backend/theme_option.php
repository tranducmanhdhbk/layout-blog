<?php
$st = &stt_get_instance();
$listPage = STT_ThemeOption::get_inst()->sttListPage();
$stInstagramUserName = get_option('stt_insta_user_name');
$stNumberOfPhotos = get_option('stt_number_of_photos');
$stInstagramLinh = get_option('stt_instagram_link');
?>
<h2>Setting Theme Options</h2>
<?php settings_errors(); ?>
<form  method="post" action="">
    <table class="form-table">
        <tbody>
        <tr>
            <th scope="row"><label><?php  echo esc_html__('Footer Page ','layout-blog') ?></label></th>
            <td>
                <select  name="stt_footer_page" >
                    <?php foreach ($listPage as $key =>$value){
                        $stFooterPageID = get_option('stt_footer_page');
                        ?>
                        <option value="<?php echo esc_attr($value->ID)?>" <?php selected($value->ID,$stFooterPageID)?> ><?php echo esc_html($value->post_title)?>  </option>
                    <?php } ?>
                </select>
               
            </td>
        </tr>
        <tr>
            <th scope="row"><label><?php  echo esc_html__('Blog Page ','layout-blog') ?></label></th>
            <td>
                <select  name="stt_blog_page" >
                    <?php foreach ($listPage as $key =>$value){
                        $stBlogPageID = get_option('stt_blog_page');
                        ?>
                        <option value="<?php echo esc_attr($value->ID)?>" <?php selected($value->ID,$stBlogPageID)?> ><?php echo esc_html($value->post_title)?>  </option>
                    <?php } ?>
                </select>

            </td>
        </tr>
        <tr>
            <th scope="row"><label ><?php  echo esc_html__('Instagram UserName','layout-blog') ?></label></th>
            <td>
                <input type="text" name="stt_insta_user_name" value="<?php echo esc_attr($stInstagramUserName) ?>" placeholder="Instagram User Name">
            </td>
        </tr>
        <tr>
            <th scope="row"><label ><?php  echo esc_html__('Number Of Photos','layout-blog') ?></label></th>
            <td>
                <input type="text" name="stt_number_of_photos" value="<?php echo esc_attr($stNumberOfPhotos) ?>" placeholder="Number">
            </td>
        </tr>
        <tr>
            <th scope="row"><label ><?php  echo esc_html__('Your Instagram Link','layout-blog') ?></label></th>
            <td>
                <input type="text" name="stt_instagram_link" value="<?php echo esc_attr($stInstagramLinh) ?>" placeholder="Link..." autocomplete="off">
            </td>
        </tr>

        </tbody>
    </table>
    <input class="button button-primary" type="submit" name="save-theme-option" value="Save Changes"/>
</form>
