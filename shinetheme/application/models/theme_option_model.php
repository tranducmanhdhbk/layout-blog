<?php

class STT_Theme_Option_Model extends STT_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function sttListPage()
    {

        $this->query->_clear_query();
        $this->query->set_table('posts')->select('*')
            ->where("post_type = 'page' AND post_status = 'publish'", false, true);
        return $this->query->get(false, false, 'OBJECT')->result();
    }

    public static function get_inst()
    {
        static $instance;
        if (is_null($instance)) {
            $instance = new self();
        }
        return $instance;
    }
}
STT_Theme_Option_Model::get_inst();