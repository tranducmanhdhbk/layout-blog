<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 8/1/2017
 * Time: 12:47 AM
 * Since: 1.0.0
 * Updated: 1.0.0
 */
$config['log_path'] = '';
$config['log_file_extension'] = '.txt';
$config['log_threshold'] = 4;
$config['log_file_permissions'] = 0644;
$config['log_date_format'] = 'Y-m-d H:i:s';

$config['key_encrypt'] = 'st_';
