<?php
function stt_get_instagram_images($username, $limit = 20)
{
    $profile_url = "https://www.instagram.com/$username/?__a=1";
    $iteration_url = $profile_url;
    $tryNext = true;
    $found = 0;
    $images = array();
    while ($tryNext) {
        $tryNext = false;
        $remote = wp_remote_get($iteration_url);
        if (is_wp_error($remote)) {
            return false;
        }
        if (200 != wp_remote_retrieve_response_code($remote)) {
            return false;
        }
        $response = wp_remote_retrieve_body($remote);
        if ($response === false) {
            return false;
        }
        $data = json_decode($response, true);
        if ($data === null) {
            return false;
        }
        $media = $data['graphql']['user']['edge_owner_to_timeline_media']['edges'];
        foreach ($media as $key => $media_item) {
            if ($found + $key < $limit) {
                if (isset($media_item['node']['thumbnail_src'])) {
                    $image_item = $media_item['node']['thumbnail_src'];
                    array_push($images, $image_item);

                }
            }

        }
        $found += count($media);
    }
    return $images;
}

if (!function_exists('stt_get_option')) {
    function stt_get_option($key = '', $default = '')
    {
        return STOptiontree::getOption($key, $default);
    }
}

if (!function_exists('stt_username')) {
    function stt_username($user_id = '')
    {
        if (empty($user_id)) {
            $user_id = get_current_user_id();
        }
        $userdata = get_userdata($user_id);
        if (!empty($userdata->first_name) || !empty($userdata->last_name)) {
            return $userdata->first_name . ' ' . $userdata->last_name;
        } else {
            return $userdata->user_login;
        }
    }
}

if (!function_exists('stt_breadcrumbs')) {
    function stt_breadcrumbs($container = false)
    {
        global $post;
        ?>
        <div class="stt-breadcrumb hidden-xs">
            <?php
            if ($container) {
                echo '<div class="container">';
            }
            ?>
            <ul>
                <?php
                if (!is_home()) {
                    echo '<li>';
                    echo '<a href="' . home_url() . '">' . esc_html__('Home', 'layout-blog') . '</a></li>';
                    if (is_category() || is_single() || is_tag() || is_tax()) {
                        if (is_category()) {
                            $title = single_cat_title('', false);
                            echo '<li class="active">' . esc_html($title) . '</li>';
                        }
                        if (is_tag()) {
                            $title = single_tag_title('', false);
                            echo '<li class="active">' . esc_html($title) . '</li>';
                        }
                        if (is_tax()) {
                            $title = single_term_title('', false);
                            echo '<li class="active">' . esc_html($title) . '</li>';
                        }
                        if (is_single()) {
                            echo '<li class="active">' . get_the_title() . '</li>';
                        }
                    } elseif (is_page()) {
                        if ($post->post_parent) {
                            $anc = get_post_ancestors($post->ID);
                            $anc_link = get_page_link($post->post_parent);

                            foreach ($anc as $ancestor) {
                                $output = '<li><a href="' . esc_url($anc_link) . '">' . get_the_title($ancestor) . '</a></li>';
                            }
                            echo stt_esc_data($output);
                            echo '<li class="active">' . get_the_title() . '</li>';
                        } else {
                            if (isset($_GET['location_name']) && !empty($_GET['location_name'])) {
                                echo '<li class="active">' . esc_html($_GET['location_name']) . '</li>';
                            } else {
                                echo '<li class="active">' . get_the_title() . '</li>';
                            }
                        }
                    } elseif (is_author()) {
                        echo '<li class="active">' . esc_html__('Author Page', 'layout-blog') . '</li>';
                    } elseif (is_search()) {
                        $search_text = isset($_GET['s']) ? $_GET['s'] : '';
                        echo '<li class="active">' . sprintf(esc_html__('Search for: %s', 'layout-blog'), '"' . esc_html($search_text) . '"') . '</li>';
                    }
                } elseif (is_tag()) {
                    single_tag_title();
                } elseif (is_day()) {
                    echo __("Archive: ", 'layout-blog');
                    the_time('F jS, Y');
                    echo '</li>';
                } elseif (is_month()) {
                    echo __("Archive: ", 'layout-blog');
                    the_time('F, Y');
                    echo '</li>';
                } elseif (is_year()) {
                    echo __("Archive: ", 'layout-blog');
                    the_time('Y');
                    echo '</li>';
                } elseif (is_author()) {
                    echo __("Author's archive: ", 'layout-blog');
                    echo '</li>';
                } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) {
                    echo __("Blog Archive: ", 'layout-blog');
                    echo '';
                }
                ?>
            </ul>
            <?php
            if ($container) {
                echo '</div>';
            }
            ?>
        </div>
        <?php
    }
}
if (!function_exists('stt_pagination')) {
    function stt_pagination($query = false, $is_ajax = false)
    {
        if (!$query) {
            global $wp_query;
            $query = $wp_query;
        }
        $big = 999999999;
        if ($is_ajax) {
            if (!empty(stt_server('HTTP_REFERER'))) {
                $link_pag = add_query_arg('paged', $big, stt_server('HTTP_REFERER'));
            } else {
                $link_pag = get_pagenum_link($big);
            }
        } else {
            $link_pag = get_pagenum_link($big);
        }
        echo paginate_links(array(
            'base' => str_replace($big, '%#%', esc_url($link_pag)),
            'format' => '?paged=%#%',
            'type' => 'list',
            'current' => max(1, get_query_var('paged')),
            'total' => $query->max_num_pages,
            'prev_text' => sprintf('<span><img class="st-header-icon"  src="%s" alt="'.stt_get_alt_image().'"></span>', STT_ASSETS_URI . 'images/blog/pagination_pre.png'),
            'next_text' => sprintf('<span><img class="st-header-icon"  src="%s" alt="'.stt_get_alt_image().'"></span>', STT_ASSETS_URI . 'images/blog/pagination_next.png')
        ));
    }
}
if (!function_exists('stt_update_post_view')) {
    function stt_update_post_view($postID)
    {
        if (is_single()) {
            $numberView = stt_get_meta($postID, 'stt_number_view');
            $numberView++;
            update_post_meta($postID, 'stt_number_view', $numberView);
        }
    }

    add_action('template_redirect', 'stt_update_post_view');
}

if (!function_exists('stt_update_destination_view')) {
    function stt_update_destination_view($termID)
    {
        if (is_tax('st_destinations')) {
            $numberView = stt_get_term_meta($termID, 'stt_number_view');
            $numberView++;
            update_term_meta($termID, 'stt_number_view', $numberView);
        }
    }

    add_action('template_redirect', 'stt_update_destination_view');
}

if (!function_exists('stt_list_comments')) {
    function stt_list_comments($comment, $args, $depth)
    {
        $file = locate_template('shinetheme/application/views/frontend/blog/single/comment-list.php');
        if (is_file($file))
            include($file);
    }
}

function stt_comments_template($pageID = '', $file = '/comments.php', $separate_comments = false)
{
    global $wp_query, $withcomments, $post, $wpdb, $id, $comment, $user_login, $user_ID, $user_identity, $overridden_cpage;

    $oldPost = $post;
    $post = get_post($pageID);
    if (is_null($post)) {
        $post = $oldPost;
        return;
    }
    if (empty($file)) {
        $file = '/comments.php';
    }

    $req = get_option('require_name_email');

    /*
     * Comment author information fetched from the comment cookies.
     */
    $commenter = wp_get_current_commenter();

    /*
     * The name of the current comment author escaped for use in attributes.
     * Escaped by sanitize_comment_cookies().
     */
    $comment_author = $commenter['comment_author'];

    /*
     * The email address of the current comment author escaped for use in attributes.
     * Escaped by sanitize_comment_cookies().
     */
    $comment_author_email = $commenter['comment_author_email'];

    /*
     * The url of the current comment author escaped for use in attributes.
     */
    $comment_author_url = esc_url($commenter['comment_author_url']);

    $comment_args = array(
        'orderby' => 'comment_date_gmt',
        'order' => 'ASC',
        'status' => 'approve',
        'post_id' => $post->ID,
        'no_found_rows' => false,
        'update_comment_meta_cache' => false, // We lazy-load comment meta for performance.
    );

    if (get_option('thread_comments')) {
        $comment_args['hierarchical'] = 'threaded';
    } else {
        $comment_args['hierarchical'] = false;
    }

    if ($user_ID) {
        $comment_args['include_unapproved'] = array($user_ID);
    } else {
        $unapproved_email = wp_get_unapproved_comment_author_email();

        if ($unapproved_email) {
            $comment_args['include_unapproved'] = array($unapproved_email);
        }
    }

    $per_page = 0;
    if (get_option('page_comments')) {
        $per_page = (int)get_query_var('comments_per_page');
        if (0 === $per_page) {
            $per_page = (int)get_option('comments_per_page');
        }

        $comment_args['number'] = $per_page;
        $page = (int)get_query_var('cpage');

        if ($page) {
            $comment_args['offset'] = ($page - 1) * $per_page;
        } elseif ('oldest' === get_option('default_comments_page')) {
            $comment_args['offset'] = 0;
        } else {
            // If fetching the first page of 'newest', we need a top-level comment count.
            $top_level_query = new WP_Comment_Query();
            $top_level_args = array(
                'count' => true,
                'orderby' => false,
                'post_id' => $post->ID,
                'status' => 'approve',
            );

            if ($comment_args['hierarchical']) {
                $top_level_args['parent'] = 0;
            }

            if (isset($comment_args['include_unapproved'])) {
                $top_level_args['include_unapproved'] = $comment_args['include_unapproved'];
            }

            $top_level_count = $top_level_query->query($top_level_args);

            $comment_args['offset'] = (ceil($top_level_count / $per_page) - 1) * $per_page;
        }
    }

    $comment_args = apply_filters('comments_template_query_args', $comment_args);
    $comment_query = new WP_Comment_Query($comment_args);
    $_comments = $comment_query->comments;

    // Trees must be flattened before they're passed to the walker.
    if ($comment_args['hierarchical']) {
        $comments_flat = array();
        foreach ($_comments as $_comment) {
            $comments_flat[] = $_comment;
            $comment_children = $_comment->get_children(
                array(
                    'format' => 'flat',
                    'status' => $comment_args['status'],
                    'orderby' => $comment_args['orderby'],
                )
            );

            foreach ($comment_children as $comment_child) {
                $comments_flat[] = $comment_child;
            }
        }
    } else {
        $comments_flat = $_comments;
    }
    $wp_query->comments = apply_filters('comments_array', $comments_flat, $post->ID);

    $comments = &$wp_query->comments;
    $wp_query->comment_count = count($wp_query->comments);
    $wp_query->max_num_comment_pages = $comment_query->max_num_pages;

    if ($separate_comments) {
        $wp_query->comments_by_type = separate_comments($comments);
        $comments_by_type = &$wp_query->comments_by_type;
    } else {
        $wp_query->comments_by_type = array();
    }

    $overridden_cpage = false;
    if ('' == get_query_var('cpage') && $wp_query->max_num_comment_pages > 1) {
        set_query_var('cpage', 'newest' == get_option('default_comments_page') ? get_comment_pages_count() : 1);
        $overridden_cpage = true;
    }

    if (!defined('COMMENTS_TEMPLATE')) {
        define('COMMENTS_TEMPLATE', true);
    }

    $theme_template = get_stylesheet_directory() . $file;

    $include = apply_filters('comments_template', $theme_template);
    if (file_exists($include)) {
        require($include);
    } elseif (file_exists(TEMPLATEPATH . $file)) {
        require(TEMPLATEPATH . $file);
    } else { // Backward compat code will be removed in a future release
        require(ABSPATH . WPINC . '/theme-compat/comments.php');
    }
    $post = $oldPost;
}

//Pagination taxonomy
function get_tax_navigation($taxonomy = 'category', $pageID = '')
{

    // Make sure the taxonomy is valid and sanitize the taxonomy
    if ('category' !== $taxonomy || 'post_tag' !== $taxonomy) {
        $taxonomy = filter_var($taxonomy, FILTER_SANITIZE_STRING);
        if (!$taxonomy)
            return false;

        if (!taxonomy_exists($taxonomy))
            return false;
    }

    // Get the current term object
    $current_term = get_term($GLOBALS['wp_the_query']->get_queried_object());

    if (is_wp_error($current_term)) {
        $term_arr = get_post_meta($pageID, 'page_term_id');
        $term_id = $term_arr[0];
    } else {
        $term_id = $current_term->term_id;
    }

    // Get all the terms ordered by slug
    $terms = get_terms($taxonomy, ['orderby' => 'slug']);
    // Make sure we have terms before we continue
    if (!$terms)
        return false;

    // Because empty terms stuffs around with array keys, lets reset them
    $terms = array_values($terms);

    // Lets get all the term id's from the array of term objects
    $term_ids = wp_list_pluck($terms, 'term_id');

    /**
     * We now need to locate the position of the current term amongs the $term_ids array. \
     * This way, we can now know which terms are adjacent to the current one
     */
    $current_term_position = array_search($term_id, $term_ids);

    // Set default variables to hold the next and previous terms
    $previous_term = '';
    $next_term = '';

    // Get the previous term
    if (0 !== $current_term_position)
        $previous_term = $terms[$current_term_position - 1];

    // Get the next term
    if (intval(count($term_ids) - 1) !== $current_term_position)
        $next_term = $terms[$current_term_position + 1];

    $link = [];
    // Build the links
    if ($previous_term)
        $link[] = '<a class="stt-prev" href="' . esc_url(get_term_link($previous_term)) . '"><span class="meta-nav" aria-hidden="true">' . '<img src="' . STT_ASSETS_URI . 'images/Arrows-prev.svg" alt="'. stt_get_alt_image() .'">' . esc_html__('Previous Destination', 'layout-blog') . '</span></a>';

    if ($next_term)
        $link[] = '<a class="stt-next" href="' . esc_url(get_term_link($next_term)) . '"><span class="meta-nav" aria-hidden="true">' . esc_html__('Next Destination', 'layout-blog') . '<img src="' . STT_ASSETS_URI . 'images/Arrows-next.svg" alt="'. stt_get_alt_image() .'">' . '</span></a>';

    return implode('', $link);
}

if (!function_exists('stt_esc_data')) {
    function stt_esc_data($var_esc, $type = '')
    {
        switch ($type) {
            case 'html':
                return esc_html($var_esc);
                break;
            case 'attr':
                return esc_attr($var_esc);
                break;
            case 'url':
                return esc_url($var_esc);
                break;
            case 'tags':
                $balance = 'balance';
                $tags = 'Tags';
                $string = call_user_func_array($balance . $tags, [$var_esc]);
                return $string;
                break;
            case '':
            default:
                return $var_esc;
                break;
        }
    }
}