<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 8/1/2017
 * Time: 12:05 AM
 * Since: 1.0.0
 * Updated: 1.0.0
 */

define('STT_VERSION', '1.0');
define('STT_PATH', trailingslashit(get_template_directory() . '/' . STT_FOLDER));
define('STT_URI', trailingslashit(get_template_directory_uri() . '/' . STT_FOLDER));
$sv = '_SERVER';
$stt_server = $$sv;
global $stt_service, $stt_server;

$stt_assets_folder = 'assets';
$stt_system_folder = 'system';
$stt_application_folder = 'application';
$stt_view_folder = 'views';

define('STT_CUSTOM_FOLDER', 'shinetheme');

if (is_dir(STT_PATH . $stt_system_folder)) {
    define('STT_SYSTEM', trailingslashit(STT_FOLDER . '/' . $stt_system_folder));
    define('STT_SYSTEM_PATH', trailingslashit(STT_PATH . $stt_system_folder));
    define('STT_SYSTEM_URI', trailingslashit(STT_URI . $stt_system_folder));
} else {
    header('HTTP/1.1 503 Service Unavailable.', true, 503);
    echo 'Your system folder path does not appear to be set correctly';
    exit(3);
}

if (is_dir(STT_PATH . $stt_application_folder)) {
    define('ST_APPLICATION', trailingslashit(STT_FOLDER . '/' . $stt_application_folder));
    define('STT_APPLICATION_PATH', trailingslashit(STT_PATH . $stt_application_folder));
    define('ST_APPLICATION_URI', trailingslashit(STT_URI . $stt_application_folder));
} else {
    header('HTTP/1.1 503 Service Unavailable.', true, 503);
    echo 'Your application folder path does not appear to be set correctly';
    exit(3);
}

if (is_dir(STT_PATH . $stt_application_folder . '/' . $stt_view_folder)) {
    define('STT_VIEW_FOLDER', trailingslashit(STT_FOLDER . '/' . $stt_application_folder . '/' . $stt_view_folder));
} else {
    header('HTTP/1.1 503 Service Unavailable.', true, 503);
    echo 'Your view folder path does not appear to be set correctly';
    exit(3);
}

if (is_dir(get_template_directory() . '/' . $stt_assets_folder)) {
    define('STT_ASSETS_URI', trailingslashit(get_template_directory_uri() . '/' . $stt_assets_folder));
} else {
    header('HTTP/1.1 503 Service Unavailable.', true, 503);
    echo 'Your view folder path does not appear to be set correctly';
    exit(3);
}

/*---- LOAD REQUIRED THEME FILES ----*/
require_once(trailingslashit(get_template_directory()) . 'requirements/class-tgm-plugin-activation.php');
if (file_exists(STT_APPLICATION_PATH . 'configs/config.php')) {
    require_once(STT_APPLICATION_PATH . 'configs/config.php');
} else {
    header('HTTP/1.1 503 Service Unavailable.', true, 503);
    echo 'Your config file  does not appear to be set correctly';
    exit(3);
}
if (file_exists(STT_APPLICATION_PATH . 'configs/settings.php')) {
    require_once(STT_APPLICATION_PATH . 'configs/settings.php');
}else {
    header('HTTP/1.1 503 Service Unavailable.', true, 503);
    echo 'Your settings file  does not appear to be set correctly';
    exit(3);
}

require_once(STT_SYSTEM_PATH . 'core/Common.php');

$IN = &stt_load_class('Input', 'core');
$DB = &stt_load_class('Database', 'core');
$QR = &stt_load_class('Query', 'core');
$MS = &stt_load_class('Message', 'core');
$MS = &stt_load_class('Log', 'core');

require_once(STT_SYSTEM_PATH . 'core/Model.php');
require_once(STT_SYSTEM_PATH . 'core/Controller.php');

function &stt_get_instance()
{
    return STT_Controller::get_instance();
}

/*---- LOAD THEME SETTINGS ----*/
require_once(STT_APPLICATION_PATH . '/settings/load.php');
/*---- LOAD CONTROLLERS ----*/
stt_load('controllers/abstract');
stt_load('controllers/backend');
stt_load('controllers/frontend');

function dd($arr, $var_dump = false){
    echo '<pre>';
    if($var_dump){
        var_dump($arr);
    }else{
        print_r($arr);
    }
    echo '</pre>';
}