<?php

    /**
     * Created by PhpStorm.
     * User: Administrator
     * Date: 8/1/2017
     * Time: 1:20 AM
     * Since: 1.0
     * Updated: 1.0
     */
    class STT_Controller
    {
        /**
         * STT_Controller constructor.
         */
        public function __construct()
        {
            foreach ( stt_is_loaded() as $var => $class ) {
                $this->$var = &stt_load_class( $class );
            }

            $this->load =& stt_load_class( 'Loader', 'core' );
            $this->load->initialize();
        }

        /**
         * @param string $key
         */
        protected function checkSecurity( $key = 'security' ) {
		    check_ajax_referer( $key, '_s', true );
	    }

        /**
         * @param array $data
         */
        protected function sendJson( $data = array() ) {
            wp_send_json( $data );
        }

        /**
         * @param string $message
         * @param array $extra
         */
        protected function sendError( $message = '', $extra = [] ) {
            $data['type']    = 'error';
            $data['status']  = 0;
            $data['message'] = $message;

            $data = array_merge( $data, $extra );
            wp_send_json( $data );
        }

        /**
         * @return STT_Controller
         */
        public static function &get_instance()
        {
            static $instance;
            if ( is_null( $instance ) ) {
                $instance = new self();
            }

            return $instance;
        }

    }