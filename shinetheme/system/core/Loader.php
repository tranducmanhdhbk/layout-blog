<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 8/1/2017
 * Time: 1:47 AM
 * Since: 1.0
 * Updated: 1.0
 */
class STT_Loader
{
    /**
     * @var array
     */
    protected $_classes = [];
    /**
     * @var array
     */
    protected $_helpers = [];
    /**
     * @var array
     */
    protected $_models = [];
    /**
     * @var
     */
    protected $_base_classes;
    /**
     * @var array
     */
    protected $_loaded_files = [];

    /**
     * @return $this
     * @since   1.0.0
     * @updated 1.0.0
     */
    public function initialize()
    {
        $this->_classes = [];
        $this->_loaded_files = [];
        $this->_models = [];
        $this->_base_classes = &stt_is_loaded();
        $this->_autoloader();

        return $this;
    }

    /**
     * @return bool
     */
    public function _autoloader()
    {
        $file = locate_template(ST_APPLICATION . 'configs/autoload.php');
        if (is_file($file)) {
            require_once($file);
        }
        if (!isset($autoload)) {
            return false;
        }

        if (isset($autoload['plugins']) && count($autoload['plugins'])) {
            foreach ($autoload['plugins'] as $path => $file) {
                $this->file($file, '/plugins/' . $path);
            }
        }

        if (isset($autoload['libraries']) && count($autoload['libraries'])) {
            $this->library($autoload['libraries']);
        }

        if (isset($autoload['helpers']) && count($autoload['helpers'])) {
            $this->helper($autoload['helpers']);
        }

    }

    /**
     * @param string $name
     * @param string $path
     * @param null $params
     * @param bool $return
     *
     * @return string
     * @since   1.0.0
     * @updated 1.0.0
     */
    public function view($name = '', $path = '', $params = null, $return = false)
    {
        $file = locate_template(ST_APPLICATION . 'views/' . $path . '/' . $name . '.php');
        if (is_file($file)) {
            if (!empty($params) && is_array($params)) {
                extract($params);
            }
            ob_start();

            $custom_file = locate_template(STT_CUSTOM_FOLDER . $path . '/' . $name . '.php');
            if (is_file($custom_file)) {
                require($custom_file);
            } else {
                require($file);
            }

            $buffer = @ob_get_clean();
            if ($return) {
                return $buffer;
            } else {
                echo stt_esc_data($buffer);
            }
        } else {
            die('Unable to load the requested file: views/' . $path . '/' . $name . '.php');
        }
    }
    public function customView($name = '', $path = '', $params = null, $return = false)
    {
        $file = locate_template(ST_APPLICATION . $path . '/' . $name . '.php');
        if (is_file($file)) {
            if (!empty($params) && is_array($params)) {
                extract($params);
            }
            ob_start();

            $custom_file = locate_template(STT_CUSTOM_FOLDER . $path . '/' . $name . '.php');
            if (is_file($custom_file)) {
                require($custom_file);
            } else {
                require($file);
            }

            $buffer = @ob_get_clean();
            if ($return) {
                return $buffer;
            } else {
                echo stt_esc_data($buffer);
            }
        } else {
            die('Unable to load the requested file: views/' . $path . '/' . $name . '.php');
        }
    }

    /**
     * @param        $filename
     * @param string $path
     *
     * @since   1.0.0
     * @updated 1.0.0
     */
    public function file($filename, $path = '')
    {
        foreach ([STT_SYSTEM, ST_APPLICATION] as $basepath) {
            $file = locate_template($basepath . $path . '/' . $filename . '.php');
            if (is_file($file)) {
                if (!in_array($file, $this->_loaded_files)) {
                    require_once($file);
                    $this->_loaded_files[] = $file;
                }
            }
        }
    }

    /**
     * @param array $helpers
     *
     * @since   1.0.0
     * @updated 1.0.0
     */
    public function helper($helpers = [])
    {
        $helpers = (array)$helpers;
        foreach ($helpers as $helper) {
            if (isset($this->_helpers[$helper])) {
                continue;
            }
            foreach ([STT_SYSTEM, ST_APPLICATION] as $basepath) {
                $file = locate_template($basepath . '/helpers/' . $helper . '.php');
                if (is_file($file)) {
                    require_once($file);
                    $this->_helpers[$helper] = true;
                }
            }
            if (!isset($this->_helpers[$helper])) {
                die('Unable to load the requested file: helpers/' . $helper . '.php');
            }
        }
    }


    /**
     * @param array $helpers
     */
    public function library($helpers = [])
    {
        $helpers = (array)$helpers;
        foreach ($helpers as $helper) {
            if (isset($this->_helpers[$helper])) {
                continue;
            }
            foreach ([STT_SYSTEM, ST_APPLICATION] as $basepath) {
                $file = locate_template($basepath . '/libraries/' . $helper . '.php');
                if (is_file($file)) {
                    require_once($file);
                    $this->_helpers[$helper] = true;
                }
            }
            if (!isset($this->_helpers[$helper])) {
                die('Unable to load the requested file: libraries/' . $helper . '.php');
            }
        }
    }

    /**
     * @param      $class
     * @param null $params
     * @param null $object_name
     *
     * @return null
     * @since   1.0.0
     * @updated 1.0.0
     */
    protected function _load_class($class, $params = null, $object_name = null)
    {
        $class = str_replace('.php', '', trim($class, '/'));
        $subdir = '';
        if (($last_slash = strrpos($class, '/')) !== false) {
            $subdir = substr($class, 0, $last_slash + 1);
            $class = substr($class, $last_slash + 1);
        }
        $is_duplicate = false;
        foreach ([ucfirst($class), strtolower($class)] as $class) {
            foreach ([STT_SYSTEM, ST_APPLICATION] as $basepath) {
                $subclass = locate_template($basepath . '/libraries/' . $subdir . $class . '.php');
                if (is_file($subclass)) {
                    if (in_array($subclass, $this->_loaded_files)) {
                        if (!is_null($object_name)) {
                            $st = &stt_get_instance();
                            if (!isset($st->$object_name)) {
                                return $this->_init_class($class, $params, $object_name);
                            }
                        }
                        $is_duplicate = true;

                        return null;
                    }
                    require_once($subclass);
                    $this->_loaded_files[] = $subclass;
                    $is_duplicate = true;

                    return $this->_init_class($class, $params, $object_name);
                }
            }
        }
        if ($is_duplicate == false) {
            die("Unable to load the requested class: " . $class);
        }
    }

    /**
     * @param      $class
     * @param bool $config
     * @param null $object_name
     *
     * @return null
     * @since   1.0.0
     * @updated 1.0.0
     */
    protected function _init_class($class, $config = false, $object_name = null)
    {
        $name = $class;
        $class = 'STT_' . $class;
        if (class_exists($class)) {
            $name = $class;
        }
        if (!class_exists($name)) {
            die("Non-existent class: " . $class);
        }
        $class = strtolower($class);
        if (is_null($object_name)) {
            $classvar = $class;
        } else {
            $classvar = $object_name;
        }
        $this->_classes[$class] = $classvar;
        $st = &stt_get_instance();
        if ($config !== null) {
            $st->$classvar = new $name($config);
        } else {
            $st->$classvar = new $name;
        }

        return null;
    }

    /**
     * @param $model
     * @param string $name
     * @return $this
     */
    public function model($model, $object = false)
    {
        if (is_array($model)) {
            foreach ($model as $babe) {
                $this->model($babe);
            }

            return $this;
        }
        if ($model === '') {
            return $this;
        }

        $name = $model;
        $st = &stt_get_instance();
        if ($object) {
            $st = $object;
        }
        if (isset($st->$name)) {
            return $this;
        }
        $model = strtolower($model);
        $file = locate_template(ST_APPLICATION . '/models/' . $model . '.php');
        if (is_file($file)) {
            if (!class_exists('STT_Model')) {
                st_load_class('Model', 'core');
            }
            require_once $file;

            $model = ucfirst($model);
            $model = 'STT_' . $model;

            $st->$name = new $model();
        }

        return $this;
    }
}