<?php

    /**
     * Created by PhpStorm.
     * User: Administrator
     * Date: 8/1/2017
     * Time: 9:14 AM
     * Since: 1.0
     * Updated: 1.0
     */
    class STT_Database
    {
        /**
         * @var string
         */
        public $_table_name = '';
        /**
         * @var bool
         */
        public $_meta_table_working = false;
        /**
         * @var array
         */
        public $_columns = [];
        /**
         * @var string
         */
        public $_table_version_id = '';

        /**
         * @param string $version
         */
        public function setVersion( $version = '' )
        {
            $this->_table_version_id = $version;
        }

        /**
         * @param string $table_name
         */
        public function setTableName( $table_name = '' )
        {
            global $wpdb;
            $this->_table_name = $wpdb->prefix . $table_name;
        }

        /**
         * @param array $columns
         */
        public function setDefaultColums( $columns = [] )
        {
            $this->_columns = $columns;
        }

        /**
         * @param string $option_version
         */
        public function updateTable( $option_version = '' )
        {
            if ( !class_exists( 'STT_System' ) || !is_admin() ) {
                return;
            }
            global $wpdb;
            $table_name    = $this->_table_name;
            $table_columns = $this->_columns;
            if ( $wpdb->get_var( $wpdb->prepare( "SHOW TABLES LIKE %s", $table_name ) ) != $table_name ) {
                global $wpdb;
                $charset_collate = $wpdb->get_charset_collate();
                $col_string      = '';
                if ( !empty( $table_columns ) ) {
                    $i = 0;
                    foreach ( $table_columns as $key => $value ) {
                        $s_char = ',';
                        if ( $i == count( $table_columns ) - 1 ) {
                            $s_char = '';
                        }
                        if ( isset( $value[ 'AUTO_INCREMENT' ] ) && $value[ 'AUTO_INCREMENT' ] ) {
                            $col_string .= ' ' . sprintf( '%s %s NOT NULL AUTO_INCREMENT PRIMARY KEY', $key, $value[ 'type' ] ) . $s_char;
                        } else {
                            $prefix = '';
                            switch ( strtolower( $value[ 'type' ] ) ) {
                                case "varchar":
                                case "float":
                                case "int":
                                case "text":
                                    if ( isset( $value[ 'length' ] ) && $value[ 'length' ] ) {
                                        $prefix = '(' . $value[ 'length' ] . ')';
                                    }
                                    break;
                            }
                            $col_string .= ' ' . $key . ' ' . $value[ 'type' ] . $prefix . $s_char;
                        }
                        $i++;
                    }
                }
                $wpdb->query( "CREATE TABLE {$table_name} ( {$col_string} ) {$charset_collate}" );
                update_option( $option_version, $this->_table_version_id );
                if ( $wpdb->get_var( $wpdb->prepare( "SHOW TABLES LIKE %s", $table_name ) ) != $table_name ) {
                    $this->_meta_table_working = false;
                } else {
                    $this->_meta_table_working = true;
                }
            } else {
                $this->_meta_table_working = true;
            }

            if ( $this->_meta_table_working ) {
                $db_version = get_option( $option_version );
                if ( !$db_version ) $db_version = '0.0.0';
                if ( $db_version ) {
                    if ( version_compare( $db_version, $this->_table_version_id, '<' ) ) {
                        $this->_upgradeTable();
                        update_option( $option_version, $this->_table_version_id, false );
                    }
                }
            }
        }

        /**
         * @since   1.0.0
         * @updated 1.0.0
         */
        private function _upgradeTable()
        {
            global $wpdb;
            $table_name    = $this->_table_name;
            $table_columns = $this->_columns;

            $insert_key = $table_columns;
            $delete_key = [];
            $update_key = [];

            $query      = "SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = %s AND TABLE_NAME = %s";
            $old_coumns = $wpdb->get_results( $wpdb->prepare( $query, [ $wpdb->dbname, $table_name ] ) );

            if ( $old_coumns and !empty( $old_coumns ) ) {
                foreach ( $old_coumns as $key => $value ) {
                    unset( $insert_key[ $value->COLUMN_NAME ] );
                    if ( isset( $table_columns[ $value->COLUMN_NAME ] ) ) {
                        if ( strtolower( $table_columns[ $value->COLUMN_NAME ][ 'type' ] ) != strtolower( $value->DATA_TYPE ) ) {
                            $update_key[ $value->COLUMN_NAME ] = $table_columns[ $value->COLUMN_NAME ];
                        }
                    } else {
                        $delete_key[] = $value->COLUMN_NAME;
                    }
                }
            }

            if ( !empty( $insert_key ) ) {
                $insert_col_string = '';
                foreach ( $insert_key as $key => $value ) {
                    if ( empty( $value[ 'type' ] ) ) continue;
                    $prefix = '';
                    switch ( strtolower( $value[ 'type' ] ) ) {
                        case "varchar":
                            if ( isset( $value[ 'length' ] ) && $value[ 'length' ] ) {
                                $prefix = '(' . $value[ 'length' ] . ')';
                            }
                            break;
                    }

                    $col_type = $value[ 'type' ];
                    $insert_col_string .= " ADD $key $col_type" . $prefix . ',';
                }
                $insert_col_string = substr( $insert_col_string, 0, -1 );
                $query             = sprintf( "ALTER TABLE %s %s ", $table_name, $insert_col_string );

                $wpdb->query( $query );
            }

            if ( !empty( $update_key ) ) {
                $update_col_string = '';
                foreach ( $update_key as $key => $value ) {
                    $prefix = '';
                    switch ( strtolower( $value[ 'type' ] ) ) {
                        case "varchar":
                            if ( isset( $value[ 'length' ] ) and $value[ 'length' ] ) {
                                $prefix = '(' . $value[ 'length' ] . ')';
                            }
                            break;
                    }

                    $col_type = $value[ 'type' ];
                    $update_col_string .= " MODIFY $key $col_type" . $prefix . ',';
                }
                $update_col_string = substr( $update_col_string, 0, -1 );
                $query             = sprintf( "ALTER TABLE %s %s ", $table_name, $update_col_string );

                $wpdb->query( $query );
            }

            if ( !empty( $delete_key ) ) {
                $delete_query_string = false;
                foreach ( $delete_key as $val ) {
                    $delete_query_string .= ' DROP COLUMN ' . $val . ',';
                }
                $delete_query_string = substr( $delete_query_string, 0, -1 );

                $query = sprintf( "ALTER TABLE %s %s", $table_name, $delete_query_string );

                $wpdb->query( $query );
            }
        }

        /**
         * @return array
         * @since   1.0.0
         * @updated 1.0.0
         */
        public function getColumns()
        {
            return $this->_columns;
        }
    }