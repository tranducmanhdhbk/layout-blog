<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 6/14/2017
 * Time: 7:42 PM
 * Since: 1.0
 * Updated: 1.0
 */
class STT_Query
{

    /**
     * @var
     */
    protected $table_name;

    /**
     * @var string
     */
    protected $table_slug;
    /**
     * @var string
     */
    protected $table_key = 'id';

    /**
     * @var array
     */
    protected $_where_query = [];
    /**
     * @var array
     */
    protected $_join_query = [];
    /**
     * @var array
     */
    protected $_select_query = [];
    /**
     * @var array
     */
    protected $_order_query = [];
    /**
     * @var array
     */
    protected $_limit_query = [];
    /**
     * @var array
     */
    protected $_last_query = [];
    /**
     * @var array
     */
    protected $_last_result = [];
    /**
     * @var array
     */
    protected $_groupby = [];
    /**
     * @var array
     */
    protected $_having = [];
    /**
     * @var array
     */
    protected $_like_query = [];


    public function __construct()
    {

    }

    public function set_table($table_name = '', $slug = '')
    {
        $this->table_name = $table_name;
        $this->table_slug = $slug;

        return $this;
    }

    /**
     * @return array
     */
    public function last_query()
    {
        return $this->_last_query;
    }

    /**
     * @param        $key
     * @param        $value
     * @param string $format
     *
     * @return $this
     */
    public function like($key, $value, $format = 'both')
    {
        if (is_array($key) and !empty($key)) {
            foreach ($key as $k1 => $v1) {
                $this->like($v1['key'], $v1['value'], $v1['format']);
            }

            return $this;
        }
        if (is_string($key)) {
            $this->_like_query[] = [
                'key' => $key,
                'value' => $value,
                'clause' => "AND",
                'format' => $format
            ];
        }

        return $this;
    }

    /**
     * @param        $key
     * @param        $value
     * @param string $format
     *
     * @return $this
     */
    public function or_like($key, $value, $format = 'both')
    {
        if (is_array($key) and !empty($key)) {
            foreach ($key as $k1 => $v1) {
                $this->or_like($v1['key'], $v1['value'], $v1['format']);
            }

            return $this;
        }
        if (is_string($key)) {
            $this->_like_query[] = [
                'key' => $key,
                'value' => $value,
                'clause' => "OR",
                'format' => $format
            ];
        }

        return $this;
    }


    /**
     * @param      $key
     * @param bool $value
     * @param bool $raw_where
     *
     * @return $this
     */
    public function where($key, $value = false, $raw_where = false)
    {
        if (is_array($key) and !empty($key)) {
            foreach ($key as $k1 => $v1) {
                $this->where($k1, $v1, $raw_where);
            }

            return $this;
        }
        if (is_string($key)) {
            $this->_where_query[] = [
                'key' => $key,
                'value' => $value,
                'clause' => 'and',
                'is_raw' => $raw_where
            ];
        }

        return $this;
    }


    /**
     * @param      $key
     * @param bool $value
     * @param bool $raw_where
     *
     * @return $this
     */
    public function or_where($key, $value = false, $raw_where = false)
    {
        if (is_array($key) and !empty($key)) {
            foreach ($key as $k1 => $v1) {
                $this->or_where($k1, $v1, $raw_where);
            }

            return $this;
        }
        if (is_string($key)) {
            $this->_where_query[] = [
                'key' => $key,
                'value' => $value,
                'clause' => 'or',
                'is_raw' => $raw_where
            ];
        }

        return $this;
    }

    /**
     * @param $column_name
     *
     * @return $this
     */
    public function select($column_name)
    {
        if (is_array($column_name) and !empty($column_name)) {
            foreach ($column_name as $v) $this->select($v);
        }

        if (is_string($column_name)) {
            $this->_select_query[] = $column_name;
        }

        return $this;
    }


    /**
     * @param        $table
     * @param string $table_slug
     * @param        $on_clause
     * @param string $join_key
     * @param bool $raw
     *
     * @return $this
     */
    public function join($table, $table_slug = '', $on_clause, $join_key = 'INNER', $raw = false)
    {
        if (is_array($table) and !empty($table)) {
            foreach ($table as $v) {
                $v = wp_parse_args($v, [
                    'table' => '',
                    'as' => '',
                    'on' => '',
                    'keyword' => '',
                    'raw' => false
                ]);
                $this->join($v['table'], $v['as'], $v['on'], $v['keyword'], $v['raw']);
            }
        }

        if (is_string($table)) {
            $this->_join_query[] = ['table' => $table, 'as' => $table_slug, 'on' => $on_clause, 'keyword' => $join_key, 'raw' => $raw];
        }

        return $this;
    }

    /**
     * @param        $key
     * @param string $value
     *
     * @return $this
     */
    public function orderby($key, $value = 'asc')
    {
        if (is_array($key) and !empty($key)) {
            foreach ($key as $k1 => $v1) {
                $this->orderby($k1, $v1);
            }

            return $this;
        }
        if (is_string($key)) {
            $this->_order_query[$key] = $value;
        }

        return $this;
    }

    /**
     * @param $key
     *
     * @return $this
     */
    public function groupby($key)
    {
        if (is_array($key) and !empty($key)) {
            foreach ($key as $v1) {
                $this->groupby($v1);
            }
        }
        if (is_string($key)) {
            $this->_groupby[] = $key;
        }

        return $this;
    }

    /**
     * @param $key
     *
     * @return $this
     */
    public function having($key)
    {
        if (is_array($key) and !empty($key)) {
            foreach ($key as $v1) {
                $this->having($v1);
            }
        }
        if (is_string($key)) {
            $this->_having[] = $key;
        }

        return $this;
    }

    /**
     * @param     $key
     * @param int $value
     *
     * @return $this
     */
    public function limit($key, $value = 0)
    {
        $this->_limit_query[0] = (int)$key;
        $this->_limit_query[1] = (int)$value;
        return $this;
    }


    /**
     * @param bool $limit
     * @param bool $offset
     * @param string $result_type
     *
     * @return $this
     */
    public function get($limit = false, $offset = false, $result_type = ARRAY_A)
    {
        if ($limit) {
            $this->limit($limit, $offset);
        }
        global $wpdb;
        $query = $this->_get_query();
        $this->_last_query = $query;
        $this->_last_result = $wpdb->get_results($query, $result_type);

        return $this;
    }

    public function update($table = '', $data = [], $where = [])
    {
        global $wpdb;
        $table = $wpdb->prefix . $table;
        return $wpdb->update($table, $data, $where);
    }

    public function insert($table = '', $data = [])
    {
        global $wpdb;
        $table = $wpdb->prefix . $table;
        $wpdb->insert($table, $data);

        return $wpdb->insert_id;
    }

    public function delete($table = '', $where = [])
    {
        global $wpdb;
        $table = $wpdb->prefix . $table;
        return $wpdb->delete($table, $where);
    }


    /**
     * @param bool $table_name
     *
     * @return $this
     */
    public function table($table_name = false)
    {
        if ($table_name) {
            $this->table_name = $table_name;
        }

        return $this;
    }


    /**
     * @return bool|mixed
     */
    public function row()
    {
        $data = isset($this->_last_result[0]) ? $this->_last_result[0] : false;
        $this->_clear_query();

        return $data;

    }

    public function total()
    {
        $total = 0;
        if (isset($this->_last_result)) {
            global $wpdb;
            $total = $wpdb->get_var("SELECT FOUND_ROWS();");
        }

        return (int)$total;
    }

    public function count()
    {
        $total = 0;
        if (isset($this->_last_result)) {
            $total = count($this->_last_result);
        }

        return (int)$total;
    }


    /**
     * @return array
     */
    public function result()
    {
        $data = $this->_last_result;
        $this->_clear_query();

        return $data;
    }


    /**
     * @param $id
     *
     * @return bool|mixed
     */
    public function find($id)
    {

        if (!$this->table_key) return false;

        $data = $this->where($this->table_key, $id)->limit(1)->get()->row();
        $this->_clear_query();

        return $data;
    }


    /**
     * @param $key
     * @param $id
     *
     * @return bool|mixed
     */
    public function find_by($key, $id)
    {
        $this->_clear_query();
        if (is_array($key)) {
            foreach ($key as $k => $v) {
                $this->where($k, $v);
            }
            $data = $this->limit(1)->get()->row();
        } else {
            $data = $this->where($key, $id)->limit(1)->get()->row();
        }


        $this->_clear_query();

        return $data;
    }

    /**
     * @param $key
     * @param $value
     *
     * @return array|bool
     */
    public function find_all_by($key, $value)
    {
        if (!$this->table_key) return false;

        $data = $this->where($key, $value)->get()->result();
        $this->_clear_query();

        return $data;
    }


    /**
     * @return bool|string
     */
    public function _get_query()
    {
        global $wpdb;
        $table_name = $wpdb->prefix . $this->table_name;

        if (!empty($this->_select_query)) {
            $select = implode(',', $this->_select_query);
        } else {
            $select = '*';
        }

        $join = false;
        if (!empty($this->_join_query)) {
            foreach ($this->_join_query as $j) {
                $j = wp_parse_args($j, [
                    'tale' => false,
                    'on' => false,
                    'as' => false,
                    'keyword' => false
                ]);

                if (!$j['table'] or !$j['on']) continue;

                if (!$j['raw']) {
                    $table = $wpdb->prefix . $j['table'];
                } else {
                    $table = $j['table'];
                }

                $j['on'] = str_replace($j['table'], $table, $j['on']);
                $j['on'] = str_replace($this->table_name, $table_name, $j['on']);
                $slug = (!empty($j['as'])) ? ' AS ' . $j['as'] . ' ' : '';
                $join .= ' ' . $j['keyword'] . ' JOIN ' . $table . $slug . ' ON ' . $j['on'];
            }
        }

        $where = ' WHERE 1=1 ';
        if (!empty($this->_where_query)) {

            foreach ($this->_where_query as $key => $value) {
                $value = wp_parse_args($value, [
                    'key' => false,
                    'value' => false,
                    'clause' => 'and',
                    'is_raw' => false
                ]);
                if (!$value['is_raw']) {
                    $last = substr($value['key'], -1);
                    switch ($last) {
                        case ">":
                        case "<":
                        case "=":
                            $where .= $wpdb->prepare(' ' . $value['clause'] . ' ' . $value['key'] . '%s ', [$value['value']]);
                            break;
                        default:
                            $where .= $wpdb->prepare(' ' . $value['clause'] . ' ' . $value['key'] . '=%s ', [$value['value']]);
                            break;

                    }
                } else {
                    $where .= ' ' . $value['clause'] . ' ' . $value['key'];
                }

            }
        }

        if (!empty($this->_like_query)) {

            foreach ($this->_like_query as $key => $value) {
                $value = wp_parse_args($value,
                    [
                        'key' => false,
                        'value' => false,
                        'clause' => 'AND',
                        'format' => 'both'
                    ]);
                switch ($value['format']) {
                    case "before":
                        $where .= ' ' . $value['clause'] . ' ' . $value['key'] . " LIKE '%" . $wpdb->_real_escape($value['value']) . "' ";
                        break;
                    case "after":
                        $where .= ' ' . $value['clause'] . ' ' . $value['key'] . " LIKE '" . $wpdb->_real_escape($value['value']) . "%' ";
                        break;

                    default:
                        $where .= ' ' . $value['clause'] . ' ' . $value['key'] . " LIKE '%" . $wpdb->_real_escape($value['value']) . "%' ";
                        break;

                }
            }
        }


        $order = false;
        if (!empty($this->_order_query)) {
            $order = ' ORDER BY ';
            foreach ($this->_order_query as $k => $v) {
                $order .= ' ' . $k . ' ' . $v . ',';
            }

            $order = substr($order, 0, -1);
        }

        $groupby = false;

        if (!empty($this->_groupby)) {
            $groupby = ' GROUP BY ';
            foreach ($this->_groupby as $k => $v) {
                $groupby .= ' ' . $v . ',';
            }

            $groupby = substr($groupby, 0, -1);

            $having = false;
            if (!empty($this->_having)) {
                $having .= ' HAVING ';
                foreach ($this->_having as $k => $v) {
                    $having .= ' ' . $v . ' AND ';
                }

                $having = substr($having, 0, -4);

                $groupby .= ' ' . $having;
            }

        }

        $limit = false;
        if (!empty($this->_limit_query)) {
            $limit = ' LIMIT ';

            $offset = !empty($this->_limit_query[1]) ? $this->_limit_query[1] : 0;

            $limit .= $offset . ',' . $this->_limit_query[0];

        }
        $slug = '';
        if (!empty($this->table_slug)) {
            $slug = ' AS ' . $this->table_slug;
        }
        if ($select) {
            $query = sprintf("SELECT SQL_CALC_FOUND_ROWS %s FROM %s %s ", $select, $table_name, $slug);

            $query .= $join;
            $query .= $where;
            $query .= $groupby;
            $query .= $order;
            $query .= $limit;

            return $query;
        }

        return false;
    }

    /**
     * @param bool $prefix
     *
     * @return string
     */
    public function get_table_name($prefix = true)
    {
        global $wpdb;

        if ($prefix)
            return $table_name = $wpdb->prefix . $this->table_name;
        else
            return $this->table_name;
    }


    /**
     *
     */
    public function _clear_query()
    {
        $this->_where_query = [];
        $this->_select_query = [];
        $this->_order_query = [];
        $this->_limit_query = [];
        $this->_join_query = [];
        $this->_groupby = [];
        $this->_having = [];
        $this->table_name = '';
        $this->table_slug = '';
    }
}
