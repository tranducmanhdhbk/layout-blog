<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 11/12/2016
 * Time: 2:51 PM
 * Since 1.0.0
 * Updated 1.0.0
 */
if (!class_exists('ST_Message')) {
    /**
     * Class ST_Message
     */
    class STT_Message
    {
        /**
         * @var string
         */
        protected $message = '';

        /**
         * ST_Message constructor.
         */
        public function __construct()
        {
            $this->message = '';
        }

        /**
         * @param string $key
         * @param string $message
         * @param string $type
         *
         * @since   1.0.0
         * @updated 1.0.0
         */
        public function set_message($key = '', $message = '', $type = 'success')
        {
            $_SESSION['st_messages'][$key] = [
                'type' => $type,
                'message' => $message
            ];
            $this->message = $_SESSION['st_messages'];
        }

        /**
         * @param string $key
         *
         * @return string
         * @since   1.0.0
         * @updated 1.0.0
         */
        public function get_message($key = '')
        {
            if (isset($_SESSION['st_messages'][$key])) {
                $this->message = $_SESSION['st_messages'][$key];
                unset($_SESSION['st_messages'][$key]);
            }

            return isset($this->message['message']) ? $this->message['message'] : '';
        }

        /**
         * @param string $key
         *
         * @return string
         * @since   1.0.0
         * @updated 1.0.0
         */
        public function get_type($key = '')
        {
            if (empty($this->message) && isset($_SESSION['st_messages'])) {
                $this->message = $_SESSION['st_messages'];
                unset($_SESSION['st_messages']);
            }
            if (!empty($key)) {
                return isset($this->message[$key]['type']) ? $this->message[$key]['type'] : '';
            } else {
                return isset($this->message) ? $this->message : '';
            }
        }

        /**
         * @param string $key
         *
         * @return string
         * @since   1.0.0
         * @updated 1.0.0
         */
        public function show_message($key = '')
        {
            $html = '';
            if (isset($_SESSION['st_messages'][$key])) {
                $this->message = $_SESSION['st_messages'][$key];
                unset($_SESSION['st_messages'][$key]);
                $html = '<div class="alert alert-' . $this->message['type'] . '">' . $this->message['message'] . '</div>';
            }

            return stt_esc_data($html);
        }

        public function has_error()
        {
            if (!empty($this->message)) {
                return true;
            }

            return false;
        }
    }
}