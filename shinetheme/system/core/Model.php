<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 5/29/2019
 * Time: 2:00 PM
 */
if (!class_exists('STT_Model')) {
    class STT_Model
    {
        public function __construct()
        {
            $this->database = new STT_Database();
            $this->query = new STT_Query();
            if (class_exists('STT_Inject')) {
                $this->inject = new STT_Inject();
            }
        }
    }
}