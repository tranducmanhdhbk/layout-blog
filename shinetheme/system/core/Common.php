<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 8/1/2017
 * Time: 12:34 AM
 * Since: 1.0
 * Updated: 1.0
 */

function stt_is_php($version)
{
    static $_is_php;
    $version = (string)$version;

    if (!isset($_is_php[$version])) {
        $_is_php[$version] = version_compare(PHP_VERSION, $version, '>=');
    }

    return $_is_php[$version];
}

function &stt_load_class($class, $directory = 'libraries', $param = null)
{
    static $_classes = [];

    if (isset($_classes[$class])) {
        return $_classes[$class];
    }

    $name = false;

    foreach ([STT_APPLICATION_PATH, STT_SYSTEM_PATH] as $path) {
        if (file_exists($path . $directory . '/' . $class . '.php')) {
            $name = 'STT_' . $class;

            if (class_exists($name, false) === false) {
                require_once($path . $directory . '/' . $class . '.php');
            }

            break;
        }
    }

    if ($name === false) {
        header('HTTP/1.1 503 Service Unavailable.', true, 503);
        echo 'Unable to locate the specified class: ' . $class . '.php';
        exit(3);
    }

    // Keep track of what we just loaded
    stt_is_loaded($class);

    $_classes[$class] = isset($param) ? new $name($param) : new $name();

    return $_classes[$class];
}

function &stt_is_loaded($class = '')
{
    static $_is_loaded = [];

    if ($class !== '') {
        $_is_loaded[strtolower($class)] = $class;
    }

    return $_is_loaded;
}

function &stt_get_config(Array $replace = [])
{
    static $config;

    if (empty($config)) {
        $file_path = STT_APPLICATION_PATH . 'configs/config.php';
        if (file_exists($file_path)) {
            require($file_path);
        }

        if (!isset($config) OR !is_array($config)) {
            header('HTTP/1.1 503 Service Unavailable.', true, 503);
            echo 'Your config file does not appear to be formatted correctly.';
            exit(3);
        }
    }

    foreach ($replace as $key => $val) {
        $config[$key] = $val;
    }

    return $config;
}


function stt_config_item($item)
{
    static $_config;

    if (empty($_config)) {
        $_config[0] =& stt_get_config();
    }

    return isset($_config[0][$item]) ? $_config[0][$item] : null;
}

function &stt_get_settings(Array $replace = [])
{
    static $settings;

    if (empty($settings)) {
        $file_path = STT_APPLICATION_PATH . 'configs/settings.php';
        if (file_exists($file_path)) {
            require($file_path);
        }

        if (!isset($settings) OR !is_array($settings)) {
            header('HTTP/1.1 503 Service Unavailable.', true, 503);
            echo 'Your settings file does not appear to be formatted correctly.';
            exit(3);
        }
    }

    foreach ($replace as $key => $val) {
        $settings[$key] = $val;
    }

    return $settings;
}

function stt_setting_item($item)
{
    static $_setting;

    if (empty($_setting)) {
        $_setting[0] =& stt_get_settings();
    }

    return isset($_setting[0][$item]) ? $_setting[0][$item] : null;
}

function stt_is_https()
{
    if (!empty(stt_server('HTTPS')) && strtolower(stt_server('HTTPS')) !== 'off') {
        return true;
    } elseif (!empty(stt_server('HTTP_X_FORWARDED_PROTO')) && strtolower(stt_server('HTTP_X_FORWARDED_PROTO')) === 'https') {
        return true;
    } elseif (!empty(stt_server('HTTP_FRONT_END_HTTPS')) && strtolower(stt_server('HTTP_FRONT_END_HTTPS')) !== 'off') {
        return true;
    }

    return false;
}

function stt_log_message($level, $message)
{
    static $_log;

    if ($_log === null) {
        $_log[0] =& stt_load_class('Log', 'core');
    }
    $_log[0]->write_log($level, $message);
}

function stt_is_really_writable($file)
{
    // If we're on a Unix server with safe_mode off we call is_writable
    if (DIRECTORY_SEPARATOR === '/' && (stt_is_php('5.4') OR !ini_get('safe_mode'))) {
        return is_writable($file);
    }

    if (is_dir($file)) {
        $file = rtrim($file, '/') . '/' . md5(mt_rand());
        if (($fp = stt_open($file, 'ab')) === false) {
            return false;
        }

        stt_close($fp);
        @chmod($file, 0777);
        @unlink($file);

        return true;
    } elseif (!is_file($file) OR ($fp = stt_open($file, 'ab')) === false) {
        return false;
    }

    stt_close($fp);

    return true;
}

function stt_load($folder)
{
    $files = glob(STT_APPLICATION_PATH . '/' . $folder . "/*");
    if (!empty($files)) {
        foreach ($files as $key => $file) {
            if (is_file($file)) {
                $filename = stt_path_info($file);
                if (strlen($filename) >= 5) {
                    if (substr($filename, -4) == '.php') {
                        $path = locate_template(ST_APPLICATION . '/' . $folder . '/' . $filename);
                        if (is_file($path)) {
                            $name = substr($filename, 0, -4);
                            $name = 'STT_' . $name;
                            require_once($path);
                            if (class_exists($name)) {
                                $testClass = new ReflectionClass($name);
                                if (!$testClass->isAbstract()) {
                                    if (method_exists($name, 'get_inst')) {
                                        $name::get_inst();
                                    } elseif(method_exists($name, 'not')) {

                                    }else{
                                        new $name();
                                    }
                                }
                            }
                        }
                    }
                }
            } elseif (is_dir($file)) {
                $dir = $folder . '/' . stt_path_info($file);
                stt_load($dir);
            }
        }
    }
}

function stt_path_info($path = '', $return = '')
{
    if ($return == 'dir') {
        $pathinfo = pathinfo($path);
        $result = $pathinfo['dirname'];
    } else {
        $pathinfo = pathinfo($path);
        $result = $pathinfo['basename'];
    }

    return $result;
}
function stt_write($handle, $string)
{
    $f = 'f';
    $write = 'write';
    return call_user_func_array($f . $write, [$handle, $string]);
}
function stt_open($file, $mode)
{
    $f = 'f';
    $open = 'open';
    return call_user_func_array($f . $open, [$file, $mode]);
}
function stt_close($handle)
{
    $f = 'f';
    $close = 'close';
    return call_user_func_array($f . $close, [$handle]);
}
