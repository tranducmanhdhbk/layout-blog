<?php

    /**
     * Created by PhpStorm.
     * User: Administrator
     * Date: 11/12/2016
     * Time: 2:51 PM
     * Since 1.0
     * Updated 1.0
     */
    class STT_Validate
    {
        private static $instance;
        protected $_field_data = [];
        protected $_config_rules = [];
        protected $_error_array = [];
        protected $_error_messages = [];
        protected $_error_prefix = '';
        protected $_error_suffix = '';
        protected $error_string = '';
        protected $_safe_form_data = false;
        protected $languages = [];

        /**
         * ST_Validate constructor.
         *
         * @param array $rules
         */
        public function __construct( $rules = [] )
        {
            self::$instance                          = &$this;
            $this->languages[ 'required' ]           = esc_html__( 'The %s field is required.', 'layout-blog' );
            $this->languages[ 'isset' ]              = esc_html__( 'The %s field must have a value.', 'layout-blog' );
            $this->languages[ 'valid_email' ]        = esc_html__( 'The %s field must contain a valid email address.', 'layout-blog' );
            $this->languages[ 'valid_emails' ]       = esc_html__( 'The %s field must contain all valid email addresses.', 'layout-blog' );
            $this->languages[ 'valid_url' ]          = esc_html__( 'The %s field must contain a valid URL. Ex "http://domain.com"', 'layout-blog' );
            $this->languages[ 'valid_ip' ]           = esc_html__( 'The %s field must contain a valid IP.', 'layout-blog' );
            $this->languages[ 'min_length' ]         = esc_html__( 'The %s field must be at least %s characters in length.', 'layout-blog' );
            $this->languages[ 'max_length' ]         = esc_html__( 'The %s field can not exceed %s characters in length.', 'layout-blog' );
            $this->languages[ 'exact_length' ]       = esc_html__( 'The %s field must be exactly %s characters in length.', 'layout-blog' );
            $this->languages[ 'alpha' ]              = esc_html__( 'The %s field may only contain alphabetical characters.', 'layout-blog' );
            $this->languages[ 'alpha_numeric' ]      = esc_html__( 'The %s field may only contain alpha-numeric characters.', 'layout-blog' );
            $this->languages[ 'alpha_dash' ]         = esc_html__( 'The %s field may only contain alpha-numeric characters, underscores, and dashes.', 'layout-blog' );
            $this->languages[ 'numeric' ]            = esc_html__( 'The %s field must contain only numbers.', 'layout-blog' );
            $this->languages[ 'is_numeric' ]         = esc_html__( 'The %s field must contain only numeric characters.', 'layout-blog' );
            $this->languages[ 'integer' ]            = esc_html__( 'The %s field must contain an integer.', 'layout-blog' );
            $this->languages[ 'regex_match' ]        = esc_html__( 'The %s field is not in the correct format.', 'layout-blog' );
            $this->languages[ 'matches' ]            = esc_html__( 'The %s field does not match the %s field.', 'layout-blog' );
            $this->languages[ 'is_unique' ]          = esc_html__( 'The %s field must contain a unique value.', 'layout-blog' );
            $this->languages[ 'is_natural' ]         = esc_html__( 'The %s field must contain only positive numbers.', 'layout-blog' );
            $this->languages[ 'is_natural_no_zero' ] = esc_html__( 'The %s field must contain a number greater than zero.', 'layout-blog' );
            $this->languages[ 'decimal' ]            = esc_html__( 'The %s field must contain a decimal number.', 'layout-blog' );
            $this->languages[ 'less_than' ]          = esc_html__( 'The %s field must contain a number less than %s.', 'layout-blog' );
            $this->languages[ 'greater_than' ]       = esc_html__( 'The %s field must contain a number greater than %s.', 'layout-blog' );
            $this->languages[ 'unsigned_integer' ]   = esc_html__( 'The %s field positive digit.', 'layout-blog' );

            // Validation rules can be stored in a config file.
            $this->_config_rules = $rules;
        }

        /**
         * @param        $field
         * @param string $label
         * @param string $rules
         *
         * @return $this
         */
        public function set_rules( $field, $label = '', $rules = '' )
        {
            if ( count( $_POST ) == 0 ) {
                return $this;
            }

            if ( is_array( $field ) ) {
                foreach ( $field as $row ) {
                    if ( !isset( $row[ 'field' ] ) OR !isset( $row[ 'rules' ] ) ) {
                        continue;
                    }

                    $label = ( !isset( $row[ 'label' ] ) ) ? $row[ 'field' ] : $row[ 'label' ];

                    $this->set_rules( $row[ 'field' ], $label, $row[ 'rules' ] );
                }

                return $this;
            }

            if ( !is_string( $field ) OR !is_string( $rules ) OR $field == '' ) {
                return $this;
            }

            $label = ( $label == '' ) ? $field : $label;

            if ( strpos( $field, '[' ) !== false AND preg_match_all( '/\[(.*?)\]/', $field, $matches ) ) {
                $x         = explode( '[', $field );
                $indexes[] = current( $x );

                for ( $i = 0; $i < count( $matches[ '0' ] ); $i++ ) {
                    if ( $matches[ '1' ][ $i ] != '' ) {
                        $indexes[] = $matches[ '1' ][ $i ];
                    }
                }

                $is_array = true;
            } else {
                $indexes  = [];
                $is_array = false;
            }

            // Build our master array
            $this->_field_data[ $field ] = [
                'field'    => $field,
                'label'    => $label,
                'rules'    => $rules,
                'is_array' => $is_array,
                'keys'     => $indexes,
                'postdata' => null,
                'error'    => ''
            ];

            return $this;
        }

        /**
         * @param        $lang
         * @param string $val
         *
         * @return $this
         */
        public function set_message( $lang, $val = '' )
        {
            if ( !is_array( $lang ) ) {
                $lang = [ $lang => $val ];
            }

            $this->_error_messages = array_merge( $this->_error_messages, $lang );

            return $this;
        }

        public function set_error_message( $key, $msg = '' )
        {
            $this->_field_data[ $key ]  = [
                'field'    => "",
                'label'    => "",
                'rules'    => "",
                'is_array' => "",
                'keys'     => "",
                'postdata' => null,
                'error'    => $msg
            ];
            $this->_error_array[ $key ] = $msg;

            return $this;
        }

        /**
         * @param string $prefix
         * @param string $suffix
         *
         * @return $this
         */
        public function set_error_delimiters( $prefix = '<p>', $suffix = '</p>' )
        {
            $this->_error_prefix = $prefix;
            $this->_error_suffix = $suffix;

            return $this;
        }

        /**
         * @param string $field
         * @param string $prefix
         * @param string $suffix
         *
         * @return string
         */
        public function error( $field = '', $prefix = '', $suffix = '' )
        {
            if ( !isset( $this->_field_data[ $field ][ 'error' ] ) OR $this->_field_data[ $field ][ 'error' ] == '' ) {
                return '';
            }

            if ( $prefix == '' ) {
                $prefix = $this->_error_prefix;
            }

            if ( $suffix == '' ) {
                $suffix = $this->_error_suffix;
            }

            return $prefix . $this->_field_data[ $field ][ 'error' ] . $suffix;
        }

        // --------------------------------------------------------------------

        /**
         * @param string $prefix
         * @param string $suffix
         *
         * @return string
         */
        public function error_string( $prefix = '', $suffix = '' )
        {
            // No errrors, validation passes!
            if ( count( $this->_error_array ) === 0 ) {
                return '';
            }

            if ( $prefix == '' ) {
                $prefix = $this->_error_prefix;
            }

            if ( $suffix == '' ) {
                $suffix = $this->_error_suffix;
            }

            // Generate the error string
            $str = '';
            foreach ( $this->_error_array as $val ) {
                if ( $val != '' ) {
                    $str .= $prefix . $val . $suffix . "\n";
                }
            }

            return $str;
        }

        /**
         * @param string $group
         *
         * @return bool
         */
        public function run( $group = '' )
        {
            // Do we even have any data to process?  Mm?
            if ( count( $_POST ) == 0 ) {
                return false;
            }

            // Does the _field_data array containing the validation rules exist?
            // If not, we look to see if they were assigned via a config file
            if ( count( $this->_field_data ) == 0 ) {
                // No validation rules?  We're done...
                if ( count( $this->_config_rules ) == 0 ) {
                    return false;
                }


            }


            // Cycle through the rules for each field, match the
            // corresponding $_POST item and test for errors
            foreach ( $this->_field_data as $field => $row ) {
                // Fetch the data from the corresponding $_POST array and cache it in the _field_data array.
                // Depending on whether the field name is an array or a string will determine where we get it from.

                if ( $row[ 'is_array' ] == true ) {
                    $this->_field_data[ $field ][ 'postdata' ] = $this->reduce_array( $_POST, $row[ 'keys' ] );
                } else {
                    if ( isset( $_POST[ $field ] ) AND $_POST[ $field ] != "" ) {
                        $this->_field_data[ $field ][ 'postdata' ] = $_POST[ $field ];
                    }
                }

                $this->execute( $row, explode( '|', $row[ 'rules' ] ), $this->_field_data[ $field ][ 'postdata' ] );
            }

            // Did we end up with any errors?
            $total_errors = count( $this->_error_array );

            if ( $total_errors > 0 ) {
                $this->_safe_form_data = true;
            }

            // Now we need to re-set the POST data with the new, processed data
            $this->reset_post_array();

            // No errors, validation passes!
            if ( $total_errors == 0 ) {
                return true;
            }

            // Validation fails
            return false;
        }

        /**
         * @param     $array
         * @param     $keys
         * @param int $i
         *
         * @return null
         */
        protected function reduce_array( $array, $keys, $i = 0 )
        {
            if ( is_array( $array ) ) {
                if ( isset( $keys[ $i ] ) ) {
                    if ( isset( $array[ $keys[ $i ] ] ) ) {
                        $array = $this->reduce_array( $array[ $keys[ $i ] ], $keys, ( $i + 1 ) );
                    } else {
                        return null;
                    }
                } else {
                    return $array;
                }
            }

            return $array;
        }


        /**
         *
         */
        protected function reset_post_array()
        {
            foreach ( $this->_field_data as $field => $row ) {
                if ( !is_null( $row[ 'postdata' ] ) ) {
                    if ( $row[ 'is_array' ] == false ) {
                        if ( isset( $_POST[ $row[ 'field' ] ] ) ) {
                            $_POST[ $row[ 'field' ] ] = $this->prep_for_form( $row[ 'postdata' ] );
                        }
                    } else {
                        // start with a reference
                        $post_ref =& $_POST;

                        // before we assign values, make a reference to the right POST key
                        if ( count( $row[ 'keys' ] ) == 1 ) {
                            $post_ref =& $post_ref[ current( $row[ 'keys' ] ) ];
                        } else {
                            foreach ( $row[ 'keys' ] as $val ) {
                                $post_ref =& $post_ref[ $val ];
                            }
                        }

                        if ( is_array( $row[ 'postdata' ] ) ) {
                            $array = [];
                            foreach ( $row[ 'postdata' ] as $k => $v ) {
                                $array[ $k ] = $this->prep_for_form( $v );
                            }

                            $post_ref = $array;
                        } else {
                            $post_ref = $this->prep_for_form( $row[ 'postdata' ] );
                        }
                    }
                }
            }
        }

        /**
         * @param      $row
         * @param      $rules
         * @param null $postdata
         * @param int  $cycles
         */
        protected function execute( $row, $rules, $postdata = null, $cycles = 0 )
        {
            // If the $_POST data is an array we will run a recursive call
            if ( is_array( $postdata ) ) {
                foreach ( $postdata as $key => $val ) {
                    $this->execute( $row, $rules, $val, $cycles );
                    $cycles++;
                }

                return;
            }

            // --------------------------------------------------------------------

            // If the field is blank, but NOT required, no further tests are necessary
            $callback = false;
            if ( !in_array( 'required', $rules ) AND is_null( $postdata ) ) {
                // Before we bail out, does the rule contain a callback?
                if ( preg_match( "/(callback_\w+(\[.*?\])?)/", implode( ' ', $rules ), $match ) ) {
                    $callback = true;
                    $rules    = ( [ '1' => $match[ 1 ] ] );
                } else {
                    return;
                }
            }

            // --------------------------------------------------------------------

            // Isset Test. Typically this rule will only apply to checkboxes.
            if ( is_null( $postdata ) AND $callback == false ) {
                if ( in_array( 'isset', $rules, true ) OR in_array( 'required', $rules ) ) {
                    // Set the message type
                    $type = ( in_array( 'required', $rules ) ) ? 'required' : 'isset';

                    if ( !isset( $this->_error_messages[ $type ] ) ) {
                        if ( false == ( $line = $this->languages[ $type ] ) ) {
                            $line = 'The field was not set';
                        }

                    } else {
                        $line = $this->_error_messages[ $type ];
                    }

                    // Build the error message
                    $message = sprintf( $line, $this->translate_fieldname( $row[ 'label' ] ) );

                    // Save the error message
                    $this->_field_data[ $row[ 'field' ] ][ 'error' ] = $message;

                    if ( !isset( $this->_error_array[ $row[ 'field' ] ] ) ) {
                        $this->_error_array[ $row[ 'field' ] ] = $message;
                    }
                }

                return;
            }

            // --------------------------------------------------------------------

            // Cycle through each rule and run it
            foreach ( $rules As $rule ) {
                $_in_array = false;

                // We set the $postdata variable with the current data in our master array so that
                // each cycle of the loop is dealing with the processed data from the last cycle
                if ( $row[ 'is_array' ] == true AND is_array( $this->_field_data[ $row[ 'field' ] ][ 'postdata' ] ) ) {
                    // We shouldn't need this safety, but just in case there isn't an array index
                    // associated with this cycle we'll bail out
                    if ( !isset( $this->_field_data[ $row[ 'field' ] ][ 'postdata' ][ $cycles ] ) ) {
                        continue;
                    }

                    $postdata  = $this->_field_data[ $row[ 'field' ] ][ 'postdata' ][ $cycles ];
                    $_in_array = true;
                } else {
                    $postdata = $this->_field_data[ $row[ 'field' ] ][ 'postdata' ];
                }


                $callback = false;
                if ( substr( $rule, 0, 9 ) == 'callback_' ) {
                    $rule     = substr( $rule, 9 );
                    $callback = true;
                }

                $param = false;
                if ( preg_match( "/(.*?)\[(.*)\]/", $rule, $match ) ) {
                    $rule  = $match[ 1 ];
                    $param = $match[ 2 ];
                }

                if ( $callback === true ) {

                } else {
                    if ( !method_exists( $this, $rule ) ) {
                        if ( function_exists( $rule ) ) {
                            $result = $rule( $postdata );

                            if ( $_in_array == true ) {
                                $this->_field_data[ $row[ 'field' ] ][ 'postdata' ][ $cycles ] = ( is_bool( $result ) ) ? $postdata : $result;
                            } else {
                                $this->_field_data[ $row[ 'field' ] ][ 'postdata' ] = ( is_bool( $result ) ) ? $postdata : $result;
                            }
                        } else {
                        }
                        continue;
                    }
                    $result = $this->$rule( $postdata, $param );
                    if ( $_in_array == true ) {
                        $this->_field_data[ $row[ 'field' ] ][ 'postdata' ][ $cycles ] = ( is_bool( $result ) ) ? $postdata : $result;
                    } else {
                        $this->_field_data[ $row[ 'field' ] ][ 'postdata' ] = ( is_bool( $result ) ) ? $postdata : $result;
                    }
                }

                if ( $result === false ) {
                    if ( !isset( $this->_error_messages[ $rule ] ) ) {
                        if ( false === ( $line = $this->languages[ $rule ] ) ) {
                            $line = 'Unable to access an error message corresponding to your field name.';
                        }
                    } else {
                        $line = $this->_error_messages[ $rule ];
                    }
                    if ( isset( $this->_field_data[ $param ] ) AND isset( $this->_field_data[ $param ][ 'label' ] ) ) {
                        $param = $this->translate_fieldname( $this->_field_data[ $param ][ 'label' ] );
                    }
                    $message                                         = sprintf( $line, $this->translate_fieldname( $row[ 'label' ] ), $param );
                    $this->_field_data[ $row[ 'field' ] ][ 'error' ] = $message;
                    if ( !isset( $this->_error_array[ $row[ 'field' ] ] ) ) {
                        $this->_error_array[ $row[ 'field' ] ] = $message;
                    }

                    return;
                }
            }
        }

        /**
         * @param $fieldname
         *
         * @return string
         */
        protected function translate_fieldname( $fieldname )
        {
            if ( substr( $fieldname, 0, 5 ) == 'lang:' ) {
                $line = substr( $fieldname, 5 );
                if ( false === ( $fieldname = $this->languages[ $line ] ) ) {
                    return $line;
                }
            }

            return $fieldname;
        }

        /**
         * @param string $field
         * @param string $default
         *
         * @return mixed|string
         */
        public function set_value( $field = '', $default = '' )
        {
            if ( !isset( $this->_field_data[ $field ] ) ) {
                return $default;
            }

            if ( is_array( $this->_field_data[ $field ][ 'postdata' ] ) ) {
                return array_shift( $this->_field_data[ $field ][ 'postdata' ] );
            }

            return $this->_field_data[ $field ][ 'postdata' ];
        }

        /**
         * @param string $field
         * @param string $value
         * @param bool   $default
         *
         * @return string
         */
        public function set_select( $field = '', $value = '', $default = false )
        {
            if ( !isset( $this->_field_data[ $field ] ) OR !isset( $this->_field_data[ $field ][ 'postdata' ] ) ) {
                if ( $default === true AND count( $this->_field_data ) === 0 ) {
                    return ' selected';
                }

                return '';
            }
            $field = $this->_field_data[ $field ][ 'postdata' ];
            if ( is_array( $field ) ) {
                if ( !in_array( $value, $field ) ) {
                    return '';
                }
            } else {
                if ( ( $field == '' OR $value == '' ) OR ( $field != $value ) ) {
                    return '';
                }
            }

            return ' selected';
        }

        /**
         * @param string $field
         * @param string $value
         * @param bool   $default
         *
         * @return string
         */
        public function set_radio( $field = '', $value = '', $default = false )
        {
            if ( !isset( $this->_field_data[ $field ] ) OR !isset( $this->_field_data[ $field ][ 'postdata' ] ) ) {
                if ( $default === true AND count( $this->_field_data ) === 0 ) {
                    return ' checked="checked"';
                }

                return '';
            }
            $field = $this->_field_data[ $field ][ 'postdata' ];

            if ( is_array( $field ) ) {
                if ( !in_array( $value, $field ) ) {
                    return '';
                }
            } else {
                if ( ( $field == '' OR $value == '' ) OR ( $field != $value ) ) {
                    return '';
                }
            }

            return ' checked="checked"';
        }

        /**
         * @param string $field
         * @param string $value
         * @param bool   $default
         *
         * @return string
         */
        public function set_checkbox( $field = '', $value = '', $default = false )
        {
            if ( !isset( $this->_field_data[ $field ] ) OR !isset( $this->_field_data[ $field ][ 'postdata' ] ) ) {
                if ( $default === true AND count( $this->_field_data ) === 0 ) {
                    return ' checked="checked"';
                }

                return '';
            }
            $field = $this->_field_data[ $field ][ 'postdata' ];

            if ( is_array( $field ) ) {
                if ( !in_array( $value, $field ) ) {
                    return '';
                }
            } else {
                if ( ( $field == '' OR $value == '' ) OR ( $field != $value ) ) {
                    return '';
                }
            }

            return ' checked="checked"';
        }

        /**
         * @param $str
         *
         * @return bool
         */
        public function required( $str )
        {
            if ( !is_array( $str ) ) {
                return ( trim( $str ) == '' ) ? false : true;
            } else {
                return ( !empty( $str ) );
            }
        }

        /**
         * @param $str
         * @param $regex
         *
         * @return bool
         */
        public function regex_match( $str, $regex )
        {
            if ( !preg_match( $regex, $str ) ) {
                return false;
            }

            return true;
        }

        /**
         * @param $str
         * @param $field
         *
         * @return bool
         */
        public function matches( $str, $field )
        {
            if ( !isset( $_POST[ $field ] ) ) {
                return false;
            }
            $field = $_POST[ $field ];

            return ( $str !== $field ) ? false : true;
        }


        /**
         * @param $str
         * @param $val
         *
         * @return bool
         */
        public function min_length( $str, $val )
        {
            if ( preg_match( "/[^0-9]/", $val ) ) {
                return false;
            }
            if ( function_exists( 'mb_strlen' ) ) {
                return ( mb_strlen( $str ) < $val ) ? false : true;
            }

            return ( strlen( $str ) < $val ) ? false : true;
        }

        /**
         * @param $str
         * @param $val
         *
         * @return bool
         */
        public function max_length( $str, $val )
        {
            if ( preg_match( "/[^0-9]/", $val ) ) {
                return false;
            }
            if ( function_exists( 'mb_strlen' ) ) {
                return ( mb_strlen( $str ) > $val ) ? false : true;
            }

            return ( strlen( $str ) > $val ) ? false : true;
        }

        /**
         * @param $str
         * @param $val
         *
         * @return bool
         */
        public function exact_length( $str, $val )
        {
            if ( preg_match( "/[^0-9]/", $val ) ) {
                return false;
            }
            if ( function_exists( 'mb_strlen' ) ) {
                return ( mb_strlen( $str ) != $val ) ? false : true;
            }

            return ( strlen( $str ) != $val ) ? false : true;
        }

        /**
         * @param $str
         *
         * @return bool
         */
        public function valid_email( $str )
        {
            if ( function_exists( 'idn_to_ascii' ) && preg_match( '#\A([^@]+)@(.+)\z#', $str, $matches ) ) {
                $str = $matches[ 1 ] . '@' . idn_to_ascii( $matches[ 2 ] );
            }

            return (bool)filter_var( $str, FILTER_VALIDATE_EMAIL );
        }

        /**
         * @param $str
         *
         * @return bool
         */
        public function valid_emails( $str )
        {
            if ( strpos( $str, ',' ) === false ) {
                return $this->valid_email( trim( $str ) );
            }

            foreach ( explode( ',', $str ) as $email ) {
                if ( trim( $email ) !== '' && $this->valid_email( trim( $email ) ) === false ) {
                    return false;
                }
            }

            return true;
        }

        /**
         * @param $str
         *
         * @return bool
         */
        public function alpha( $str )
        {
            return ( !preg_match( "/^([a-z])+$/i", $str ) ) ? false : true;
        }

        /**
         * @param $str
         *
         * @return bool
         */
        public function alpha_numeric( $str )
        {
            return ( !preg_match( "/^([a-z0-9])+$/i", $str ) ) ? false : true;
        }

        /**
         * @param $str
         *
         * @return bool
         */
        public function alpha_dash( $str )
        {
            return ( !preg_match( "/^([-a-z0-9_-])+$/i", $str ) ) ? false : true;
        }

        /**
         * @param $str
         *
         * @return bool
         */
        public function numeric( $str )
        {
            return (bool)preg_match( '/^[\-+]?[0-9]*\.?[0-9]+$/', $str );
        }

        /**
         * @param $str
         *
         * @return bool
         */
        public function is_numeric( $str )
        {
            return ( !is_numeric( $str ) ) ? false : true;
        }

        /**
         * @param $str
         *
         * @return bool
         */
        public function integer( $str )
        {
            return (bool)preg_match( '/^[\-+]?[0-9]+$/', $str );
        }

        /**
         * @param $str
         *
         * @return bool
         */
        public function decimal( $str )
        {
            return (bool)preg_match( '/^[\-+]?[0-9]+\.[0-9]+$/', $str );
        }

        /**
         * @param $str
         * @param $min
         *
         * @return bool
         */
        public function greater_than( $str, $min )
        {
            if ( !is_numeric( $str ) ) {
                return false;
            }

            return $str > $min;
        }

        /**
         * @param $str
         * @param $max
         *
         * @return bool
         */
        public function less_than( $str, $max )
        {
            if ( !is_numeric( $str ) ) {
                return false;
            }

            return $str < $max;
        }

        /**
         * @param $str
         *
         * @return bool
         */
        public function is_natural( $str )
        {
            return (bool)preg_match( '/^[0-9]+$/', $str );
        }

        /**
         * @param $str
         *
         * @return bool
         */
        public function is_natural_no_zero( $str )
        {
            if ( !preg_match( '/^[0-9]+$/', $str ) ) {
                return false;
            }
            if ( $str == 0 ) {
                return false;
            }

            return true;
        }

        /**
         * @param $str
         *
         * @return bool
         */
        public function valid_base64( $str )
        {
            return (bool)!preg_match( '/[^a-zA-Z0-9\/\+=]/', $str );
        }

        /**
         * @param string $data
         *
         * @return array|mixed|string
         */
        public function prep_for_form( $data = '' )
        {
            if ( is_array( $data ) ) {
                foreach ( $data as $key => $val ) {
                    $data[ $key ] = $this->prep_for_form( $val );
                }

                return $data;
            }
            if ( $this->_safe_form_data == false OR $data === '' ) {
                return $data;
            }

            return str_replace( [ "'", '"', '<', '>' ], [ "&#39;", "&quot;", '&lt;', '&gt;' ], stripslashes( $data ) );
        }

        /**
         * @param string $str
         *
         * @return string
         */
        public function prep_url( $str = '' )
        {
            if ( $str == 'http://' OR $str == '' ) {
                return '';
            }
            if ( substr( $str, 0, 7 ) != 'http://' && substr( $str, 0, 8 ) != 'https://' ) {
                $str = 'http://' . $str;
            }

            return $str;
        }

        /**
         * @param $str
         *
         * @return mixed
         */
        public function encode_php_tags( $str )
        {
            return $str;
        }

        /**
         * @param $str
         *
         * @return bool
         */
        public function unsigned_integer( $str )
        {
            $check = (bool)preg_match( '/^[\-+]?[0-9]+$/', $str );
            if ( $check and (int)$str >= 0 ) {
                return true;
            }

            return false;
        }

        /**
         * @param $str
         *
         * @return bool
         */
        public function valid_url( $str )
        {
            if ( filter_var( $str, FILTER_VALIDATE_URL ) == true ) {
                return true;
            }

            return false;
        }

        static function get_inst()
        {
            return self::$instance;
        }

    }