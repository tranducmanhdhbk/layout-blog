<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 11/12/2016
 * Time: 2:51 PM
 * Since 1.0.0
 * Updated 1.0.0
 */

if (!class_exists('STT_Assets')) {
    /**
     * Class ST_Assets
     */
    class STT_Assets
    {
        /**
         * @var ST_Assets
         */
        private static $instance;
        /**
         * @var string
         */
        public $asset_url;

        /**
         * @var
         */
        public $inline_css;
        /**
         * @var string
         */
        public $current_css_id;
        /**
         * @var string
         */
        public $prefix_class = "st_";

        /**
         * ST_Assets constructor.
         */
        public function __construct()
        {
            self::$instance = &$this;
            $this->current_css_id = time() . rand('9', '999');
            add_filter('stt_custom_css_content', array($this, 'stt_custom_css_content'), 10);
            $this->asset_url = get_template_directory_uri() . '/css';
        }

        /**
         * @param bool $file
         *
         * @return string
         */
        public function url($file = false)
        {
            return $this->asset_url . '/' . $file;
        }

        /**
         * @param bool $string
         * @param bool $effect
         *
         * @return string
         */
        public function build_css($string = false, $effect = false)
        {
            if (!empty($string)) {
                $this->current_css_id++;
                $this->inline_css .= "." . $this->prefix_class . $this->current_css_id . $effect . "{ $string }";
                return $this->prefix_class . $this->current_css_id;
            }
        }

        /**
         * @param bool $string
         */
        public function add_css($string = false)
        {
            $this->inline_css = $string . $this->inline_css;

        }

        /**
         * @param $contents
         *
         * @return string
         */
        public function stt_custom_css_content($contents)
        {
            if (!empty($this->inline_css)) {
                $contents .= $this->inline_css;
            }
            return $contents;
        }

        /**
         * @return ST_Assets
         */
        static function get_inst()
        {
            return self::$instance;
        }
    }

    new STT_Assets();
}
