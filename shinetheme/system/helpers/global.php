<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 11/12/2016
 * Time: 2:51 PM
 * Since 1.0
 * Updated 1.0
 */

function stt_get_meta($post_id, $meta_key, $default = '')
{
    $meta_value = get_post_meta($post_id, $meta_key, true);
    if ($meta_value) {
        return $meta_value;
    }

    return $default;
}

function stt_get_term_meta($term_id, $meta_key, $default = '')
{
    $meta_value = get_term_meta($term_id, $meta_key, true);
    if ($meta_value) {
        return $meta_value;
    }

    return $default;
}

function st_get_user_meta($user_id, $meta_key, $default = '')
{
    $meta_value = get_user_meta($user_id, $meta_key, true);
    if ($meta_value) {
        return $meta_value;
    }

    return $default;
}

function stt_get_alt_image($image_id = null)
{
    $alt = stt_get_meta($image_id, '_wp_attachment_image_alt', '');
    if (!$alt) {
        $alt = get_bloginfo('description');
    }

    return esc_attr($alt);
}

function stt_get_vc_pagecontent($page_id)
{
    if ($page_id) {
        $page = get_post($page_id);
        if ($page) {
            //$content = apply_filters('the_content', $page->post_content);
            $content = $page->post_content;

            $content = str_replace(']]>', ']]&gt;', $content);


            $shortcodes_custom_css = get_post_meta($page_id, '_wpb_shortcodes_custom_css', true);
            ST_Assets::get_inst()->add_css($shortcodes_custom_css);

            wp_reset_postdata();
            wp_reset_query();

            return $content;
        }
    }
}

function stt_get_pagecontent($page_id = false)
{
    if ($page_id) {
        $page = get_post($page_id);
        if ($page) {
            $content = '';
            global $kc;
            if (isset($kc)) {
                if (isset($page->post_content_filtered) && !empty($page->post_content_filtered)) {
                    $content = kc_do_shortcode($page->post_content_filtered);
                } else {
                    $content = kc_do_shortcode($page->post_content);
                }
            } else {
                echo do_shortcode($page->post_content);
            }

            return $content;
        }
    }

    return false;
}

function st_encode_attr($data)
{
    return htmlspecialchars(json_encode($data), ENT_QUOTES, 'UTF-8');
}


function stt_encrypt($string)
{
    $key_encrypt = st_config_item('key_encrypt');

    return md5(md5($key_encrypt) . md5($string));
}

function stt_compare_encrypt($string = '', $encrypt = '')
{
    $key_encrypt = st_config_item('key_encrypt');
    $string = md5(md5($key_encrypt) . md5($string));
    if (!empty($string) && !empty($encrypt) && $string === $encrypt) {
        return true;
    }

    return false;
}

function stt_get_post_lang($post_id)
{
    $lang = '';
    if (stt_is_wpml()) {
        global $wpdb;

        $query = $wpdb->prepare('SELECT language_code FROM ' . $wpdb->prefix . 'icl_translations WHERE element_id="%d"', $post_id);
        $query_exec = $wpdb->get_row($query);

        $lang = $query_exec->language_code;
    }

    return $lang;
}

function stt_default_lang()
{
    $lang = '';
    if (stt_is_wpml()) {
        global $sitepress;
        $lang = $sitepress->get_default_language();
    }

    return $lang;
}

function stt_current_lang()
{
    $lang = '';
    if (stt_is_wpml()) {
        $lang = ICL_LANGUAGE_CODE;
    }

    return $lang;
}


function stt_post_origin($post_id, $post_type = 'post')
{
    if (stt_is_wpml()) {
        global $sitepress;

        return apply_filters('wpml_object_id', $post_id, $post_type, true, $sitepress->get_default_language());
    } else {
        return $post_id;
    }
}

function stt_post_translated($post_id, $post_type = 'post', $lang = '')
{
    if (stt_is_wpml() && has_filter('wpml_object_id')) {
        if (empty($lang)) {
            $lang = stt_current_lang();
        }

        return wpml_object_id_filter($post_id, $post_type, true, $lang);
    } else {
        return $post_id;
    }
}

function stt_is_translate_post($post_id, $post_type = 'post')
{
    if (empty($post_id)) {
        return true;
    }
    if (stt_is_wpml()) {
        $post_origin = (int)stt_post_origin($post_id, $post_type);
        if ($post_id == $post_origin) {
            return false;
        } else {
            return true;
        }
    }

    return false;
}

function stt_is_wpml()
{
    if (defined('ICL_LANGUAGE_CODE') && defined('ICL_SITEPRESS_VERSION')) {
        return true;
    }

    return false;
}

function stt_get_all_langs($not_current = false, $return_all = false)
{
    $langs = [];
    if (stt_is_wpml()) {
        $languages = apply_filters('wpml_active_languages', null, ['skip_missing' => 0, 'orderby' => 'custom']);
        if (is_array($languages) && !empty($languages)) {
            foreach ($languages as $lang) {
                if ($not_current && $lang['language_code'] == stt_current_lang()) {
                    continue;
                } else {
                    if ($return_all) {
                        $langs[] = $lang;
                    } else {
                        $langs[] = $lang['language_code'];
                    }
                }
            }
        }
    }

    return $langs;
}

function stt_current_url()
{
    return esc_url(wp_unslash(stt_server('REQUEST_URI')));
}

function stt_get_size_image($value = '', $default = 'thumbnail')
{
    $return = $default;
    if (strpos($value, 'x')) {
        $size_arr = explode('x', $value);
        if (is_array($size_arr) and count($size_arr) == 2) {
            $return = $size_arr;
        }
    } else {
        if (!empty($value)) {
            $return = $value;
        }
    }

    return $return;
}

function stt_hex_to_rgb($hex)
{
    $hex = str_replace("#", "", $hex);

    if (strlen($hex) == 3) {
        $r = hexdec(substr($hex, 0, 1) . substr($hex, 0, 1));
        $g = hexdec(substr($hex, 1, 1) . substr($hex, 1, 1));
        $b = hexdec(substr($hex, 2, 1) . substr($hex, 2, 1));
    } else {
        $r = hexdec(substr($hex, 0, 2));
        $g = hexdec(substr($hex, 2, 2));
        $b = hexdec(substr($hex, 4, 2));
    }
    $rgb = [$r, $g, $b];

    return $rgb; // returns an array with the rgb values
}

function stt_start_get_view()
{
    ob_start();
}

function stt_end_get_view()
{
    return @ob_get_clean();
}

function stt_list_hours($step = 15)
{
    $return = [];
    $start = 0;
    $end = 12;
    $time = ['AM', 'PM'];
    foreach ($time as $subfix) {
        if ($subfix == 'PM') {
            $start = 1;
            $end = 11;
        }
        for ($i = $start; $i <= $end; $i++) {
            for ($j = 0; $j <= 45; $j += $step) {
                $time = sprintf('%02d:%02d %s', $i, $j, $subfix);
                $return[$time] = $time;
            }
        }
    }

    return $return;
}

function stt_list_countries($key = '')
{
    $countries = array(
        "AF" => "Afghanistan",
        "AL" => "Albania",
        "DZ" => "Algeria",
        "AS" => "American Samoa",
        "AD" => "Andorra",
        "AO" => "Angola",
        "AI" => "Anguilla",
        "AQ" => "Antarctica",
        "AG" => "Antigua and Barbuda",
        "AR" => "Argentina",
        "AM" => "Armenia",
        "AW" => "Aruba",
        "AU" => "Australia",
        "AT" => "Austria",
        "AZ" => "Azerbaijan",
        "BS" => "Bahamas",
        "BH" => "Bahrain",
        "BD" => "Bangladesh",
        "BB" => "Barbados",
        "BY" => "Belarus",
        "BE" => "Belgium",
        "BZ" => "Belize",
        "BJ" => "Benin",
        "BM" => "Bermuda",
        "BT" => "Bhutan",
        "BO" => "Bolivia",
        "BA" => "Bosnia and Herzegovina",
        "BW" => "Botswana",
        "BV" => "Bouvet Island",
        "BR" => "Brazil",
        "IO" => "British Indian Ocean Territory",
        "BN" => "Brunei Darussalam",
        "BG" => "Bulgaria",
        "BF" => "Burkina Faso",
        "BI" => "Burundi",
        "KH" => "Cambodia",
        "CM" => "Cameroon",
        "CA" => "Canada",
        "CV" => "Cape Verde",
        "KY" => "Cayman Islands",
        "CF" => "Central African Republic",
        "TD" => "Chad",
        "CL" => "Chile",
        "CN" => "China",
        "CX" => "Christmas Island",
        "CC" => "Cocos (Keeling) Islands",
        "CO" => "Colombia",
        "KM" => "Comoros",
        "CG" => "Congo",
        "CD" => "Congo, the Democratic Republic of the",
        "CK" => "Cook Islands",
        "CR" => "Costa Rica",
        "CI" => "Cote D'Ivoire",
        "HR" => "Croatia",
        "CU" => "Cuba",
        "CY" => "Cyprus",
        "CZ" => "Czech Republic",
        "DK" => "Denmark",
        "DJ" => "Djibouti",
        "DM" => "Dominica",
        "DO" => "Dominican Republic",
        "EC" => "Ecuador",
        "EG" => "Egypt",
        "SV" => "El Salvador",
        "GQ" => "Equatorial Guinea",
        "ER" => "Eritrea",
        "EE" => "Estonia",
        "ET" => "Ethiopia",
        "FK" => "Falkland Islands (Malvinas)",
        "FO" => "Faroe Islands",
        "FJ" => "Fiji",
        "FI" => "Finland",
        "FR" => "France",
        "GF" => "French Guiana",
        "PF" => "French Polynesia",
        "TF" => "French Southern Territories",
        "GA" => "Gabon",
        "GM" => "Gambia",
        "GE" => "Georgia",
        "DE" => "Germany",
        "GH" => "Ghana",
        "GI" => "Gibraltar",
        "GR" => "Greece",
        "GL" => "Greenland",
        "GD" => "Grenada",
        "GP" => "Guadeloupe",
        "GU" => "Guam",
        "GT" => "Guatemala",
        "GN" => "Guinea",
        "GW" => "Guinea-Bissau",
        "GY" => "Guyana",
        "HT" => "Haiti",
        "HM" => "Heard Island and Mcdonald Islands",
        "VA" => "Holy See (Vatican City State)",
        "HN" => "Honduras",
        "HK" => "Hong Kong",
        "HU" => "Hungary",
        "IS" => "Iceland",
        "IN" => "India",
        "ID" => "Indonesia",
        "IR" => "Iran, Islamic Republic of",
        "IQ" => "Iraq",
        "IE" => "Ireland",
        "IL" => "Israel",
        "IT" => "Italy",
        "JM" => "Jamaica",
        "JP" => "Japan",
        "JO" => "Jordan",
        "KZ" => "Kazakhstan",
        "KE" => "Kenya",
        "KI" => "Kiribati",
        "KP" => "Korea, Democratic People's Republic of",
        "KR" => "Korea, Republic of",
        "KW" => "Kuwait",
        "KG" => "Kyrgyzstan",
        "LA" => "Lao People's Democratic Republic",
        "LV" => "Latvia",
        "LB" => "Lebanon",
        "LS" => "Lesotho",
        "LR" => "Liberia",
        "LY" => "Libyan Arab Jamahiriya",
        "LI" => "Liechtenstein",
        "LT" => "Lithuania",
        "LU" => "Luxembourg",
        "MO" => "Macao",
        "MK" => "Macedonia, the Former Yugoslav Republic of",
        "MG" => "Madagascar",
        "MW" => "Malawi",
        "MY" => "Malaysia",
        "MV" => "Maldives",
        "ML" => "Mali",
        "MT" => "Malta",
        "MH" => "Marshall Islands",
        "MQ" => "Martinique",
        "MR" => "Mauritania",
        "MU" => "Mauritius",
        "YT" => "Mayotte",
        "MX" => "Mexico",
        "FM" => "Micronesia, Federated States of",
        "MD" => "Moldova, Republic of",
        "MC" => "Monaco",
        "MN" => "Mongolia",
        "MS" => "Montserrat",
        "MA" => "Morocco",
        "MZ" => "Mozambique",
        "MM" => "Myanmar",
        "NA" => "Namibia",
        "NR" => "Nauru",
        "NP" => "Nepal",
        "NL" => "Netherlands",
        "AN" => "Netherlands Antilles",
        "NC" => "New Caledonia",
        "NZ" => "New Zealand",
        "NI" => "Nicaragua",
        "NE" => "Niger",
        "NG" => "Nigeria",
        "NU" => "Niue",
        "NF" => "Norfolk Island",
        "MP" => "Northern Mariana Islands",
        "NO" => "Norway",
        "OM" => "Oman",
        "PK" => "Pakistan",
        "PW" => "Palau",
        "PS" => "Palestinian Territory, Occupied",
        "PA" => "Panama",
        "PG" => "Papua New Guinea",
        "PY" => "Paraguay",
        "PE" => "Peru",
        "PH" => "Philippines",
        "PN" => "Pitcairn",
        "PL" => "Poland",
        "PT" => "Portugal",
        "PR" => "Puerto Rico",
        "QA" => "Qatar",
        "RE" => "Reunion",
        "RO" => "Romania",
        "RU" => "Russian Federation",
        "RW" => "Rwanda",
        "SH" => "Saint Helena",
        "KN" => "Saint Kitts and Nevis",
        "LC" => "Saint Lucia",
        "PM" => "Saint Pierre and Miquelon",
        "VC" => "Saint Vincent and the Grenadines",
        "WS" => "Samoa",
        "SM" => "San Marino",
        "ST" => "Sao Tome and Principe",
        "SA" => "Saudi Arabia",
        "SN" => "Senegal",
        "CS" => "Serbia and Montenegro",
        "SC" => "Seychelles",
        "SL" => "Sierra Leone",
        "SG" => "Singapore",
        "SK" => "Slovakia",
        "SI" => "Slovenia",
        "SB" => "Solomon Islands",
        "SO" => "Somalia",
        "ZA" => "South Africa",
        "GS" => "South Georgia and the South Sandwich Islands",
        "ES" => "Spain",
        "LK" => "Sri Lanka",
        "SD" => "Sudan",
        "SR" => "Suriname",
        "SJ" => "Svalbard and Jan Mayen",
        "SZ" => "Swaziland",
        "SE" => "Sweden",
        "CH" => "Switzerland",
        "SY" => "Syrian Arab Republic",
        "TW" => "Taiwan, Province of China",
        "TJ" => "Tajikistan",
        "TZ" => "Tanzania, United Republic of",
        "TH" => "Thailand",
        "TL" => "Timor-Leste",
        "TG" => "Togo",
        "TK" => "Tokelau",
        "TO" => "Tonga",
        "TT" => "Trinidad and Tobago",
        "TN" => "Tunisia",
        "TR" => "Turkey",
        "TM" => "Turkmenistan",
        "TC" => "Turks and Caicos Islands",
        "TV" => "Tuvalu",
        "UG" => "Uganda",
        "UA" => "Ukraine",
        "AE" => "United Arab Emirates",
        "GB" => "United Kingdom",
        "US" => "United States",
        "UM" => "United States Minor Outlying Islands",
        "UY" => "Uruguay",
        "UZ" => "Uzbekistan",
        "VU" => "Vanuatu",
        "VE" => "Venezuela",
        "VN" => "Viet Nam",
        "VG" => "Virgin Islands, British",
        "VI" => "Virgin Islands, U.s.",
        "WF" => "Wallis and Futuna",
        "EH" => "Western Sahara",
        "YE" => "Yemen",
        "ZM" => "Zambia",
        "ZW" => "Zimbabwe"
    );
    if ($key) {
        return (isset($countries[$key])) ? $countries[$key] : '';
    }
    return $countries;
}

function stt_list_default_currencies()
{
    $currencies = array(
        'ALL' => 'Albania Lek',
        'AFN' => 'Afghanistan Afghani',
        'ARS' => 'Argentina Peso',
        'AWG' => 'Aruba Guilder',
        'AUD' => 'Australia Dollar',
        'AZN' => 'Azerbaijan New Manat',
        'BSD' => 'Bahamas Dollar',
        'BBD' => 'Barbados Dollar',
        'BDT' => 'Bangladeshi taka',
        'BYR' => 'Belarus Ruble',
        'BZD' => 'Belize Dollar',
        'BMD' => 'Bermuda Dollar',
        'BOB' => 'Bolivia Boliviano',
        'BAM' => 'Bosnia and Herzegovina Convertible Marka',
        'BWP' => 'Botswana Pula',
        'BGN' => 'Bulgaria Lev',
        'BRL' => 'Brazil Real',
        'BND' => 'Brunei Darussalam Dollar',
        'KHR' => 'Cambodia Riel',
        'CAD' => 'Canada Dollar',
        'KYD' => 'Cayman Islands Dollar',
        'CLP' => 'Chile Peso',
        'CNY' => 'China Yuan Renminbi',
        'COP' => 'Colombia Peso',
        'CRC' => 'Costa Rica Colon',
        'HRK' => 'Croatia Kuna',
        'CUP' => 'Cuba Peso',
        'CZK' => 'Czech Republic Koruna',
        'DKK' => 'Denmark Krone',
        'DOP' => 'Dominican Republic Peso',
        'XCD' => 'East Caribbean Dollar',
        'EGP' => 'Egypt Pound',
        'SVC' => 'El Salvador Colon',
        'EEK' => 'Estonia Kroon',
        'EUR' => 'Euro Member Countries',
        'FKP' => 'Falkland Islands (Malvinas) Pound',
        'FJD' => 'Fiji Dollar',
        'GHC' => 'Ghana Cedis',
        'GIP' => 'Gibraltar Pound',
        'GTQ' => 'Guatemala Quetzal',
        'GGP' => 'Guernsey Pound',
        'GYD' => 'Guyana Dollar',
        'HNL' => 'Honduras Lempira',
        'HKD' => 'Hong Kong Dollar',
        'HUF' => 'Hungary Forint',
        'ISK' => 'Iceland Krona',
        'INR' => 'India Rupee',
        'IDR' => 'Indonesia Rupiah',
        'IRR' => 'Iran Rial',
        'IMP' => 'Isle of Man Pound',
        'ILS' => 'Israel Shekel',
        'JMD' => 'Jamaica Dollar',
        'JPY' => 'Japan Yen',
        'JEP' => 'Jersey Pound',
        'KZT' => 'Kazakhstan Tenge',
        'KPW' => 'Korea (North) Won',
        'KRW' => 'Korea (South) Won',
        'KGS' => 'Kyrgyzstan Som',
        'LAK' => 'Laos Kip',
        'LVL' => 'Latvia Lat',
        'LBP' => 'Lebanon Pound',
        'LRD' => 'Liberia Dollar',
        'LTL' => 'Lithuania Litas',
        'MKD' => 'Macedonia Denar',
        'MYR' => 'Malaysia Ringgit',
        'MUR' => 'Mauritius Rupee',
        'MXN' => 'Mexico Peso',
        'MNT' => 'Mongolia Tughrik',
        'MZN' => 'Mozambique Metical',
        'NAD' => 'Namibia Dollar',
        'NPR' => 'Nepal Rupee',
        'ANG' => 'Netherlands Antilles Guilder',
        'NZD' => 'New Zealand Dollar',
        'NIO' => 'Nicaragua Cordoba',
        'NGN' => 'Nigeria Naira',
        'NOK' => 'Norway Krone',
        'OMR' => 'Oman Rial',
        'PKR' => 'Pakistan Rupee',
        'PAB' => 'Panama Balboa',
        'PYG' => 'Paraguay Guarani',
        'PEN' => 'Peru Nuevo Sol',
        'PHP' => 'Philippines Peso',
        'PLN' => 'Poland Zloty',
        'QAR' => 'Qatar Riyal',
        'RON' => 'Romania New Leu',
        'RUB' => 'Russia Ruble',
        'SHP' => 'Saint Helena Pound',
        'SAR' => 'Saudi Arabia Riyal',
        'RSD' => 'Serbia Dinar',
        'SCR' => 'Seychelles Rupee',
        'SGD' => 'Singapore Dollar',
        'SBD' => 'Solomon Islands Dollar',
        'SOS' => 'Somalia Shilling',
        'ZAR' => 'South Africa Rand',
        'LKR' => 'Sri Lanka Rupee',
        'SEK' => 'Sweden Krona',
        'CHF' => 'Switzerland Franc',
        'SRD' => 'Suriname Dollar',
        'SYP' => 'Syria Pound',
        'TWD' => 'Taiwan New Dollar',
        'THB' => 'Thailand Baht',
        'TTD' => 'Trinidad and Tobago Dollar',
        'TRY' => 'Turkey Lira',
        'TRL' => 'Turkey Lira',
        'TVD' => 'Tuvalu Dollar',
        'UAH' => 'Ukraine Hryvna',
        'GBP' => 'United Kingdom Pound',
        'USD' => 'United States Dollar',
        'UYU' => 'Uruguay Peso',
        'UZS' => 'Uzbekistan Som',
        'VEF' => 'Venezuela Bolivar',
        'VND' => 'Viet Nam Dong',
        'YER' => 'Yemen Rial',
        'ZWD' => 'Zimbabwe Dollar'
    );

    return $currencies;
}

function stt_all_languages($key = '')
{
    $language_codes = array(
        'en' => 'English',
        'aa' => 'Afar',
        'ab' => 'Abkhazian',
        'af' => 'Afrikaans',
        'am' => 'Amharic',
        'ar' => 'Arabic',
        'as' => 'Assamese',
        'ay' => 'Aymara',
        'az' => 'Azerbaijani',
        'ba' => 'Bashkir',
        'be' => 'Byelorussian',
        'bg' => 'Bulgarian',
        'bh' => 'Bihari',
        'bi' => 'Bislama',
        'bn' => 'Bengali/Bangla',
        'bo' => 'Tibetan',
        'br' => 'Breton',
        'ca' => 'Catalan',
        'co' => 'Corsican',
        'cs' => 'Czech',
        'cy' => 'Welsh',
        'da' => 'Danish',
        'de' => 'German',
        'dz' => 'Bhutani',
        'el' => 'Greek',
        'eo' => 'Esperanto',
        'es' => 'Spanish',
        'et' => 'Estonian',
        'eu' => 'Basque',
        'fa' => 'Persian',
        'fi' => 'Finnish',
        'fj' => 'Fiji',
        'fo' => 'Faeroese',
        'fr' => 'French',
        'fy' => 'Frisian',
        'ga' => 'Irish',
        'gd' => 'Scots/Gaelic',
        'gl' => 'Galician',
        'gn' => 'Guarani',
        'gu' => 'Gujarati',
        'ha' => 'Hausa',
        'hi' => 'Hindi',
        'hr' => 'Croatian',
        'hu' => 'Hungarian',
        'hy' => 'Armenian',
        'ia' => 'Interlingua',
        'ie' => 'Interlingue',
        'ik' => 'Inupiak',
        'in' => 'Indonesian',
        'is' => 'Icelandic',
        'it' => 'Italian',
        'iw' => 'Hebrew',
        'ja' => 'Japanese',
        'ji' => 'Yiddish',
        'jw' => 'Javanese',
        'ka' => 'Georgian',
        'kk' => 'Kazakh',
        'kl' => 'Greenlandic',
        'km' => 'Cambodian',
        'kn' => 'Kannada',
        'ko' => 'Korean',
        'ks' => 'Kashmiri',
        'ku' => 'Kurdish',
        'ky' => 'Kirghiz',
        'la' => 'Latin',
        'ln' => 'Lingala',
        'lo' => 'Laothian',
        'lt' => 'Lithuanian',
        'lv' => 'Latvian/Lettish',
        'mg' => 'Malagasy',
        'mi' => 'Maori',
        'mk' => 'Macedonian',
        'ml' => 'Malayalam',
        'mn' => 'Mongolian',
        'mo' => 'Moldavian',
        'mr' => 'Marathi',
        'ms' => 'Malay',
        'mt' => 'Maltese',
        'my' => 'Burmese',
        'na' => 'Nauru',
        'ne' => 'Nepali',
        'nl' => 'Dutch',
        'no' => 'Norwegian',
        'oc' => 'Occitan',
        'om' => '(Afan)/Oromoor/Oriya',
        'pa' => 'Punjabi',
        'pl' => 'Polish',
        'ps' => 'Pashto/Pushto',
        'pt' => 'Portuguese',
        'qu' => 'Quechua',
        'rm' => 'Rhaeto-Romance',
        'rn' => 'Kirundi',
        'ro' => 'Romanian',
        'ru' => 'Russian',
        'rw' => 'Kinyarwanda',
        'sa' => 'Sanskrit',
        'sd' => 'Sindhi',
        'sg' => 'Sangro',
        'sh' => 'Serbo-Croatian',
        'si' => 'Singhalese',
        'sk' => 'Slovak',
        'sl' => 'Slovenian',
        'sm' => 'Samoan',
        'sn' => 'Shona',
        'so' => 'Somali',
        'sq' => 'Albanian',
        'sr' => 'Serbian',
        'ss' => 'Siswati',
        'st' => 'Sesotho',
        'su' => 'Sundanese',
        'sv' => 'Swedish',
        'sw' => 'Swahili',
        'ta' => 'Tamil',
        'te' => 'Tegulu',
        'tg' => 'Tajik',
        'th' => 'Thai',
        'ti' => 'Tigrinya',
        'tk' => 'Turkmen',
        'tl' => 'Tagalog',
        'tn' => 'Setswana',
        'to' => 'Tonga',
        'tr' => 'Turkish',
        'ts' => 'Tsonga',
        'tt' => 'Tatar',
        'tw' => 'Twi',
        'uk' => 'Ukrainian',
        'ur' => 'Urdu',
        'uz' => 'Uzbek',
        'vi' => 'Vietnamese',
        'vo' => 'Volapuk',
        'wo' => 'Wolof',
        'xh' => 'Xhosa',
        'yo' => 'Yoruba',
        'zh' => 'Chinese',
        'zu' => 'Zulu',
    );

    if ($key) {
        return isset($language_codes[$key]) ? $language_codes[$key] : '';
    }
    return $language_codes;
}

function stt_date_format()
{
    $format = get_option('date_format', 'F j, Y');
    $format = str_replace('F', 'm', $format);
    $format = str_replace('j', 'd', $format);
    $format = str_replace('S', 'd', $format);
    $format = str_replace('n', 'M', $format);
    $format = str_replace('M', 'M', $format);
    $format = str_replace('y', 'Y', $format);

    return $format;
}

function stt_date_format_moment()
{
    $format = get_option('date_format', 'F j, Y');
    $format = str_replace('j', 'd', $format);
    $format = str_replace('S', 'd', $format);
    $format = str_replace('n', 'm', $format);

    $ori_format = [
        'd' => 'DD',
        'm' => 'MM',
        'M' => 'MMM',
        'Y' => 'YYYY',
        'y' => 'YY',
        'F' => 'MMMM',
    ];
    preg_match_all("/[a-zA-Z]/", $format, $out);

    $out = $out[0];
    foreach ($out as $key => $val) {
        foreach ($ori_format as $ori_key => $ori_val) {
            if ($val == $ori_key) {
                $format = str_replace($val, $ori_val, $format);
            }
        }
    }

    return $format;
}

function stt_time_format()
{
    return get_option('time_format', 'g:i a');
}

function stt_date_time_format()
{
    return stt_date_format() . ' ' . stt_time_format();
}

function stt_convert_date_to_default($date, $strtotime = false)
{
    $format = stt_date_format();

    $myDateTime = DateTime::createFromFormat($format, $date);
    if (!empty($myDateTime)) {
        $newDateString = $myDateTime->format('Y-m-d');
        if ($strtotime) {
            return strtotime($newDateString);
        }

        return $newDateString;
    } else {
        return false;
    }
}

function stt_date_diff($start, $end, $type = 'date')
{
    switch ($type) {
        case 'date':
            $start = date_create(date('Y-m-d', $start));
            $end = date_create(date('Y-m-d', $end));
            $diff = date_diff($start, $end);

            return $diff->format("%a");
            break;
        case 'hour':
            $diff = $end - $start;
            $hour = (int)($diff / 3600);
            if ($hour <= 0) {
                $hour = 1;
            }
            if ($diff % 3600) {
                $hour += 1;
            }

            return $hour;
            break;
        case 'minute':
            $diff = $end - $start;
            $minute = (int)($diff / 60);
            if ($minute <= 0) {
                $minute = 1;
            }
            if ($diff % 60) {
                $minute += 1;
            }

            return $minute;
            break;
        case 'second':
            return $end - $start;
            break;
    }

}

function stt_time_since($older_date, $newer_date = false, $gmt = false)
{

    $unknown_text = __('sometimes', 'layout-blog');
    $right_now_text = __('right now', 'layout-blog');
    $ago_text = __('%s ago', 'layout-blog');

    $chunks = array(
        array(60 * 60 * 24 * 365, __('year', 'layout-blog'), __('years', 'layout-blog')),
        array(60 * 60 * 24 * 30, __('month', 'layout-blog'), __('months', 'layout-blog')),
        array(60 * 60 * 24 * 7, __('week', 'layout-blog'), __('weeks', 'layout-blog')),
        array(60 * 60 * 24, __('day', 'layout-blog'), __('days', 'layout-blog')),
        array(60 * 60, __('hour', 'layout-blog'), __('hours', 'layout-blog')),
        array(60, __('minute', 'layout-blog'), __('minutes', 'layout-blog')),
        array(1, __('second', 'layout-blog'), __('seconds', 'layout-blog'))
    );

    if (!empty($older_date) && !is_numeric($older_date)) {
        $time_chunks = explode(':', str_replace(' ', ':', $older_date));
        $date_chunks = explode('-', str_replace(' ', '-', $older_date));
        $older_date = gmmktime((int)$time_chunks[1], (int)$time_chunks[2], (int)$time_chunks[3], (int)$date_chunks[1], (int)$date_chunks[2], (int)$date_chunks[0]);
    }

    $newer_date = (!$newer_date) ? strtotime(current_time('mysql', $gmt)) : $newer_date;

    $since = $newer_date - $older_date;

    if (0 > $since) {
        $output = $unknown_text;

    } else {

        for ($i = 0, $j = count($chunks); $i < $j; ++$i) {
            $seconds = $chunks[$i][0];

            $count = floor($since / $seconds);
            if (0 != $count) {
                break;
            }
        }

        if (!isset($chunks[$i])) {
            $output = $right_now_text;

        } else {

            $output = (1 == $count) ? '1 ' . $chunks[$i][1] : $count . ' ' . $chunks[$i][2];

            if ($i + 2 < $j) {
                $seconds2 = $chunks[$i + 1][0];
                $name2 = $chunks[$i + 1][1];
                $count2 = floor(($since - ($seconds * $count)) / $seconds2);

                if (0 != $count2) {
                    $output .= (1 == $count2) ? _x(',', 'Separator in time since', 'layout-blog') . ' 1 ' . $name2 : _x(',', 'Separator in time since', 'layout-blog') . ' ' . $count2 . ' ' . $chunks[$i + 1][2];
                }
            }

            if (!(int)trim($output)) {
                $output = $right_now_text;
            }
        }
    }

    if ($output != $right_now_text) {
        $output = sprintf($ago_text, $output);
    }

    return apply_filters('stt_get_time_since', $output, $older_date, $newer_date);
}
if (!function_exists('stt_server')) {
    function stt_server($name = '')
    {
        global $stt_server;
        return isset($stt_server[$name]) ? $stt_server[$name] : $stt_server;
    }
}
