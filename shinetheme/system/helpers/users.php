<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 3/23/2019
 * Time: 7:15 PM
 */
if (!function_exists('stt_list_partners')) {
    function stt_list_partners()
    {
        $args = [
            'role' => 'partner'
        ];
        $users = new WP_User_Query($args);

        return $users->get_results();
    }
}
if (!function_exists('stt_user_can_manage_service')) {
    function stt_user_can_manage_service($user_id = '')
    {
        if (empty($user_id)) {
            $user_id = get_current_user_id();
        }
        if (!$user_id) {
            return false;
        }
        $roles = stt_get_role($user_id);
        if (in_array('administrator', $roles) || in_array('partner', $roles)) {
            return true;
        }
        return false;
    }
}
if (!function_exists('stt_get_role')) {
    function stt_get_role($user_id)
    {
        $userData = get_userdata($user_id);
        if ($userData) {
            return $userData->roles;
        }
        return [];
    }
}

if (!function_exists('stt_is_admin')) {
    function stt_is_admin($user_id = '')
    {
        if (empty($user_id)) {
            $user_id = get_current_user_id();
        }
        $roles = stt_get_role($user_id);
        if (in_array('administrator', $roles)) {
            return true;
        }
        return false;
    }
}

if (!function_exists('stt_admin_email')) {
    function stt_admin_email()
    {
        return get_option('admin_email');
    }
}

if (!function_exists('stt_admin_data')) {
    function stt_admin_data()
    {
        $adminEmail = stt_admin_email();
        return get_user_by('email', $adminEmail);
    }
}


if (!function_exists('stt_username')) {
    function stt_username($user_id = '')
    {
        if (empty($user_id)) {
            $user_id = get_current_user_id();
        }
        $userdata = get_userdata($user_id);
        return (!empty($userdata->display_name)) ? $userdata->display_name : $userdata->first_name . ' ' . $userdata->last_name;
    }
}
if (!function_exists('stt_get_avatar')) {
    function stt_get_avatar($user_id = '', $size = 50)
    {
        if (!$user_id) {
            $user_id = get_current_user_id();
        }
        $avatar = st_get_user_meta($user_id, 'stt_avatar');
        if ($avatar) {
            $avatarUrl = wp_get_attachment_image_url($avatar, [$size, $size]);
            return '<img src="' . $avatarUrl . '" class="avatar rounded-circle" alt="Avatar">';
        } else {
            return get_avatar($user_id, $size,'','',array('class' => 'rounded-circle'));
        }
    }
}
if (!function_exists('stt_user_data')) {
    function stt_user_data($user_id, $key = '')
    {
        $userdata = get_userdata($user_id);
        if (!empty($key)) {
            return isset($userdata->$key) ? $userdata->$key : '';
        }
        return $userdata;
    }
}