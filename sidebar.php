<?php
    /**
     * Created by PhpStorm.
     * User: Administrator
     * Date: 11/9/2016
     * Time: 2:51 PM
     * Since 1.0.0
     * Updated 1.0.0
     */

    if ( is_active_sidebar( 'main-sidebar' ) ):
        ?>
        <div class="sidebar">
            <?php dynamic_sidebar( 'main-sidebar' ); ?>
        </div>
    <?php endif; ?>