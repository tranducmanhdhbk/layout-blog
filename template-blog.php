<?php
/**
 * Template Name:  Template Blog
 * Created by PhpStorm.
 * User: Jream
 * Date: 9/3/2019
 * Time: 9:30 PM
 */
get_header();
$pageID = get_the_ID();
while (have_posts()): the_post();
    echo '<div class="stt-content-wrapper">';
    the_content();
    if (comments_open() || get_comments_number()) {
        if(!is_front_page()) {
            echo '<div class="comment-wrapper"><div class="container">';
            stt_comments_template($pageID);
            echo '</div></div>';
        }
    }
    echo '</div>';
endwhile;
get_footer();