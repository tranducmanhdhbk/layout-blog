<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 11/9/2016
 * Time: 2:51 PM
 * Since 1.0.0
 * Updated 1.0.0
 */

get_header();
$pageID = get_the_ID();
if (!is_home()) {
    stt_breadcrumbs(true);
}
while (have_posts()): the_post();
    echo '<div class="stt-content-wrapper"><div class="container">';
    ?>
    <h2 class="page-title"><?php echo esc_html(get_the_title()); ?></h2>
    <?php
    the_content();
    wp_link_pages(
        array(
            'before' => '<div class="page-links">' . esc_html__('Pages:', 'layout-blog'),
            'after' => '</div>',
        )
    );
    if (comments_open() || get_comments_number()) {
        echo '<div class="comment-wrapper">';
        stt_comments_template($pageID);
        echo '</div>';
    }
    echo '</div></div>';
endwhile;
wp_reset_postdata();
wp_reset_query();
get_footer();
?>