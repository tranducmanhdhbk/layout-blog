<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 11/9/2016
 * Time: 2:51 PM
 * Since 1.0.0
 * Updated 1.0.0
 */

get_header();
$item_per_row = 2;
if(!is_home()) {
    stt_breadcrumbs( true );
}

$active_sidebar = is_active_sidebar( 'blog-sidebar' );
$class_left = 'col-lg-8';
if(!$active_sidebar){
    $class_left = 'col-lg-12';
    $item_per_row = 3;
}
$class_number_column = 'number-column-' . stt_esc_data($item_per_row);
?>
<div class="stt-index-page">
    <div class="container">
        <div class="row">
            <div class="<?php echo esc_attr($class_left); ?>">
                <div class="stt-elements stt-list-of-blog blogpage">
                    <?php
                    $class = 'col-lg-6';
                    if($item_per_row == '3'){
                        $class = 'col-lg-4 col-md-4';
                    }
                    global $wp_query;
                    if (have_posts()) {
                        $stt = stt_get_instance();
                        echo '<div class="stt-list-blog"><div class="card-columns '. esc_attr($class_number_column) .'">';
                        while (have_posts()) {
                            the_post();
                            echo '<div class="card">';
                            $stt->load->view('grid3', 'frontend/blog');
                            echo '</div>';
                        }
                        echo '</div></div>';
                    }
                    wp_reset_postdata();
                    echo '<div class="stt-pagination">';
                    stt_pagination($wp_query, false);
                    echo '</div>';
                    ?>
                </div>
            </div>
            <?php if($active_sidebar){ ?>
                <div class="col-lg-4">
                    <?php dynamic_sidebar( 'blog-sidebar' ); ?>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<?php
get_footer();
?>
