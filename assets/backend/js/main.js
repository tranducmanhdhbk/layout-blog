(function ($) {
    'use strict';
    $(document).ready(function () {
        $(document).on('click', '.video-img .upload-image .add-image', function (e) {
            e.preventDefault();
            var t = $(this),
                parent = t.closest('.upload-image'),
                thumb = $('.thumb', parent),
                input = $('input.img_url', parent),
                button = $('.remove-image', parent);

            var custom_uploader = wp.media({
                title: 'Select image',
                multiple: false
            }).on('select', function() {
                var attachment = custom_uploader.state().get('selection').first().toJSON();
                input.val(attachment.id);
                thumb.html('<img src="'+ attachment.url +'" />')
                button.show();
            }).open();
        });

        $(document).on('click', '.video-img .upload-image .remove-image', function (e) {
            e.preventDefault();
            var t = $(this),
                parent = t.closest('.upload-image'),
                thumb = $('.thumb', parent),
                input = $('input.img_url', parent);
            input.val('');
            thumb.html('')
            t.hide();
        });
    });
    $(document).ready(function () {
        var ST_Gallery = {

            frame: function (elm) {
                var selection = this.select(elm);
                this._frame = wp.media({
                    id: 'stt-gallery-frame'
                    , frame: 'post'
                    , state: 'gallery-edit'
                    , title: elm.data('title')
                    , editing: true
                    , multiple: true
                    , selection: selection
                });
                this._frame.on('update', function () {
                    var controller = ST_Gallery._frame.states.get('gallery-edit')
                        , library = controller.get('library')
                        , ids = library.pluck('id')
                        , parent = $(elm).parents('.stt-upload-wrapper')
                        , input = parent.children('.upload_value');
                    input.attr('value', ids);
                    $.ajax({
                        type: 'POST',
                        url: st_params.ajaxurl,
                        dataType: 'json',
                        data: {
                            action: 'gallery_update',
                            ids: ids,
                            security: st_params.security
                        },
                        success: function (res) {
                            parent.children('.attachments').html(res.img);
                            input.attr('value', res.ids);
                        }
                    })
                });
                return this._frame
            },
            select: function (elm) {
                var input = $(elm).parents('.stt-upload-wrapper').children('.upload_value')
                    , ids = input.attr('value')
                    , _shortcode = '[gallery ids=\'' + ids + '\]'
                    ,
                    shortcode = wp.shortcode.next('gallery', _shortcode)
                    , defaultPostId = wp.media.gallery.defaults.id
                    , attachments
                    , selection;
                // Bail if we didn't match the shortcode or all of the content.
                if (!shortcode)
                    return;
                // Ignore the rest of the match object.
                shortcode = shortcode.shortcode;
                if (_.isUndefined(shortcode.get('id')) && !_.isUndefined(defaultPostId))
                    shortcode.set('id', defaultPostId);

                if (_.isUndefined(shortcode.get('ids')) && !input.hasClass('ot-gallery-shortcode') && ids)
                    shortcode.set('ids', ids);

                if (_.isUndefined(shortcode.get('ids')))
                    shortcode.set('ids', '0');
                attachments = wp.media.gallery.attachments(shortcode)
                selection = new wp.media.model.Selection(attachments.models, {
                    props: attachments.props.toJSON()
                    , multiple: true
                });
                selection.gallery = attachments.gallery;
                // Fetch the query's attachments, and then break ties from the query to allow for sorting.
                selection.more().done(function () {
                    selection.props.set({query: false});
                    selection.unmirror();
                    selection.props.unset('orderby');
                });
                return selection
            },
            open: function (elm) {
                this.frame(elm).open()
            },
            remove: function (elm) {
                // $(elm).parents('.ots-upload-wrapper').children('.upload_value').attr('value', '');
                $(elm).parents('.attachments-item ').remove();
                var ids = [];
                $('.stt-upload-wrapper .attachments .attachments-item').each(function () {
                    var id = $(this).find('img').data('id');
                    if (!ids.includes(id)) {
                        ids.push(id);
                    }
                });
                $('.stt-upload-wrapper .upload_value').val(ids.toString());
            },
            avatar:function (t) {
                if (window.wp && wp.media) {
                    window.st_media_frame = window.st_media_frame || new wp.media.view.MediaFrame.Select({
                        title: t.attr('title'),
                        button: {
                            text: t.data('text')
                        },
                        multiple: false
                    });
                    window.st_media_frame.on('select', function () {
                        var attachment = window.st_media_frame.state().get('selection').first(),
                            attachment_id = attachment.attributes.id,
                            mime = attachment.attributes.mime,
                            regex = /^image\/(?:jpe?g|png|gif|svg\+xml|x-icon)$/i;
                        if (mime.match(regex)) {
                            parent = t.closest('form');
                            var data =parent.serializeArray();
                            data.push({
                                name: 'security',
                                value: st_params.security
                            }, {
                                name: 'attachment_id',
                                value: attachment_id
                            },
                                {
                                    name:'action',
                                    value:'st_dashboard_change_avatar'
                                },
                                );

                            $.post(st_params.ajaxurl, data, function (respon) {
                                if (typeof respon == 'object') {
                                    if (respon.status === 1) {
                                        $('input[name="upload_value"]', parent).val(respon.avatar);
                                        $('.stt-edit-avatar', parent).html(respon.url);
                                    }
                                }
                            }, 'json');
                        }
                        window.st_media_frame.off('select');
                    }).open();
                }
            }
        };
        $(document).on('click.', '.stt-attachments-delete', function (e) {
            e.preventDefault()
            ST_Gallery.remove($(this))
        });
        $(document).on('click.', '.stt-update-avatar', function (e) {
            e.preventDefault()
            ST_Gallery.avatar($(this))
        });
        $(document).on('click', '.add-attachments', function (e) {
            e.preventDefault()
            ST_Gallery.open($(this))
        });

    });
})(jQuery);