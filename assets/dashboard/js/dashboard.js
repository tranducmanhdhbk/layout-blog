Object.size = function (obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};

function whichTransitionEvent() {
    var t;
    var el = document.createElement('fakeelement');
    var transitions = {
        'transition': 'transitionend',
        'OTransition': 'oTransitionEnd',
        'MozTransition': 'transitionend',
        'WebkitTransition': 'webkitTransitionEnd'
    };

    for (t in transitions) {
        if (el.style[t] !== undefined) {
            return transitions[t];
        }
    }
}

jQuery(document).ready(function ($) {
    'use strict';

    var ST_Dashboard = {
        init: function () {
            var base = this;
            var body = $('body');
            base.initEqualTop(body);
            base.initBindData(body);
            base.initMatchHeigth(body);
            base.initNavs(body);
            base.initCheckAll(body);

            setTimeout(function () {
                $('[data-flickity]', body).flickity({
                    "imagesLoaded": true
                });
            }, 1000);
        },
        initNavs: function (el) {
            $('#st-dashboard-navs', el).on('click', '.toogle-menu', function (ev) {
                ev.preventDefault();
                $('#st-dashboard-navs').toggleClass('toggle');
                $('#st-dashboard-content').toggleClass('toggle');
            });
            $.fn.niceScroll && $('#st-dashboard-navs').niceScroll();
            el.on('resize', function () {
                $.fn.niceScroll && $('#st-dashboard-navs').niceScroll();
            }).resize();
        },
        initMatchHeigth: function (body) {
            $('.has-matchHeight', body).matchHeight();
        },
        initEqualTop: function (el) {
            $('[data-st-equal-top]', el).each(function () {
                var t = $(this);
                var target = t.data('st-equal-top');
                if ($(target).length) {
                    t.css({'margin-top': $(target).position().top});
                }
            });
        },
        initBindData: function (el) {
            $('[data-st-bind-from]', el).each(function () {
                var t = $(this);
                var target = t.data('st-bind-from');
                if (target.length) {
                    let val = $(target).val() || t.data('st-bind-default');
                    t.text(val);
                    el.on('change keyup', target, function () {
                        var val = $(this).val() || t.data('st-bind-default');
                        t.text(val);
                    });
                }
            });
        },
        initCheckAll: function (el) {
            $('.check-all', el).click(function (ev) {
                var t = $(this);
                var parent = $(this).closest('.form-group');
                $('input[type="checkbox"]', parent).each(function () {
                    if (!$(this).is(t)) {
                        $(this).prop('checked', !$(this).is(':checked'));
                    }
                });
            });
        }
    };
    ST_Dashboard.init();

    var ST_Login = {
        init: function () {
            var base = this;
            var body = $('body');
            base.initLogin(body);
        },
        initLogin: function (el) {
            var base = this;
            $('#st-dashboard-login-form', el).submit(function (ev) {
                ev.preventDefault();
                var form = $(this);
                var data = form.serializeArray();
                data.push({
                    name: 'security',
                    value: st_params.security
                });

                base.initLoader(form);
                $.post(st_params.ajaxurl, data, function (respon) {
                    if (typeof respon == 'object') {
                        $('.form-message', form).html(respon.message);
                        base.initLoader(form, 'reset');
                        if (respon.redirect) {
                            setTimeout(function () {
                                window.location.href = respon.redirect;
                            }, 2000);
                        }
                    }
                }, 'json');
            });
        },
        initLoader: function (el, reset) {
            if (reset === 'reset') {
                $('.st-loader', el).hide();
            } else {
                $('.st-loader', el).show();
                $('.form-message', el).empty();
            }
        }
    };
    ST_Login.init();

    var ST_Gallery = {
        frame: function (elm) {
            var selection = this.select(elm);
            this._frame = wp.media({
                id: 'stt-gallery-frame'
                , frame: 'post'
                , state: 'gallery-edit'
                , title: elm.data('title')
                , editing: true
                , multiple: true
                , selection: selection
            });

            this._frame.on('update', function () {
                var controller = ST_Gallery._frame.states.get('gallery-edit')
                    , library = controller.get('library')
                    , ids = library.pluck('id')
                    , parent = $(elm).parents('.st-field-media-advanced')
                    , input = parent.find('.upload_value');

                input.attr('value', ids);
                $.ajax({
                    type: 'POST',
                    url: st_params.ajaxurl,
                    dataType: 'json',
                    data: {
                        action: 'st_gallery_update_dashboard',
                        ids: ids,
                        post_id: input.data('post-id'),
                        security: st_params.security
                    },
                    success: function (res) {
                        parent.find('.st-dashboard-upload-render').html(res.img);
                        input.attr('value', res.ids);
                        if (res.thumbnail) {
                            $('#st-dashboard-service-preview .img-featured').attr('src', res.thumbnail);
                        }
                    }
                })
            });

            return this._frame

        },
        frame2: function (elm) {
            var selection = this.select(elm);
            this._frame = wp.media({
                id: 'stt-gallery-frame'
                , frame: 'post'
                , state: 'gallery-edit'
                , title: elm.data('title')
                , editing: true
                , multiple: true
                , selection: selection
            });

            this._frame.on('update', function () {
                var controller = ST_Gallery._frame.states.get('gallery-edit')
                    , library = controller.get('library')
                    , ids = library.pluck('id')
                    , parent = $(elm).parents('.st-field-images')
                    , input = parent.find('.upload_value');

                input.attr('value', ids);
                $.ajax({
                    type: 'POST',
                    url: st_params.ajaxurl,
                    dataType: 'json',
                    data: {
                        action: 'st_images_update_dashboard',
                        ids: ids,
                        post_id: input.data('post-id'),
                        security: st_params.security
                    },
                    success: function (res) {
                        parent.find('.st-dashboard-upload-render').html(res.img);
                        input.attr('value', res.ids);
                    }
                })
            });

            return this._frame

        },
        select: function (elm) {
            var input = $(elm).parents('.st-field').find('.upload_value')
                , ids = input.attr('value')
                , _shortcode = '[gallery ids=\'' + ids + '\]'
                ,
                shortcode = wp.shortcode.next('gallery', _shortcode)
                , defaultPostId = wp.media.gallery.defaults.id
                , attachments
                , selection;

            // Bail if we didn't match the shortcode or all of the content.
            if (!shortcode)
                return;

            // Ignore the rest of the match object.
            shortcode = shortcode.shortcode;

            if (_.isUndefined(shortcode.get('id')) && !_.isUndefined(defaultPostId))
                shortcode.set('id', defaultPostId);

            if (_.isUndefined(shortcode.get('ids')) && !input.hasClass('ot-gallery-shortcode') && ids)
                shortcode.set('ids', ids);

            if (_.isUndefined(shortcode.get('ids')))
                shortcode.set('ids', '0');

            attachments = wp.media.gallery.attachments(shortcode);

            selection = new wp.media.model.Selection(attachments.models, {
                props: attachments.props.toJSON()
                , multiple: true
            });

            selection.gallery = attachments.gallery;

            selection.more().done(function () {
                selection.props.set({query: false});
                selection.unmirror();
                selection.props.unset('orderby');
            });
            return selection
        },
        open: function (elm) {
            this.frame(elm).open()
        },
        open2: function (elm) {
            this.frame2(elm).open()
        },
        remove: function (elm) {
            $(elm).parents('.st-field').children('.upload_value').attr('value', '');
            $(elm).parents('.st-field').children('.st-dashboard-upload-render').empty();
        }
    };

    var ST_Options = {
        prevTab: false,
        nextTab: false,
        form: false,
        init: function () {
            var base = this;
            var body = $('body');
            var metabox = $('#st-dashboard-content', body);
            base.initTab(metabox);
            base.initInput(metabox);
            base.initColor(metabox);
            base.initGoogleMap(metabox);
            base.initGoogleFonts(metabox);
            base.initSelect(metabox);
            base.initToolTip(metabox);
            base.initGallery(metabox);
            base.initImage(metabox);
            base.initImages(metabox);
            base.initCalendar(metabox);
            base.initcustomPrice(metabox);
            base.initDatePicker(metabox);
            base.initListItem(metabox);
            base.initMaxLength(metabox);
            base.initValidation(metabox, false);
            base.initSubmit(metabox);
            base.initConditions(metabox);

        },

        initColor: function (el) {
            $('.st-field-color', el).each(function () {
                var render = $('.render-color', this).get(0);
                var input = $('input', this).get(0);
                var myColor = new jscolor(input, {
                    styleElement: render,
                    hash: true
                });
            });

        },
        initTab: function (el) {
            var base = this;
            var tabContainer = $('.nav-tabs', el);
            $('a[data-toggle="tab"]', tabContainer).on('shown.bs.tab', function (e) {
                var current = $(e.target);
                base.gotoTab(current);
            });
            $('.nav-item .nav-link', tabContainer).click(function (ev) {
                ev.preventDefault();
                var t = $(this);
                base.nextTab = t;
                base.prevTab = t.closest('.nav-tabs').find('.nav-item .nav-link.active');
                let buttonSubmit = $('button.btn-submit-form[data-current-tab="#' + base.prevTab.attr('id') + '"]');
                if (buttonSubmit.length) {
                    let form = $(buttonSubmit.data('form'));
                    base.doSubmit(form, buttonSubmit, false);
                    if (Object.size(st_params.isValidated)) {
                        return false;
                    }
                }
            });
            $('.st-sub-tab-title .nav-link', el).click(function (ev) {
                ev.preventDefault();
                var t = $(this),
                    parent = t.closest('.st-sub-tabs');
                $('li', parent).removeClass('active');
                t.parent().addClass('active');
                $('.sub-tab-content', parent).removeClass('active');
                var target = t.attr('href');
                $(target).addClass('active');
            });
        },
        gotoTab: function (goto) {
            var base = this;
            if (goto.length) {
                var parent = goto.closest('.nav-tabs');
                var index = goto.closest('li').index();

                $('.nav-item .head', parent).removeClass('active-left active-right');
                $('.nav-item .nav-link', parent).removeClass('active active-left active-right');
                var tabContent = $('#' + goto.attr('aria-controls'));
                if (tabContent.length) {
                    tabContent.closest('.tab-content').find('.tab-pane').removeClass('active');
                    tabContent.addClass('active');
                }
                $('.nav-item', parent).each(function () {
                    var t = $(this);
                    if (t.index() < index) {
                        $('.head', t).addClass('active-left active-right');
                        $(' .nav-link', t).addClass('active-left active-right');
                    } else {
                        if (t.index() === index) {
                            $('.head', t).addClass('active-left');
                            $('.nav-link', t).addClass('active active-left');
                        }
                    }
                });

                if (index === $('.nav-item', parent).length - 1 && !parent.hasClass('nav-theme-settings')) {
                    $('.nav-item', parent).find('.head').addClass('active-left active-right');
                    $('.nav-item', parent).find('.nav-link').addClass('active active-left active-right');
                }
            }
        },
        initInput: function (el) {
            var base = this;
            $('.has-inner-label', el).each(function () {
                var input = $(this);
                let val = input.val();
                if (val !== '' || input.hasClass('has-default')) {
                    input.addClass('has-value');
                } else {
                    input.removeClass('has-value');
                }
            });
            $('.has-inner-label', el).on('change keyup', function (ev) {
                var input = $(this);
                let val = input.val();
                if (val !== '' || input.hasClass('has-default')) {
                    input.addClass('has-value');
                } else {
                    input.removeClass('has-value');
                }
            });
        },
        initGoogleFonts: function (el) {
            var base = this;
            $('.selectpicker', el).selectpicker();
            $('.st-google-fonts', el).on('change', function () {
                var t = $(this);
                var parent = t.closest('.st-field');
                var variants = $('option:selected', t).data('variants');
                var subsets = $('option:selected', t).data('subsets');
                if (variants) {
                    $('.st-font-variants', parent).empty();
                    variants = variants.split(',');
                    for (let i = 0; i < variants.length; i++) {
                        $('.st-font-variants', parent).append('<div class="checkbox-item"><input id="font-variant-' + variants[i] + '" type="checkbox" name="font_variants[]" value="' + variants[i] + '"><label for="font-variant-' + variants[i] + '">' + variants[i] + '</label></div>')
                    }
                }
                if (subsets) {
                    $('.st-font-subsets', parent).empty();
                    subsets = subsets.split(',');
                    for (let i = 0; i < subsets.length; i++) {
                        $('.st-font-subsets', parent).append('<div class="checkbox-item"><input id="font-variant-' + subsets[i] + '" type="checkbox" name="font_subsets[]" value="' + subsets[i] + '"><label for="font-variant-' + subsets[i] + '">' + subsets[i] + '</label></div>')
                    }
                }
            });
        },
        initGoogleMap: function (el) {
            var base = this;
            $('.google-map', el).each(function () {
                var googleEl = $(this),
                    googleMap = googleEl.get(0);
                var parent = googleEl.closest('.st-field');
                var options = {
                    center: {lat: googleEl.data('lat'), lng: googleEl.data('lng')},
                    zoom: googleEl.data('zoom'),
                    disableDefaultUI: true
                };
                var markers = null;
                var map = new google.maps.Map(googleMap, options);

                parent.on('st_autocomplete_changed', function (el, places) {
                    var myLatLng = {lat: places.geometry.location.lat(), lng: places.geometry.location.lng()};
                    markers && markers.setMap(null);
                    var marker = new google.maps.Marker({
                        position: myLatLng,
                        map: map
                    });
                    markers = marker;
                    map.setCenter(myLatLng);
                });

                map.addListener('zoom_changed', function () {
                    $('.st-zoom', parent).attr('value', map.getZoom());
                });

                var geocoder = new google.maps.Geocoder();
                google.maps.event.addListener(map, 'click', function (event) {
                    var fetch = {
                        'locality': $('.st-city', parent),
                        'administrative_area_level_1': $('.st-state', parent),
                        'postal_code': $('.st-postcode', parent),
                        'country': $('.st-country', parent),
                    };
                    var componentForm = {
                        locality: 'long_name',
                        administrative_area_level_1: 'short_name',
                        country: 'long_name',
                        postal_code: 'short_name'
                    };

                    geocoder.geocode({
                        'latLng': event.latLng
                    }, function (results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            if (results[0]) {
                                var latLng = {lat: event.latLng.lat(), lng: event.latLng.lng()};
                                var marker = new google.maps.Marker({
                                    position: latLng,
                                    map: map
                                });
                                markers && markers.setMap(null);
                                markers = marker;
                                map.setCenter(latLng);

                                $('.st-lat', parent).attr('value', event.latLng.lat()).trigger('change');
                                $('.st-lng', parent).attr('value', event.latLng.lng()).trigger('change');
                                $('.has-google-search', parent).attr('value', results[0].formatted_address).trigger('change');

                                for (var key in fetch) {
                                    fetch[key].attr('value', '');
                                }
                                for (var i = 0; i < results[0].address_components.length; i++) {
                                    var addressType = results[0].address_components[i].types[0];
                                    if (componentForm[addressType]) {
                                        var val = results[0].address_components[i][componentForm[addressType]];
                                        fetch[addressType].attr('value', val).trigger('change');
                                    }
                                }
                            }
                        }
                    });
                });
            });
            $('.has-google-search', el).each(function () {
                var searchEl = $(this),
                    search = searchEl.get(0);
                var parent = searchEl.closest('.st-field');
                var fetch = {
                    'locality': $('.st-city', parent),
                    'administrative_area_level_1': $('.st-state', parent),
                    'postal_code': $('.st-postcode', parent),
                    'country': $('.st-country', parent),
                };
                var componentForm = {
                    locality: 'long_name',
                    administrative_area_level_1: 'short_name',
                    country: 'long_name',
                    postal_code: 'short_name'
                };

                var searchBox = new google.maps.places.Autocomplete(search, {types: ['geocode']});
                searchBox.setFields(['address_component', 'geometry']);

                searchBox.addListener('place_changed', function () {
                    var places = searchBox.getPlace();
                    if (places.length == 0) {
                        return;
                    }
                    for (var key in fetch) {
                        fetch[key].attr('value', '');
                    }
                    for (var i = 0; i < places.address_components.length; i++) {
                        var addressType = places.address_components[i].types[0];
                        if (componentForm[addressType]) {
                            var val = places.address_components[i][componentForm[addressType]];
                            fetch[addressType].attr('value', val).trigger('change');
                        }
                    }

                    $('.st-lat', parent).attr('value', places.geometry.location.lat()).trigger('change');
                    $('.st-lng', parent).attr('value', places.geometry.location.lng()).trigger('change');
                    searchEl.trigger('change');
                    parent.trigger('st_autocomplete_changed', [places]);
                });
            });
        },
        initSelect: function (el) {
            var base = this;
            $('.selectpicker', el).selectpicker();
        },
        initToolTip: function (el) {
            $('[data-toggle="tooltip"]', el).tooltip();
        },
        initGallery: function (el) {
            var base = this;
            $('.st-field-media-advanced', el).each(function () {
                var t = $(this),
                    button = $('.btn-upload-media', t);

                button.click(function (ev) {
                    ev.preventDefault();
                    ST_Gallery.open($(this));
                });
                t.on('blur', '.gallery-title', function () {
                    var parent = $(this).closest('.gallery-item'),
                        loader = $('.st-loader', parent);
                    var val = $(this).val();
                    if (val !== '') {
                        loader.show();
                        var data = {
                            action: 'st_change_caption_image',
                            caption: val,
                            id: $(this).attr('data-id'),
                            security: st_params.security
                        };
                        $.post(st_params.ajaxurl, data, function (respon) {
                            if (typeof respon == 'object') {
                                if (respon.status == 1) {
                                    console.log('Changed the caption');
                                } else {
                                    console.log('Error when change the caption');
                                }
                            }
                            loader.hide();
                        }, 'json');
                    }
                });
                t.on('click', '.stt-gallery-delete', function (ev) {
                    ev.preventDefault();
                    var t = $(this),
                        container = t.closest('.st-field-media-advanced'),
                        gallery = $('.upload_value', container).val(),
                        id = t.attr('data-id');
                    $.confirm({
                        animation: 'none',
                        title: 'Confirm!',
                        content: 'Are you sure to delete this image?',
                        buttons: {
                            ok: {
                                text: "Delete it!",
                                btnClass: 'btn-primary',
                                action: function () {
                                    if (gallery !== '') {
                                        gallery = gallery.replace(',' + id, '');
                                        gallery = gallery.replace(id + ',', '');
                                    }
                                    $('.upload_value', container).attr('value', gallery).trigger('change');
                                    t.closest('.item').remove();
                                }
                            },
                            close: function () {

                            }
                        }
                    });
                });
            });
            el.on('click', '.stt-gallery-add-featured', function (ev) {
                ev.preventDefault();

                var t = $(this),
                    parent = t.closest('.gallery-item');

                var data = {
                    post_id: t.attr('data-post-id'),
                    id: t.attr('data-id'),
                    action: 'st_dashboard_set_featured',
                    security: st_params.security
                };
                $('.st-loader', parent).show();

                $.post(st_params.ajaxurl, data, function (respon) {
                    if (typeof respon == 'object') {
                        if (respon.status == 1) {
                            t.closest('.st-dashboard-upload-render').find('.gallery-item .stt-gallery-add-featured').removeClass('is-featured');
                            t.closest('.st-dashboard-upload-render').find('.gallery-item').removeClass('is-featured');
                            $('#st-dashboard-service-preview').find('.featured').removeClass('is-featured');

                            $('#st-dashboard-service-preview').find('.thumbnail .img-featured').attr('src', respon.img);

                            $('#st-dashboard-service-preview').find('.featured').addClass('is-featured');
                            t.addClass('is-featured');
                            t.closest('.gallery-item').addClass('is-featured');
                        }
                        base.alert(respon);
                    }
                    $('.st-loader', parent).hide();
                }, 'json');
            });
        },
        initImage: function (el) {
            var base = this;
            $(document).on('click', '.stt-upload-wrapper .add-attachment', function () {
                var t = $(this);
                var field_id = t.closest('.stt-upload-wrapper').find('input').attr('id'),
                    post_id = t.attr('rel'),
                    btnContent = '';
                if (window.wp && wp.media) {
                    window.st_media_frame = window.st_media_frame || new wp.media.view.MediaFrame.Select({
                        title: t.attr('title'),
                        button: {
                            text: t.data('text')
                        },
                        multiple: false
                    });
                    window.st_media_frame.on('select', function () {
                        var attachment = window.st_media_frame.state().get('selection').first(),
                            href = attachment.attributes.url,
                            attachment_id = attachment.attributes.id,
                            mime = attachment.attributes.mime,
                            regex = /^image\/(?:jpe?g|png|gif|svg\+xml|x-icon)$/i;
                        if (mime.match(regex)) {
                            btnContent += '<div class="attachment-item"><img src="' + href + '" alt="Attachment"></div>';
                        }
                        $('#' + field_id).attr('value', attachment_id);
                        t.closest('.stt-upload-wrapper').find('.attachments').html(btnContent);
                        t.closest('.stt-upload-wrapper').find('.remove-attachment').removeClass('d-none');
                        let btnAdd = t.closest('.stt-upload-wrapper').find('.add-attachment');
                        btnAdd.text(btnAdd.attr('data-change'));
                        window.st_media_frame.off('select');
                    }).open();
                }
                return false;
            });
            $(document).on('click', '.remove-attachment', function (event) {
                event.preventDefault();
                $(this).closest('.stt-upload-wrapper').find('input').attr('value', '').trigger('change');
                $(this).closest('.stt-upload-wrapper').find('.attachment-item').remove();
                $(this).addClass('d-none');

                let btnAdd = $(this).closest('.stt-upload-wrapper').find('.add-attachment');
                btnAdd.text(btnAdd.attr('data-text'));

                return false;
            });
        },
        initImages: function (el) {
            var base = this;
            $(document).on('click.st_gallery.data-api', '.stt-attachments-delete', function (e) {
                e.preventDefault();
                var t = $(this),
                    parent = t.parent();
                var id = parent.attr('data-id');
                var images = t.closest('.st-field').find('.upload_value').val();
                if (images !== '') {
                    images = images.replace(',' + id, '');
                    images = images.replace(id + ',', '');
                }

                t.closest('.st-field').find('.upload_value').val(images).trigger('change');
                parent.remove();
            });

            $(document).on('click.st_gallery.data-api', '.add-attachments', function (e) {
                e.preventDefault();
                ST_Gallery.open2($(this))
            });
        },
        initDatePicker: function (el) {
            var base = this;
            $('.flatpickr', el).each(function () {
                var t = $(this);
                t.flatpickr({
                    enableTime: !1,
                    dateFormat: t.attr('date-format'),
                    minDate: moment().format('YYYY-MM-DD')
                });
            });
        },
        initcustomPrice: function (el) {
            var base = this;
            var parent = $('.st-field-custom-price', el);
            $('.add-price', parent).click(function (ev) {
                ev.preventDefault();
                let data = {
                    startDate: $('.start-date', parent).val(),
                    endDate: $('.end-date', parent).val(),
                    customPrice: $('.custom-price', parent).val(),
                    postID: $(this).data('post-id'),
                    action: 'st_dashboard_add_custom_price',
                    security: st_params.security
                };
                $('.st-loader', parent).show();
                $.post(st_params.ajaxurl, data, function (respon) {
                    if (typeof respon == 'object') {
                        base.alert(respon);
                        if (respon.html) {
                            $('.price-render', parent).html(respon.html);
                        }
                    }
                    $('.st-loader', parent).hide();
                }, 'json');

            });
            parent.on('click', '.delete', function (ev) {
                ev.preventDefault();
                var t = $(this);
                $.confirm({
                    animation: 'none',
                    title: 'Confirm!',
                    content: 'Are you sure to delete this item?',
                    buttons: {
                        ok: {
                            text: "Delete it!",
                            btnClass: 'btn-primary',
                            action: function () {
                                let data = {
                                    startDate: t.data('start'),
                                    endDate: t.data('end'),
                                    postID: t.data('post-id'),
                                    action: 'st_dashboard_delete_custom_price',
                                    security: st_params.security
                                };
                                $('.st-loader', parent).show();
                                $.post(st_params.ajaxurl, data, function (respon) {
                                    if (typeof respon == 'object') {
                                        base.alert(respon);
                                        if (respon.html) {
                                            $('.price-render', parent).html(respon.html);
                                        }
                                    }
                                    $('.st-loader', parent).hide();
                                }, 'json');
                            }
                        },
                        cancel: function () {

                        }
                    }
                });

            })
        },
        initCalendar: function (el) {
            var base = this;

            $('.st-field-calendar', el).each(function () {
                var t = $(this);
                var container = $('.st-availability', t);
                var calendar = $('.calendar_input', container);
                var options = {
                    parentEl: container,
                    showCalendar: true,
                    alwaysShowCalendars: true,
                    singleDatePicker: true,
                    sameDate: true,
                    autoApply: true,
                    disabledPast: true,
                    dateFormat: 'DD/MM/YYYY',
                    enableLoading: true,
                    showEventTooltip: false,
                    classNotAvailable: ['disabled', 'off'],
                    disableHightLight: true,
                    fetchEvents: function (start, end, el, callback) {
                        var events = [];
                        if (el.flag_get_events) {
                            return false;
                        }
                        el.flag_get_events = true;
                        el.container.find('.st-loader').show();
                        var data = {
                            action: calendar.data('action'),
                            start: start.format('YYYY-MM-DD'),
                            end: end.format('YYYY-MM-DD'),
                            post_id: calendar.data('id'),
                            _s: st_params.security
                        };
                        $.post(st_params.ajaxurl, data, function (respon) {
                            if (typeof respon === 'object') {
                                if (typeof respon.events === 'object') {
                                    events = respon.events;
                                }
                            } else {
                                console.log('Can not get data');
                            }
                            callback(events, el);
                            el.flag_get_events = false;
                            el.container.find('.st-loader').hide();
                        }, 'json');
                    }
                };
                if (typeof locale_daterangepicker == 'object') {
                    options.locale = locale_daterangepicker;
                }
                calendar.daterangepicker(options, function (start, end, label, elmdate, el) {
                    if (label == 'clicked_date') {
                        let data = {
                            post_id: calendar.data('id'),
                            start: start.format('YYYY-MM-DD'),
                            end: end.format('YYYY-MM-DD'),
                            action: 'st_dashboard_set_available_calendar',
                            security: st_params.security
                        };
                        el.container.find('.st-loader').show();
                        $.post(st_params.ajaxurl, data, function (respon) {
                            if (typeof respon == 'object') {
                                base.alert(respon);
                            }
                            el.updateCalendars();
                            el.container.find('.st-loader').hide();
                        }, 'json');
                    }
                });
                var dp = calendar.data('daterangepicker');
                dp.show();
            });
        },
        initMaxLength: function (el) {
            var base = this;
            $('input[maxlength]', el).maxlength({
                alwaysShow: !0,
                placement: 'bottom-right'
            });
            $('textarea[maxlength]', el).maxlength({
                alwaysShow: !0,
                placement: 'bottom-right'
            });
        },
        initValidation: function (el, addEvent) {
            var base = this;
            $('.has-validation', el).each(function () {
                let _id = $(this).attr('id'),
                    validation = $(this).attr('data-validation');
                bootstrapValidate('#' + _id, validation, function (isValid) {
                    if (isValid) {
                        if (typeof st_params.isValidated[_id] != 'undefined') {
                            delete st_params.isValidated[_id];
                        }
                    } else {
                        st_params.isValidated[_id] = 1;
                    }
                });
                if (addEvent) {
                    if ($(this).val() == '') {
                        $(this).trigger('focus').trigger('blur');
                    }
                }
            });
        },
        initListItem: function (el) {
            var base = this;
            base.initListItemSort();
            base.initBindListItem(el);
            $('body').on('click', '.add-list-item', function (ev) {
                ev.preventDefault();
                var t = $(this),
                    parent = t.closest('.st-field-list-item');
                t.prop('disable', 'disabled');
                var data = {
                    action: 'st_add_list_item',
                    items: parent.data('items'),
                    id: parent.data('id'),
                    security: st_params.security
                };

                $.post(st_params.ajaxurl, data, function (respon) {
                    if (typeof respon == 'object') {
                        var container = $('.st-render', parent);
                        container.find('.st-list-item .render').removeClass('toggleFlex');
                        var el = $(respon.html).appendTo(container).find('.render').addClass('toggleFlex');

                        base.initListItemSort('refresh', el);
                        base.initBindListItem();
                        base.initInput(el);
                        base.initGoogleMap(el);
                        base.initGoogleFonts(el);
                        base.initSelect(el);
                        base.initToolTip(el);
                        base.initGallery(el);
                        base.initImage(el);
                        base.initImages(el);
                        base.initcustomPrice(el);
                        base.initCalendar(el);
                        base.initDatePicker(el);
                        base.initMaxLength(el);
                        base.initValidation(el, false);
                    }
                    t.prop('disable', false);
                }, 'json');
            });
            $('body').on('click', '.st-list-items li .edit', function (ev) {
                ev.preventDefault();
                var t = $(this);
                t.closest('.st-list-item').find('.render').toggleClass('toggleFlex');
            });
            $('body').on('click', '.st-list-items li .delete', function (ev) {
                ev.preventDefault();
                var t = $(this);
                $.confirm({
                    animation: 'none',
                    title: 'Confirm!',
                    content: 'Are you sure to delete this item?',
                    buttons: {
                        ok: {
                            text: "Delete it!",
                            btnClass: 'btn-primary',
                            action: function () {
                                t.closest('.st-list-item').remove();
                                base.initListItemSort('refresh');
                            }
                        },
                        cancel: function () {

                        }
                    }
                });
            });
        },
        initListItemSort: function (refresh, el) {
            var base = this;
            if (refresh == 'refresh') {
                $(".st-list-items", el).sortable('refresh');
            } else {
                if ($(".st-list-items", el).length) {
                    $(".st-list-items", el).sortable();
                }
            }
        },
        initBindListItem: function (el) {
            el = el || $('body');
            $('.st-field-list-item', el).each(function () {
                var container = $(this);
                var hasBinded = container.data('bind-from') || false;
                if (hasBinded) {
                    $('.st-list-item', el).each(function () {
                        var parent = $(this);
                        var element = $('[id^="' + hasBinded + '"]', parent);
                        element.on('change keyup', function (ev) {
                            $('.st-list-item-heading .htext', parent).html($(this).val());
                        }).keyup().change();
                        $('.st-list-item-heading .htext', parent).html(element.val());
                    });
                }
            });

        },
        initColorPicker: function (el) {

        },
        doSubmit: function (form, button, isSubmit) {
            var base = this;
            if (form.length) {
                if (typeof tinyMCE != 'undefined') {
                    tinyMCE.triggerSave();
                }
                let data = form.serializeArray();
                data.push({
                    name: 'security',
                    value: st_params.security
                });
                base.initValidation(form, true);
                if (Object.size(st_params.isValidated)) {
                    $("html, body").animate({scrollTop: $('.has-validation.is-invalid', form).first().offset().top}, 500);
                } else {
                    $('.st-loader.st-loader-submit', form).show();
                    $.post(st_params.ajaxurl, data, function (respon) {
                        if (typeof respon == 'object') {
                            base.alert(respon);
                            if (respon.status == 1) {
                                if (isSubmit !== 'onlySubmit') {
                                    if (base.nextTab.length) {
                                        base.gotoTab(base.nextTab);
                                    } else {
                                        let tab = button.data('current-tab');
                                        if ($(tab).length) {
                                            let nextTab = $(tab).parent().next('.nav-item').find('.nav-link');
                                            base.gotoTab(nextTab);
                                            $("html, body").animate({scrollTop: nextTab.offset().top}, 500);
                                        }
                                    }
                                    if (isSubmit === 'submit') {
                                        base.prevTab = base.nextTab;
                                        base.nextTab = base.nextTab.parent().next('.nav-item').find('.nav-link');
                                        if (base.nextTab.length) {
                                            $("html, body").animate({scrollTop: base.nextTab.offset().top - 50}, 500);
                                        }
                                    }
                                    if (isSubmit === 'finish' && respon.redirect) {
                                        window.location.href = respon.redirect;
                                    }
                                }
                            }
                        }
                        $('.st-loader', form).hide();
                    }, 'json');
                }
            }
        },
        initSubmit: function (el) {
            var base = this;
            $('.btn-submit-form.st-metabox-save-continue', el).click(function (ev) {
                ev.preventDefault();
                var t = $(this);
                base.prevTab = $(t.data('current-tab'));
                base.nextTab = base.prevTab.parent('li').next().find('.nav-link');
                var form = $(t.data('form'));
                base.doSubmit(form, t, 'submit');
            });
            $('.btn-submit-form.st-metabox-save', el).click(function (ev) {
                ev.preventDefault();
                var t = $(this);
                base.prevTab = $(t.data('current-tab'));
                base.nextTab = base.prevTab;
                var form = $(t.data('form'));
                base.doSubmit(form, t, 'onSubmit');
            });
            $('.btn-submit-form.st-metabox-save-finish', el).click(function (ev) {
                ev.preventDefault();
                var t = $(this);
                base.prevTab = $(t.data('current-tab'));
                base.nextTab = base.prevTab;
                var form = $(t.data('form'));
                base.doSubmit(form, t, 'finish');
            });

            $('.st-metabox-prev', el).click(function (ev) {
                ev.preventDefault();
                var t = $(this),
                    currentTab = t.data('current-tab');
                if ($(currentTab).length) {
                    let prevTab = $(currentTab).parent().prev('.nav-item').find('.nav-link');
                    prevTab.click();
                }
            });
        },
        conditionObjects: function () {
            return 'select, input[type="radio"]:checked, input[type="text"], input[type="hidden"], input[type="number"], input[type="checkbox"]';
        },
        initConditions: function (el) {
            var base = this;
            var delay = (function () {
                var timer = 0;
                return function (callback, ms) {
                    clearTimeout(timer);
                    timer = setTimeout(callback, ms);
                };
            })();
            $('.st-field[id^="st-field-"]', el).on('change.conditionals, keyup.conditionals', base.conditionObjects(), function (e) {
                if (e.type === 'keyup') {
                    delay(function () {
                        base.parseCondition(el);
                    }, 200);
                } else {
                    base.parseCondition(el);
                }
            });
            base.parseCondition(el);
        },
        parseCondition: function (el) {
            var base = this;
            $('.st-field[id^="st-field-"][data-condition]', el).each(function () {
                var passed;
                var conditions = base.matchConditions($(this).data('condition'));
                var operator = ($(this).data('operator') || 'and').toLowerCase();
                var unique = $(this).data('unique') || '';
                if (conditions.length > 0) {
                    $.each(conditions, function (index, condition) {
                        var target = $('#st-field-' + condition.check + unique, el);
                        var targetEl = !!target.length && target.find(base.conditionObjects()).first();

                        if (!target.length || (!targetEl.length && condition.value.toString() != '')) {
                            return;
                        }
                        var v1 = targetEl.length ? targetEl.val().toString() : '';
                        if (targetEl[0].type && targetEl[0].type == 'checkbox') {
                            v1 = [];
                            target.find(base.conditionObjects()).each(function () {
                                if ($(this).is(':checked')) {
                                    v1.push($(this).val().toString());
                                }
                            });
                        }
                        var v2 = condition.value.toString();
                        var result;
                        switch (condition.rule) {
                            case 'less_than':
                                result = (parseInt(v1) < parseInt(v2));
                                break;
                            case 'less_than_or_equal_to':
                                result = (parseInt(v1) <= parseInt(v2));
                                break;
                            case 'greater_than':
                                result = (parseInt(v1) > parseInt(v2));
                                break;
                            case 'greater_than_or_equal_to':
                                result = (parseInt(v1) >= parseInt(v2));
                                break;
                            case 'contains':
                                result = (v1.indexOf(v2) !== -1 ? true : false);
                                break;
                            case 'is':
                                if (typeof v1 != 'object') {
                                    result = (v2 == v1);
                                } else {
                                    result = ($.inArray(v2, v1) != -1) ? true : false;
                                }
                                break;
                            case 'not':
                                if (typeof v1 != 'object') {
                                    result = (v2 != v1);
                                } else {
                                    result = ($.inArray(v2, v1) == -1) ? true : false;
                                }
                                break;
                        }

                        if ('undefined' == typeof passed) {
                            passed = result;
                        }
                        switch (operator) {
                            case 'or':
                                passed = (passed || result);
                                break;
                            case 'and':
                            default:
                                passed = (passed && result);
                                break;
                        }
                    });

                    if (passed) {
                        $(this).animate({opacity: 'show', height: 'show'}, 200);
                    } else {
                        $(this).animate({opacity: 'hide', height: 'hide'}, 200);
                    }
                }

            });
        },
        matchConditions: function (condition) {
            var match;
            var regex = /(.+?):(is|not|contains|less_than|less_than_or_equal_to|greater_than|greater_than_or_equal_to)\((.*?)\),?/g;
            var conditions = [];

            while (match = regex.exec(condition)) {
                conditions.push({
                    'check': match[1],
                    'rule': match[2],
                    'value': match[3] || ''
                });
            }

            return conditions;
        },
        alert: function (data) {
            if (data.status == 1) {
                $.toast({
                    heading: data.heading,
                    text: data.message,
                    showHideTransition: 'fade',
                    icon: 'success',
                    loader: 0,
                    position: 'bottom-right',
                });
            } else {
                $.toast({
                    heading: data.heading,
                    text: data.message,
                    showHideTransition: 'fade',
                    icon: 'error',
                    loader: 0,
                    position: 'bottom-right',
                });
            }

        },
    };
    ST_Options.init();

    var STAlert = function (data) {
        if (data.status == 1) {
            $.toast({
                heading: data.heading,
                text: data.message,
                showHideTransition: 'fade',
                icon: 'success',
                loader: 0,
                position: 'bottom-right',
            });
        } else {
            $.toast({
                heading: data.heading,
                text: data.message,
                showHideTransition: 'fade',
                icon: 'error',
                loader: 0,
                position: 'bottom-right',
            });
        }
    };
    $('.form-global-action').each(function () {
        var t = $(this),
            hasValidation = t.hasClass('has-validation');
        t.submit(function (ev) {
            ev.preventDefault();
            if (hasValidation) {
                $('.has-validation', t).each(function () {
                    let _id = $(this).attr('id'),
                        validation = $(this).attr('data-validation');
                    bootstrapValidate('#' + _id, validation, function (isValid) {
                        if (isValid) {
                            if (typeof st_params.isValidated[_id] != 'undefined') {
                                delete st_params.isValidated[_id];
                            }
                        } else {
                            st_params.isValidated[_id] = 1;
                        }
                    });
                    $(this).trigger('focus').trigger('blur');
                });
            }
            if (Object.size(st_params.isValidated)) {
                return false;
            }
            var data = t.serializeArray();
            data.push({
                name: '_s',
                value: st_params.security
            });

            $('.st-loader', t).show();
            $.post(st_params.ajaxurl, data, function (respon) {
                if (typeof respon == 'object') {
                    STAlert(respon);
                    t.trigger('st_global_action_done');
                    if (respon.redirect) {
                        window.location.href = respon.redirect;
                    }
                }
                $('.st-loader', t).hide();
            }, 'json');
        });
    });

    $('.link-global-action').each(function () {
        var t = $(this);
        t.click(function (ev) {
            ev.preventDefault();
            var data = t.data('params');
            data['action'] = t.data('action');
            data['_s'] = st_params.security;

            $.post(st_params.ajaxurl, data, function (respon) {
                if (typeof respon == 'object') {
                    STAlert(respon);
                    t.trigger('st_link_global_action_done', [respon]);
                    if (respon.reload) {
                        window.location.reload();
                    }
                    if (respon.redirect) {
                        window.location.href = respon.redirect;
                    }
                }
            }, 'json');
        });
    });

    $('.checkbox-global-action').each(function () {
        var t = $(this);
        t.change(function (ev) {
            ev.preventDefault();
            var data = t.data('params');
            data['action'] = t.data('action');
            data['val'] = (t.is(':checked')) ? t.val() : '';
            data['_s'] = st_params.security;

            $.post(st_params.ajaxurl, data, function (respon) {
                if (typeof respon == 'object') {
                    STAlert(respon);
                    t.trigger('st_checkbox_global_action_done', [respon]);
                }
            }, 'json');
        });
    });

    $('.link-global-action.link-delete-service').on('st_link_global_action_done', function (e, respon) {
        var t = $(this);
        if (respon.status === 1) {
            var parent = t.closest(t.data('parent'));
            parent.addClass('deleting-effect');
            parent.one(whichTransitionEvent(), function () {
                parent.hide();
            });
        }
    });

    $('.link-global-action.link-delete-coupon').on('st_link_global_action_done', function (e, respon) {
        var t = $(this);
        if (respon.status === 1) {
            var parent = t.closest(t.data('parent'));
            parent.addClass('deleting-effect');
            parent.one(whichTransitionEvent(), function () {
                parent.hide();
            });
        }
    });

    $('#form-change-information').on('st_global_action_done', function (e) {
        var t = $(this),
            preview = $('.profile-preview');
        $('.st-loader', preview).show();
        var data = $('form', preview).serializeArray();
        data.push({
            name: '_s',
            value: st_params.security
        });

        $.post(st_params.ajaxurl, data, function (respon) {
            if (typeof respon == 'object') {
                if (respon.status === 1) {
                    $('.information-validation').html(respon.html);
                } else {
                    STAlert(respon);
                }
            }
            $('.st-loader', preview).hide();
        }, 'json');
    });

    $('.profile-preview .change-avatar').click(function (ev) {
        ev.preventDefault();
        var t = $(this),
            parent = t.closest('.profile-preview');
        if (window.wp && wp.media) {
            window.st_media_frame = window.st_media_frame || new wp.media.view.MediaFrame.Select({
                title: t.attr('title'),
                button: {
                    text: t.data('text')
                },
                multiple: false
            });
            window.st_media_frame.on('select', function () {
                var attachment = window.st_media_frame.state().get('selection').first(),
                    attachment_id = attachment.attributes.id,
                    mime = attachment.attributes.mime,
                    regex = /^image\/(?:jpe?g|png)$/i;
                if (mime.match(regex)) {
                    $('.st-loader', parent).show();
                    var data = $('form', parent).serializeArray();
                    data[2].value = 'st_dashboard_change_avatar';
                    data.push({
                        name: '_s',
                        value: st_params.security
                    }, {
                        name: 'attachment_id',
                        value: attachment_id
                    });
                    $.post(st_params.ajaxurl, data, function (respon) {
                        if (typeof respon == 'object') {
                            if (respon.status === 1) {
                                $('img.avatar', parent).attr('src', respon.avatar);
                            } else {
                                STAlert(respon);
                            }
                        }
                        $('.st-loader', parent).hide();
                    }, 'json');
                } else {
                    STAlert({
                        status: 0,
                        messsage: 'The image format is invalid'
                    });
                }
                window.st_media_frame.off('select');
            }).open();
        }
    });
    $('select[name="show_revenue_by"]').change(function () {
        $(this).closest('form').submit();
    });

    $('.select-action-change').change(function () {
        $(this).closest('form').submit();
    });

    $('#modal-show-booking-invoice').on('show.bs.modal', function (e) {
        var t = $(this),
            loader = $('.st-loader', t),
            render = $('.render', t);

        render.empty();
        loader.show();

        var data = $(e.relatedTarget).data('data');
        data['action'] = 'st_dashboard_view_booking_detail';
        data['_s'] = st_params.security;

        $.post(st_params.ajaxurl, data, function (respon) {
            if (typeof respon == 'object') {
                if (respon.status == 0) {
                    STAlert(respon);
                } else {
                    render.html(respon.message);
                }
            }
            loader.hide()
        }, 'json');
    });

    $('#modal-show-partner-detail').on('show.bs.modal', function (e) {
        var t = $(this),
            loader = $('.st-loader', t),
            render = $('.render', t);

        render.empty();
        loader.show();
        var data = $(e.relatedTarget).data('data');
        data['action'] = 'st_dashboard_view_partner_detail';
        data['_s'] = st_params.security;

        $.post(st_params.ajaxurl, data, function (respon) {
            if (typeof respon == 'object') {
                if (respon.status == 0) {
                    STAlert(respon);
                } else {
                    render.html(respon.message);
                }
            }
            loader.hide()
        }, 'json');
    });

    $('.dropdown-message-box').on('shown.bs.dropdown', function () {
        $.fn.niceScroll && $('.dropdown-message-box .message-area').niceScroll();
    });

    $('.dropdown-message-box').on('click', '.message-item', function (ev) {
        ev.stopPropagation();
    });

    $('.message-delete, .link-delete-message').on('st_link_global_action_done', function (ev) {
        var t = $(this),
            parent = t.closest(t.data('parent'));
        parent.addClass('deleting-effect');
        parent.one(whichTransitionEvent(), function () {
            parent.hide();
        });
        setTimeout(function () {
            $.fn.niceScroll && $('.dropdown-message-box .message-area').niceScroll();
        }, 2000);
    });

    $('.delete-wishlist').on('st_link_global_action_done', function (ev, respon) {
        if (respon.status == 1) {

            $(this).closest('.col').remove();
        }
    });

    $(document).on('click', '.btn_add_wishlist', function (event) {
        event.preventDefault();
        var t = $(this);
        $.ajax({
            url: st_params.ajaxurl,
            type: "POST",
            data: {
                _s: st_params.security,
                action: "st_add_wishlist",
                data_id: t.data('id'),
                data_type: t.data('type')
            },
            dataType: "json",
            beforeSend: function () {
                t.addClass('loading');
            }
        }).done(function (res) {
            if (res.status) {
                t.removeClass('added').addClass(res.class);
            }
            t.removeClass('loading');
        })
    });

});