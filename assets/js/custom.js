(function ($) {
    'use strict';
    var xhr = null;
    var body = $('body');

    jQuery(window).on('elementor/frontend/init', function () {
        elementorFrontend.hooks.addAction('frontend/element_ready/stt_list_of_blog.default', function () {
            jQuery('.stt-carousel').flickity({
                "imagesLoaded": true,
                "wrapAround": true,
                // "autoPlay": 3000
            });


        });
        elementorFrontend.hooks.addAction('frontend/element_ready/stt_gallery_destination.default', function () {
            jQuery('.stt-gallery').each(function () {
                let t = jQuery(this);
                var owl = jQuery('.owl-carousel', t).owlCarousel({
                    loop: true,
                    autoplay: true,
                    margin: 1,
                    responsive: {
                        0: {
                            items: 1
                        },
                        576: {
                            items: 2
                        },
                        768: {
                            items: 3
                        }
                    }
                });
            })

        });
    });
    jQuery(window).on('elementor/frontend/init', function () {
        elementorFrontend.hooks.addAction('frontend/element_ready/stt_list_of_destination.default', function () {
            jQuery('.stt-list-destination').each(function () {
                let t = jQuery(this);
                var owl = jQuery('.owl-carousel', t).owlCarousel({
                    loop: true,
                    autoplay: true,
                    margin: 1,
                    responsive: {
                        0: {
                            items: 2
                        },
                        576: {
                            items: 2
                        },
                        768: {
                            items: 3
                        }
                    }
                });
                jQuery('.Destination-Next', t).click(function (ev) {
                    ev.preventDefault();
                    owl.trigger('next.owl.carousel');
                })
            })
        });

        elementorFrontend.hooks.addAction('frontend/element_ready/stt_map.default', function () {
            //Map element
            jQuery('.stt-map-element').each(function () {
                var parent = jQuery(this),
                    mapEl = jQuery('.google-map', parent);
                var style = mapEl.data('style');


                var data = {
                    center: {
                        lat: parseFloat(mapEl.data().lat),
                        lng: parseFloat(mapEl.data().lng)
                    },
                    zoom: mapEl.data('zoom'),
                    disableDefaultUI: mapEl.data().disablecontrol,
                };
                var showcustomcontrol = mapEl.data('showcustomcontrol');
                var map = new google.maps.Map(mapEl.get(0), data);
                new google.maps.Marker({
                    position: new google.maps.LatLng(mapEl.data().lat, mapEl.data().lng),
                    icon: mapEl.data().icon,
                    map: map,

                });

                var showLabel = mapEl.data('showlabel');
                if (showLabel === false) {
                    var customStyled = [
                        {
                            featureType: "all",
                            elementType: "labels",
                            stylers: [
                                {visibility: "off"}
                            ]
                        }
                    ];

                    map.set('styles', customStyled);
                }

                var mapStyle = mapEl.data('style');
                if (mapStyle !== 'custom_style') {
                    map.setMapTypeId(mapStyle);
                } else {
                    var customMap = mapEl.data('customstyle');
                    if (customMap != '') {
                        map.set('styles', customMap);
                    } else {
                        map.setMapTypeId('roadmap');
                    }
                }

                if (showcustomcontrol) {
                    customControlGoogleMap(mapEl, map);
                }
            });

            /* Map with service */
            if (jQuery('#st-search-map').length) {
                reloadMapData(jQuery('#st-search-map').data('jsonmap'));
            }
        });
    });


    window.customControlGoogleMap = function (mapEl, map) {
        //==== Top Right area
        var topRightArea = document.createElement('div');
        topRightArea.className = 'google-control-top-right-area';
        var controlFullScreen = document.createElement('div');
        controlFullScreen.className = 'google-control-fullscreen google-custom-control';
        controlFullScreen.innerHTML = '<img src="' + st_list_map_params.icon_full_screen + '" alt="Full Screen"/>';
        topRightArea.appendChild(controlFullScreen);
        var controlCloseFullScreen = document.createElement('div');
        controlCloseFullScreen.className = 'google-control-closefullscreen google-custom-control hide';
        controlCloseFullScreen.innerHTML = '<img src="' + st_list_map_params.icon_close + '" alt="Full Screen"/>';
        topRightArea.appendChild(controlCloseFullScreen);
        var controlMyLocation = document.createElement('div');
        controlMyLocation.className = 'google-control-mylocation google-custom-control';
        controlMyLocation.innerHTML = '<img src="' + st_list_map_params.icon_my_location + '" alt="Full Screen"/>';
        topRightArea.appendChild(controlMyLocation);
        var controlStyles = document.createElement('div');
        controlStyles.className = 'google-control-styles google-custom-control';
        controlStyles.innerHTML = '<img src="' + st_list_map_params.icon_my_style + '" alt="Full Screen"/><div class="google-control-dropdown"><div class="item">Silver</div><div class="item">Retro</div><div class="item">Dark</div><div class="item">Night</div><div class="item">Aubergine</div></div>';
        topRightArea.appendChild(controlStyles);
        //==== Bottom Right area
        var bottomRightArea = document.createElement('div');
        bottomRightArea.className = 'google-control-bottom-right-area';
        var controlZoomIn = document.createElement('div');
        controlZoomIn.className = 'google-control-zoomin google-custom-control';
        controlZoomIn.innerHTML = '<img src="' + st_list_map_params.icon_zoom_in + '" alt="Full Screen"/>';
        bottomRightArea.appendChild(controlZoomIn);
        var controlZoomOut = document.createElement('div');
        controlZoomOut.className = 'google-control-zoomout google-custom-control';
        controlZoomOut.innerHTML = '<img src="' + st_list_map_params.icon_zoom_out + '" alt="Full Screen"/>';
        bottomRightArea.appendChild(controlZoomOut);
        map.controls[google.maps.ControlPosition.RIGHT_TOP].push(topRightArea);
        map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(bottomRightArea);
        controlFullScreen.addEventListener('click', function () {
            controlFullScreen.classList.add('hide');
            controlCloseFullScreen.classList.remove('hide');
            var element = map.getDiv();
            if (element.requestFullscreen) {
                element.requestFullscreen();
            }
            if (element.webkitRequestFullScreen) {
                element.webkitRequestFullScreen();
            }
            if (element.mozRequestFullScreen) {
                element.mozRequestFullScreen();
            }
        });
        controlCloseFullScreen.addEventListener('click', function () {
            controlFullScreen.classList.remove('hide');
            controlCloseFullScreen.classList.add('hide');
            if (document.exitFullscreen) document.exitFullscreen();
            else if (document.webkitExitFullscreen) document.webkitExitFullscreen();
            else if (document.mozCancelFullScreen) document.mozCancelFullScreen();
            else if (document.msExitFullscreen) document.msExitFullscreen();
        });
        controlMyLocation.addEventListener('click', function () {

            var options = {
                enableHighAccuracy: true,
                timeout: 5000,
                maximumAge: 0
            };

            function success(pos) {
                var crd = pos.coords;

                console.log('Your current position is:');
                console.log(`Latitude : ${crd.latitude}`);
                console.log(`Longitude: ${crd.longitude}`);
                console.log(`More or less ${crd.accuracy} meters.`);
            }

            function error(err) {
                console.warn(`ERROR(${err.code}): ${err.message}`);
            }

            navigator.geolocation.getCurrentPosition(success, error, options);

            if (navigator.geolocation) navigator.geolocation.getCurrentPosition(function (pos) {

                var latlng = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);
                map.setCenter(latlng);
                new google.maps.Marker({
                    position: latlng,
                    icon: mapEl.data().icon,
                    map: map
                });
            }, function (error) {
                console.log('Can not get your Location');
            });
        });
        controlZoomIn.addEventListener('click', function () {
            var current = map.getZoom();
            map.setZoom(current + 1);
        });
        controlZoomOut.addEventListener('click', function () {
            var current = map.getZoom();
            map.setZoom(current - 1);
        });
        controlStyles.addEventListener('click', function () {
            controlStyles.querySelector('.google-control-dropdown').classList.toggle('show');
        });
        var dropdownStyles = controlStyles.querySelector('.google-control-dropdown');
        var items = dropdownStyles.querySelectorAll('.item');
        for (var i = 0; i < items.length; i++) {
            items[i].addEventListener('click', function (e) {
                var style = this.textContent.toLowerCase();
                if (mapStyles[style]) {
                    map.setOptions({styles: mapStyles[style]});
                }
            });
        }
    };
    $(document).ready(function () {
        var orderBy = 'lastest_post';
        layoutBlogFilterCallback();
        //Video Popup
        $(".stt-video").each(function () {

            $(this).magnificPopup({
                type: 'iframe'
            })
        });

        /* Sort selection */
        $('.sort-by .sort-dropdown li').click(function (e) {

            var t = $(this),
                parent = t.closest('.sort-by'),
                render = $('.value-change', parent),
                inputSort = $('input[name="sort_by"]', parent);

            $('li', parent).removeClass('active');
            t.addClass('active');
            render.text(t.text());
            inputSort.val(t.data('value')).trigger('change');
        });

        /* Sort change */

        $('.stt-toolbar .sort-by input[name="sort_by"]').change(function () {
            if (orderBy !== $(this).val()) {
                orderBy = $(this).val();
                layoutBlogFilterCallback();

            }
        });
        var filter_paged = '';
        $(document).on('click', '.stt-list-of-blog.style4 .stt-pagination ul.page-numbers li a.page-numbers', function (e) {
            e.preventDefault();
            var t = $(this);
            var applyPage = '';
            var currentPage = t.closest('.stt-pagination').find('.page-numbers.current').text();
            if (t.hasClass('next')) {
                applyPage = parseInt(currentPage) + 1;
            } else if (t.hasClass('prev')) {
                applyPage = parseInt(currentPage) - 1;
            } else {
                applyPage = parseInt(t.text());
            }
            filter_paged = applyPage;
            layoutBlogFilterCallback();

        });
        /*Scoll to list blog of blog page*/
        $(document).on('click', '.stt-list-of-blog.style4 .stt-pagination ul.page-numbers li a.page-numbers', function (e) {
            window.scrollTo({
                top: $('.stt-list-of-blog.style4').offset.top - 50,
                behavior: 'smooth'
            });
        });
        /*slider single blog*/
        jQuery('.blog-slider').each(function () {
            let t = jQuery(this);
            var owl = jQuery('.owl-carousel', t).owlCarousel({
                loop: true,
                autoplay: true,
                margin: 1,
                responsive: {
                    0: {
                        items: 1
                    }
                }
            });
            jQuery('.Blog-Next', t).click(function (ev) {
                ev.preventDefault();
                owl.trigger('next.owl.carousel');
            })
        });

        function layoutBlogFilterCallback() {

            if (xhr != null) {
                xhr.abort();
            }
            var parent = $('.stt-list-of-blog.style4');
            var loader = $('.stt-loader', parent);

            xhr = $.ajax({
                url: st_params.ajaxurl,
                type: "POST",
                data: {
                    orderby: orderBy,
                    action: 'st_filter_layout_blog',
                    _s: st_params.security,
                    filter_paged: filter_paged,
                },
                dataType: "json",
                beforeSend: function () {
                    loader.fadeIn();
                },
                complete: function () {
                    loader.fadeOut();

                }
            }).done(function (res) {
                if (res.status) {
                    $('.stt-list-blog', parent).html(res.html);
                    $('.blog-pagination', parent).html(res.pagination);
                }
            })
        }

        var totalTaxLoad = 0;
        $(document).on('click', '.des-load-more .stt-load-more', function (e) {
            e.preventDefault();
            var t = $(this);
            var offset = t.data('offset');
            var paged = $("input[name=stt-number-des]").val();
            var parent = $('.stt-list-of-destination.style2');
            var loader = $('.stt-loader', parent);

            if (xhr != null) {
                xhr.abort();
            }
            xhr = $.ajax({
                url: st_params.ajaxurl,
                type: "POST",
                data: {
                    paged: paged,
                    offset: offset,
                    action: 'st_load_more',
                    count: totalTaxLoad,
                    _s: st_params.security,
                },
                dataType: "json",
                beforeSend: function () {
                    loader.fadeIn();
                },
                complete: function () {
                    loader.fadeOut();
                }
            }).done(function (res) {
                if (res.status) {
                    $('.stt-list-destination .row', parent).append(res.html);
                    $('.des-load-more .stt-load-more').data('offset', parseInt(offset) + parseInt(paged));
                    $('.des-load-more .stt-load-more').attr('data-offset', parseInt(offset) + parseInt(paged));
                    totalTaxLoad = res.count;
                    if (res.count <= $('.stt-list-destination .destination-item').length) {
                        $('.des-load-more .stt-load-more').hide();
                    }

                }
            })
        });
        // choose item tab in destination
        $(document).on('click', '.stt-tabs-infor-des .stt-title .stt-title-item ', function (e) {
            var parent = $(this).closest('.stt-tabs-infor-des');
            $('.stt-title-item', parent).removeClass('active');
            $(this).addClass('active');
        });
        // daterangepicker for form search
        $('input[name="date"]').daterangepicker(
            {
                onlyShowCurrentMonth: true,
                showCalendar: false,
                alwaysShowCalendars: false,
                singleDatePicker: false,
                sameDate: false,
                autoApply: true,
                disabledPast: true,
                dateFormat: 'DD/MM/YYYY',
                enableLoading: true,
                showEventTooltip: true,
                customClass: 'stt-search-calendar',
                classNotAvailable: ['disabled', 'off'],
                disableHightLight: true,
                disabledDates: [],
                locale: {
                    "firstDay": 1
                },
            },
            function (start, end, label) {
                console.log("Callback has been called!");
                $('#reportrange').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
                $('#start').val(start.format('DD/MM/YYYY'));
                $('#end').val(end.format('DD/MM/YYYY'));
                $('#date').val(start.format('DD/MM/YYYY hh:mm') + ' am- ' + end.format('DD/MM/YYYY hh:m$endm') + ' pm');
            }
        );

        //Guest for form search
        $('.stt-number').each(function () {
            var t = $(this);
            var min = t.data('min');
            var max = t.data('max');
            t.find('.control').on('click', function () {
                var $button = $(this);
                numberButtonFunc($button);
            });

            function numberButtonFunc($button) {
                var oldValue = parseInt($button.parent().find(".value").text());
                var container = $button.closest('.stt-number');
                var total = 0;
                $('.value', container).each(function () {
                    var currentValue = parseInt($(this).text());
                    total += currentValue;
                });
                var newVal = oldValue;
                if ($button.hasClass('add')) {
                    if (total < max) {
                        if (oldValue < max) {
                            newVal = parseFloat(oldValue) + 1;
                        } else {
                            newVal = max;
                        }
                    }
                } else {
                    if (oldValue > min) {
                        newVal = parseFloat(oldValue) - 1;
                    } else {
                        newVal = min;
                    }
                }
                $button.parent().find(".value").text(newVal);
                $('input', t).val(newVal).trigger('change');
                var adultNumber = $('input[name="number_adult"]', $('.stt-number')).val();
                console.log(adultNumber);
                var html = '';
                if (adultNumber < 2) {
                    html = adultNumber + ' ' + 'Adult';
                } else {
                    html = adultNumber + ' ' + 'Adults';
                }
                $('.form-guest-search .adult').html(html);

                var childNumber = $('input[name="number_child"]', $('.stt-number')).val();
                if (childNumber < 2) {
                    html = childNumber + ' ' + 'children';
                } else {
                    html = childNumber + ' ' + 'childrens';
                }
                $('.form-guest-search .children').html(html);

            }
        });

        $(document).on('click', '.form-guest-search .dropdown-menu', function (e) {
            e.stopPropagation();
        });
        // Toggle menu
        $(document).on('click', '.stt-header-content .toggle-menu', function () {
            var parent = $(this).closest('.stt-header-content');
            $('.stt-header-menu', parent).addClass('open');
        });
        $(document).on('click', '.stt-header-menu .back-menu', function (e) {
            var parent = $(this).closest('.stt-header-content');
            $('.stt-header-menu', parent).removeClass('open');
        });
        $(document).on('click', '.stt-header-menu ul .menu-item-has-children', function (e) {
            e.stopPropagation();
            var parent = $(this).closest('.stt-header-content');
            $('.sub-menu', parent).toggleClass('sub-menu-mobile');
        });


        setTimeout(function () {
            if($('.stt-blog .share-column').length){
                $(window).on('resize', function () {
                    let share = $('.stt-blog .share-column'),
                        headerOffset = $('.stt-header-content').offset(),
                        margin = 100,
                        condition=  50;
                    if(window.matchMedia("(min-width: 1400px)").matches){
                        margin = 100;
                    }else{
                        margin = 30;
                        condition = 30;
                    }
                    if (headerOffset.left - margin < condition) {
                        share.css({
                            'position': '',
                            'top': '',
                            'left': ''
                        }).addClass('inline');
                    } else {
                        share.css({
                            'position': 'fixed',
                            'top': $(window).height() / 2,
                            'left': headerOffset.left - margin - (share.width()/2)
                        }).removeClass('inline');
                    }

                }).resize();

                $(window).on('scroll', function(){
                    let share = $('.stt-blog .share-column'),
                        tag = $('.blog-tag');

                    if(!share.hasClass('inline')){
                        if((tag.offset().top - $(window).scrollTop() <= $(window).height() /2)){
                            share.css({
                                'top': tag.offset().top - $(window).scrollTop() - (share.width()/2)
                            })
                        }else{
                            share.css({
                                'top':  $(window).height() / 2,
                            })
                        }
                    }
                });
            }

        }, 100);
        setTimeout(function () {
            if($('.stt-des-infor .share-column').length){
                let share_w = $('.stt-des-infor .share-column').width()+5;
                $(window).on('resize',function () {
                    let share = $('.stt-des-infor .share-column'),
                        header = $('.stt-header-content'),
                        headerOffset = header.offset(),
                        margin = 70,
                        condition  = 50;
                    if(window.matchMedia("(min-width: 1400px)").matches){
                        margin = 70;
                    }else{
                        margin = 30;
                        condition = 30;
                    }

                    if( $(window).width() - (header.offset().left + header.width()) - margin < condition){
                        share.css({
                            'position': '',
                            'top': '',
                            'left': '',
                            'width': ''
                        }).addClass('inline');
                    }else {
                        share.css({
                            'position': 'fixed',
                            'top': $('.stt-des-infor').offset().top  + (share.width() / 2) + 10,
                            'left': headerOffset.left  + header.width() + margin -(share.width()/2),
                            'width': share_w
                        }).removeClass('inline');
                    }

                }).resize();

                $(window).on('scroll', function(){
                    let des = $('.stt-des-infor'),
                        share = $('.stt-des-infor .share-column');
                    if(!share.hasClass('inline')){
                        if( des.offset().top - $(window).scrollTop() <= 0){

                            if(($('.stt-list-of-blog').offset().top - $(window).scrollTop() <= $(window).height() /2)){
                                share.css({
                                    'top': $('.stt-list-of-blog').offset().top - $(window).scrollTop() + (share.width() / 2)
                                });
                            }else{
                                share.css({
                                    'top': $(window).height() / 2
                                });
                            }
                        }else{
                            share.css({
                                'top': des.offset().top - $(window).scrollTop() + (share.width() / 2) + 10
                            });
                        }
                    }

                }).scroll();
            }

        },100);

    });


})(jQuery);
