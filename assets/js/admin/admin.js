jQuery(document).ready(function($){
    'use strict';

    $('input[name="term_icon"]').each(function () {
        var t      = $(this),
            parent = t.parent();
        parent.css('position', 'relative');
        parent.append('<div class="st-icon-new-wrapper">' +
            '<input type="text" name="search" placeholder="Enter a minimum of 2 characters">' +
            '<div class="result">' +
            '<div class="render"></div>' +
            '<div class="loader-wrapper">' +
            '    <div class="lds-ripple">' +
            '        <div></div>' +
            '        <div></div>' +
            '    </div>' +
            '</div>' +
            '</div>' +
            '</div>');

        t.focus(function () {
            $('.st-icon-new-wrapper', parent).show();
            if(t.val() == ''){
                $('.st-icon-new-wrapper input', parent).focus();
            }
        });
        $('body').click(function (ev) {
            if ($(ev.target).closest('.st-icon-new-wrapper').length == 0 && !$(ev.target).is('#term_icon')) {
                $('.st-icon-new-wrapper', parent).hide();
            }
        });
        var timeout, ajax;
        $('.st-icon-new-wrapper input', parent).on('keyup', function () {
            var text = $(this).val();
            if (text.length < 2) {
                return false;
            }
            clearTimeout(timeout);
            if(ajax){
                ajax.abort();
            }
            var data = {
                text  : text,
                action: 'st_get_icon_new'
            };
            timeout  = setTimeout(function () {
                $('.st-icon-new-wrapper .loader-wrapper', parent).show();
                ajax = $.post(ajaxurl, data, function (respon) {
                    $('.st-icon-new-wrapper .loader-wrapper', parent).hide();
                    if (typeof respon == 'object') {
                        if (respon.status == 0) {
                            $('.st-icon-new-wrapper .render', parent).html(respon.data);
                        } else {
                            var html = '';
                            $.each(respon.data, function (index, value) {
                                html += '<div class="item" data-icon="' + index + '">' + value + '</div>';
                            });
                            $('.st-icon-new-wrapper .render', parent).html(html);
                            $('.st-icon-new-wrapper .render .item', parent).click(function(){
                                t.val($(this).attr('data-icon'));
                            });
                        }
                    }
                }, 'json');
            }, 500);
        });
    });
});