<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 11/9/2016
 * Time: 2:51 PM
 * Since 1.0.0
 * Updated 1.0.0
 */
get_header();
$termID = get_queried_object()->term_id;
stt_update_destination_view($termID);
$pageID = stt_get_term_meta($termID,'stt_destination_page');
?>
<?php
echo '<div class="stt-content-wrapper">';
echo \Elementor\Plugin::$instance->frontend->get_builder_content( $pageID );
if (comments_open() || get_comments_number()) {
    echo '<div class="comment-wrapper"><div class="container">';
    stt_comments_template($pageID);
    echo '</div></div>';
}
echo '</div>';
?>
<?php
get_footer();
?>


