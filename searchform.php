<?php
    /**
     * Created by PhpStorm.
     * User: Administrator
     * Date: 11/9/2016
     * Time: 2:51 PM
     * Since 1.0.0
     * Updated 1.0.0
     */
?>

<form class="form form-search header-search" action="<?php echo esc_url( home_url( '/' ) ); ?>" method="get">
    <div class="form-group placeholder">
        <input autocomplete="off" class="form-control" type="text" name="s"
               value="<?php echo esc_attr( get_search_query() ); ?>"/>
        <input type="hidden" name="post_type" value="post">
        <button name="submit" value="<?php echo esc_attr__( 'Search', 'layout-blog' ); ?>"
                class="btn btn-primary btn-search hidden" type="submit"><i class="fa fa-search"></i>
        </button>
    </div>
</form>