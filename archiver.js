// require modules
var fs = require('fs');
var archiver = require('archiver');

var output = fs.createWriteStream(__dirname + '/requirements/plugins/layout-blog-system.zip');
var archive = archiver('zip', {
    zlib: {level: 9}
});

output.on('close', function () {
    console.log(archive.pointer() + ' total bytes');
    console.log('archiver has been finalized and the output file descriptor has closed.');
});

output.on('end', function () {
    console.log('Data has been drained');
});

archive.on('warning', function (err) {
    if (err.code === 'ENOENT') {
    } else {
        throw err;
    }
});

archive.on('error', function (err) {
    throw err;
});

archive.pipe(output);

archive.directory('../../plugins/layout-blog-system/assets/', 'layout-blog-system/assets');
archive.directory('../../plugins/layout-blog-system/importer/', 'layout-blog-system/importer');
archive.directory('../../plugins/layout-blog-system/languages/', 'layout-blog-system/languages');
archive.directory('../../plugins/layout-blog-system/system/', 'layout-blog-system/system');
archive.directory('../../plugins/layout-blog-system/views/', 'layout-blog-system/views');

archive.file('../../plugins/layout-blog-system/index.php', { name: 'layout-blog-system/index.php' });
archive.file('../../plugins/layout-blog-system/LICENSE.txt', { name: 'layout-blog-system/LICENSE.txt' });
archive.file('../../plugins/layout-blog-system/readme.txt', { name: 'layout-blog-system/readme.txt' });
archive.file('../../plugins/layout-blog-system/layout-blog-system.php', { name: 'layout-blog-system/layout-blog-system.php' });

archive.finalize();



var output1 = fs.createWriteStream('../layout-blog.zip');
var archive1 = archiver('zip', {
    zlib: {level: 9}
});

output1.on('close', function() {
    console.log(archive1.pointer() + ' total bytes');
    console.log('archiver has been finalized and the output file descriptor has closed.');
});

output1.on('end', function() {
    console.log('Data has been drained');
});

archive1.on('warning', function(err) {
    if (err.code === 'ENOENT') {
    } else {
        throw err;
    }
});

archive1.on('error', function(err) {
    throw err;
});

archive1.pipe(output1);

archive1.directory('assets/','layout-blog/assets/');
archive1.directory('languages/','layout-blog/languages/');
archive1.directory('requirements/','layout-blog/requirements/');
archive1.directory('shinetheme/','layout-blog/shinetheme/');

archive1.glob('../layout-blog/*.txt');
archive1.glob('../layout-blog/*.css');
archive1.glob('../layout-blog/*.php');
archive1.glob('../layout-blog/*.png');

archive1.finalize();