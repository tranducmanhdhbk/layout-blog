<?php
if ( post_password_required() ) {
    return;
}
?>

<div id="comments" class="comments-area">

    <?php if ( have_comments() ) : ?>
        <h2 class="comments-title">
            <?php
            $comments_number = get_comments_number();
            if($comments_number == 1){
                echo sprintf(esc_html__('%s Comment ', 'layout-blog'), $comments_number);
            }else{
                echo sprintf(esc_html__('%s Comments', 'layout-blog'), $comments_number);
            }
            ?>
        </h2>

        <ol class="comment-list">
            <?php
            wp_list_comments( array(
                'style'       => 'ol',
                'short_ping'  => true,
                'avatar_size' => 50,
                'callback'    => 'stt_list_comments',
                'type'        => 'comment',
            ) );
            ?>
        </ol><!-- .comment-list -->

        <?php
        if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) :
            ?>
            <nav class="navigation comment-navigation" role="navigation">
                <div class="nav-links">
                    <?php
                    if ( $prev_link = get_previous_comments_link( __( 'Old Comments', 'layout-blog' ) ) ) :
                        printf( '<div class="nav-previous">%s</div>', $prev_link );
                    endif;

                    if ( $next_link = get_next_comments_link( __( 'New Comments', 'layout-blog' ) ) ) :
                        printf( '<div class="nav-next">%s</div>', $next_link );
                    endif;
                    ?>
                </div><!-- .nav-links -->
            </nav><!-- .comment-navigation -->
        <?php
        endif;
        ?>

    <?php endif; ?>

    <?php
    // If comments are closed and there are comments, let's leave a little note, shall we?
    if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) :
        ?>
        <p class="no-comments"><?php echo  esc_html__( 'Comments are closed.', 'layout-blog' ); ?></p>
    <?php endif; ?>

    <?php comment_form(
            array(
                'must_log_in'          => '<p class="must-log-in">' . sprintf(esc_html__('You have %s to post a comment  .', 'layout-blog'), '<a href="#st-login-popup" class="st-box-popup"  data-effect="mfp-zoom-in">'. esc_html__('logged in', 'layout-blog') .'</a>') . '</p>',
                'label_submit'=>'Submit',
                'title_reply' => '<img src="'. STT_ASSETS_URI .'/images/blog/ico-mess.png" alt="'.stt_get_alt_image().'" ><span>'. esc_html__('Write a comment','layout-blog') .'</span>',


            )
    ); ?>

</div><!-- .comments-area -->