<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 11/9/2016
 * Time: 2:51 PM
 * Since 1.0.0
 * Updated 1.0.0
 */
$pageFooter = get_option('stt_footer_page');
if(!empty($pageFooter)){
    echo '<footer class="stt-content-footer">';
    echo \Elementor\Plugin::$instance->frontend->get_builder_content( $pageFooter );
    echo '</footer>';
}
else{ ?>
    <footer id="footer-default">
        <?php echo wp_kses(sprintf(__('© %s <a href="%s" target="_blank">%s</a>', 'layout-blog'), date('Y'), home_url('/'), get_bloginfo('name')), ['a' => ['href' => [], 'target' => []]]) ?>
    </footer>
<?php } ?>
</div>
<?php wp_footer(); ?>
</body>
</html>