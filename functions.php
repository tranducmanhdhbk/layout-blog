<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 8/1/2017
 * Time: 12:03 AM
 * Since: 1.0.0
 * Updated: 1.0.0
 */

if (!session_id()) {
    session_start();
}
add_filter('use_block_editor_for_post', '__return_false');
add_action('after_setup_theme', 'st_layout_blog_setup');
function st_layout_blog_setup()
{
    load_theme_textdomain('layout-blog', get_template_directory() . '/languages');

    add_theme_support('automatic-feed-links');
    add_theme_support('title-tag');
    add_theme_support('post-thumbnails');
    add_theme_support(
        'html5',
        array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        )
    );
    add_theme_support('editor-styles');
    add_theme_support('responsive-embeds');
    register_nav_menus(
        apply_filters('st_register_menus', ['primary-menu' => __('Primary', 'layout-blog')])
    );
    add_image_size('st_image_location', 270, 360, true);
}

add_action('widgets_init', 'stt_add_sidebar');
if (!function_exists('stt_add_sidebar')) {
    function stt_add_sidebar()
    {
        register_sidebar([
            'name' => __('Blog Sidebar', 'layout-blog'),
            'id' => 'blog-sidebar',
            'description' => __('Widgets in this area will be shown on all posts and pages.', 'layout-blog'),
            'before_title' => '<h4 class="widget-title">',
            'after_title' => '</h4>',
            'before_widget' => '<div id="%1$s" class="sidebar-widget %2$s">',
            'after_widget' => '</div>',
        ]);
        register_sidebar([
            'name' => __('Normal Sidebar', 'layout-blog'),
            'id' => 'normal-sidebar',
            'before_title' => '<h4 class="widget-title">',
            'after_title' => '</h4>',
            'before_widget' => '<div id="%1$s" class="sidebar-widget %2$s">',
            'after_widget' => '</div>',
        ]);
    }
}
add_filter('comment_post_redirect', 'redirect_after_comment');
function redirect_after_comment($location)
{
    return stt_server("HTTP_REFERER");
}

define('STT_FOLDER', 'shinetheme');
require_once(trailingslashit(get_template_directory()) . STT_FOLDER . '/' . 'Shinetheme.php');

add_filter('upload_mimes', 'st_upload_types', 1, 1);
if ( ! isset( $content_width ) ) $content_width = 900;
function st_upload_types($mime_types)
{
    $mime_types['svg'] = 'image/svg+xml';

    return $mime_types;
}
