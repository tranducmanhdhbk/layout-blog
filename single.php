<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 11/9/2016
 * Time: 2:51 PM
 * Since 1.0.0
 * Updated 1.0.0
 */
$st = stt_get_instance();
get_header();


while (have_posts()): the_post();
    global $post;
    stt_update_post_view(get_the_ID());
    $categoryDetail = get_the_category(get_the_ID());
    $destinationDetail = get_the_terms(get_the_ID(), 'st_destinations');
    $tagDetail = get_the_tags(get_the_ID());
    $active_sidebar = is_active_sidebar('blog-sidebar');
    $class_left = 'col-md-8';
    if (!$active_sidebar) {
        $class_left = 'col-md-12';
    }
    ?>
    <div class="stt-blog">

        <div class="container">
            <div class="row">
                <div class="<?php echo esc_attr($class_left) ?> col-xs-12">
                    <?php stt_breadcrumbs(); ?>
                    <div id="post-<?php the_ID(); ?>" <?php post_class('stt-blog-content'); ?>>

                        <div class="stt-blog-slider">
                            <?php $st->load->view('single_slider', 'frontend/blog/single') ?>
                        </div>
                        <div class="stt-blog-title">
                            <?php $st->load->view('single_title', 'frontend/blog/single') ?>
                        </div>
                        <div class="blog-content">
                            <?php the_content() ?>
                        </div>
                        <div class="share-column">
                            <?php stt_blog_sharet(); ?>
                        </div>
                        <div class="blog-tag">
                            <?php
                            if (!empty($tagDetail)) {
                                foreach ($tagDetail as $k => $v) {
                                    $link = get_term_link($v->term_id, 'post_tag');
                                    ?>
                                    <div class="  tag-item">
                                        <div class="tag-name">
                                            <a href="<?php echo esc_url($link) ?>"><span><?php echo esc_html($v->name) ?></span></a>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                        <div class="author-info">
                            <div class="author-avatar">
                                <?php echo  get_avatar(get_option('admin_email'),210,'','',array("class" => "rounded-circle")) ?>
                            </div>
                            <div class="author-content">
                                <div class="stt-author-infor">
                                    <p class="author-name"><?php echo esc_html__('By ', 'layout-blog') ?>
                                        <span><?php echo esc_html(stt_username($post->post_author)) ?> </span></p>
                                    <div class="contact">
                                        <a class="facebook ico-item"><i class="fab fa-facebook-f"></i></a>
                                        <a class="instagram ico-item"><i class="fab fa-instagram"></i></a>
                                        <a class="twitter ico-item"><i class="fab fa-twitter"></i></a>
                                    </div>
                                </div>
                                <div class="stt-author-text">
                                    <span><?php echo esc_html__('Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam', 'layout-blog') ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="pagination clearfix">
                            <?php
                            $p = get_adjacent_post(1, '', 1);
                            if(!empty($p)){
                                $prevTitle = $p->post_title;
                            }else{
                                $prevTitle = '';
                            }
                            $n = get_adjacent_post(1, '', 0);
                            if(!empty($n)){
                                $nextTitle = $n->post_title;
                            }else{
                                $nextTitle = '';
                            }
                            the_post_navigation([
                                'next_text' => '<div class="stt-next"><span class="meta-nav" aria-hidden="true">' . esc_html__('Next post', 'layout-blog') . '<img src="' . STT_ASSETS_URI . 'images/Arrows-next.svg" alt="'.stt_get_alt_image().'">' . '</span></div>
                                <p>' . esc_html($nextTitle) . '</p>  ',
                                'prev_text' => '<div class="stt-prev"><span class="meta-nav" aria-hidden="true">' . '<img src="' . STT_ASSETS_URI . 'images/Arrows-prev.svg" alt="'.stt_get_alt_image().'" >' . esc_html__('Previous post', 'layout-blog') . '</span></div>
                                  <p>' . esc_html($prevTitle) . '</p>  ',
                            ]);
                            ?>
                        </div>
                        <div id="comment-wrapper">
                            <?php
                            if (comments_open() || get_comments_number()) :
                                comments_template();
                            endif;
                            ?>
                        </div>
                    </div>
                </div>
                <?php if ($active_sidebar) {?>
                    <div class=" col-md-4 col-xs-12">
                        <aside class='sidebar-right'>
                            <?php dynamic_sidebar('blog-sidebar'); ?>
                        </aside>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>


<?php endwhile;
get_footer();
?>

