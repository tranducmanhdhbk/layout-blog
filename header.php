<?php
    /**
     * Created by PhpStorm.
     * User: Administrator
     * Date: 11/9/2016
     * Time: 2:51 PM
     * Since 1.0.0
     * Updated 1.0.0
     */
$st = &stt_get_instance();
$headerStyle = stt_get_option('header','style-default');
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div class="body-wrapper">
    <header class="header-<?php echo esc_attr($headerStyle) ?>">
        <?php if($headerStyle == 'style-1'){
            $st->load->view('header-style-1','frontend/header');
        }else{
            $st->load->view('header-style-default','frontend/header');
        } ?>
    </header>


