<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 11/9/2016
 * Time: 2:51 PM
 * Since 1.0.0
 * Updated 1.0.0
 */
get_header();
$termID = get_queried_object()->term_id;
$st = &stt_get_instance();
$class_item = 'col-md-6';
$active_sidebar = is_active_sidebar('blog-sidebar');
$class_left = 'col-md-8';
if (!$active_sidebar) {
    $class_left = 'col-md-12';
    $class_item = 'col-md-4';
}
?>

<?php
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

global $wp_query;


echo '<div class="list-blog-archive"><div class="container">';
echo '<div class="row">';
echo '<div class="' . esc_attr($class_left) . ' col-xs-12"><div class="list-blog">';
stt_breadcrumbs(true);
echo '<div class="row">';
if (have_posts()) {
    while (have_posts()) {
        the_post();
        echo '<div class="' . esc_attr($class_item) . '">';
        ?>
        <div class="blog-item">
            <?php
            if (has_post_thumbnail()) {
                ?>
                <div class="thumb">
                    <a href="<?php echo esc_url(get_the_permalink()); ?>">
                        <?php the_post_thumbnail(array(740, 560)); ?>
                    </a>
                </div>

            <?php }
            ?>
            <div class="content">
                <div class="stt-taxonomy-blog clearfix">
                    <div class="stt-category">
                        <?php
                        if (is_category($termID)) {
                            $cateName = get_cat_name($termID);
                            $link = get_category_link($termID);
                        } else {
                            $stCategory = get_the_terms(get_the_ID(), 'category');
                            shuffle($stCategory);
                            if (!empty($stCategory)) {
                                $cateName = $stCategory[0]->name;
                                $link = get_category_link($stCategory[0]->term_id);
                            }
                        }
                        ?>
                        <a href="<?php echo esc_url($link) ?>"><?php echo esc_html($cateName) ?></a>
                    </div>
                    <div class="stt-destination">
                        <?php
                        $stDestination = get_the_terms(get_the_ID(), 'st_destinations');
                        if (!empty($stDestination)) {
                            $d = $stDestination[0];
                            $link = get_term_link($d->term_id, 'st_destinations');
                        }
                        ?>
                        <a href="<?php echo esc_url($link) ?>"><?php echo esc_html($d->name) ?></a>
                    </div>
                </div>
                <h4>
                    <a href="<?php echo esc_url(get_permalink($post->ID)); ?>"><?php echo esc_html(get_the_title()); ?></a>
                </h4>
                <p class="text-muted">
                        <span class="stt-author"><?php echo esc_html__('By ', 'layout-blog');
                            echo esc_html(stt_username($post->post_author)) ?></span>
                    <span><?php the_time(get_option('date_format')) ?></span>
                </p>
                <p class="stt-blog-description"><?php echo esc_html(get_the_excerpt()); ?></p>
            </div>
        </div>
        <?php
        echo '</div>';
    }
}
echo '</div></div>';
wp_reset_postdata();
echo '<div class="stt-pagination">';
stt_pagination($wp_query);
echo '</div>';
echo '</div>';
if ($active_sidebar) {
    echo '<div class="col-md-4 col-xs-12">';
    ?>
    <aside class='sidebar-right'>
        <?php dynamic_sidebar('blog-sidebar'); ?>
    </aside>

    <?php
    echo '</div>';
}

echo '</div></div></div>';
?>
<?php
get_footer();
?>

