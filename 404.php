<?php
    /**
     * Created by PhpStorm.
     * User: Administrator
     * Date: 11/9/2016
     * Time: 2:51 PM
     * Since 1.0.0
     * Updated 1.0.0
     */
get_header();
?>
<div class="container">
    <div class="row st-404-swapper">
        <div class="col-md-5  st-404-content" >
            <img class="img-thumbnail" src="<?php echo STT_ASSETS_URI ?>images/404.png" alt="<?php echo stt_get_alt_image() ?>">
            <h1><?php esc_html_e('Page not found','layout-blog') ?></h1>
            <p class="st-note-err"><?php esc_html_e('Sorry, but the page that you requested doesn’t exits','layout-blog') ?></p>
            <button type="submit" class="btn btn-default st-comeback"><a href=" <?php echo home_url() ?>"><?php esc_html_e('Go to homepage','layout-blog') ?></a></button>
        </div>
        <div class="col-md-6 offset-md-1 st-404-image">
            <img class="img-thumbnail" src="<?php echo STT_ASSETS_URI ?>images/ic-404.png" alt="<?php echo stt_get_alt_image() ?>">
        </div>
    </div>

</div>
<?php get_footer();
?>