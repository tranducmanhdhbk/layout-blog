var gulp = require('gulp');
var bower = require('gulp-bower');
var sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');

var config = {
    bowerDir: './bower_components'
};

// Install ow Bower Components
gulp.task('bower', function() {
    return bower()
        .pipe(gulp.dest(config.bowerDir))
});


gulp.task('dashboard', function() {
    gulp.src('./assets/dashboard/sass/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('./assets/dashboard/css/'))
});
gulp.task('scss', function() {
    gulp.src('./assets/sass/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('./assets/css/'))
});

gulp.task('backend', function() {
    gulp.src('./assets/backend/sass/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('./assets/backend/css/'))
});

gulp.task('default', ['scss', 'dashboard', 'backend'] , function () {
    gulp.watch([
        './assets/dashboard/sass/*.scss',
        './assets/backend/sass/*.scss',
        './assets/sass/*.scss'
    ], ['scss', 'dashboard', 'backend']);
});
gulp.task('front-scss', function() {
    gulp.src('./assets/sass/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('./assets/css/'))
});

gulp.task('frontScss', ['front-scss'] , function () {
    gulp.watch('./assets/sass/*.scss', ['front-scss']);
});